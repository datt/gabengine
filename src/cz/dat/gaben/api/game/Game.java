package cz.dat.gaben.api.game;

import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.exception.Gabexception;
import cz.dat.gaben.api.interfaces.IApiContext;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.util.GabeLogger;

import java.util.HashMap;
import java.util.Map;

public class Game {
    protected Map<Integer, ScreenBase> screens;
    protected ScreenBase currentScreen;
    private GameWindowBase window;

    public Game() {
        this.screens = new HashMap<>();
    }

    /**
     * Called every tick. Number of ticks per second is defined in {@link GameWindowBase#tps}
     * variable.
     */
    @ExceptionHandling(value = "LOGIC", errorCode = 102)
    public void onTick() {
        if (this.currentScreen != null)
            this.currentScreen.tick();
    }

    /**
     * Called every renderTick tick (every loop cycle).
     *
     * @param ptt Partial tick time
     */
    @ExceptionHandling(value = "RENDER", errorCode = 101)
    public void onRenderTick(float ptt) {
        if (this.currentScreen != null)
            this.currentScreen.renderTick(ptt);
    }

    public void onFpsCount() {
        this.window.setWindowTitle(this.window.title
                + " - "
                + this.window.fps
                + " FPS - "
                + ((this.currentScreen != null) ? this.currentScreen.getTitle()
                : ""));
    }

    public void init() {
    }

    public void cleanup() {
    }

    public void handleFeatureBreakingException(Gabexception e) {
        if(e.getFeatureBroken().equals("SHADERS")) {
            GabeLogger.warning("Shaders failed. The game probably won't work.");
        }
    }

    public void openScreen(ScreenBase screen) {
        if (this.currentScreen != null) {
            this.currentScreen.onClosing();
            this.window.getInput().removeEventListener(this.currentScreen);
        }

        this.window.getInput().addEventListener(screen);
        screen.onOpening();

        this.window.setWindowTitle(this.window.getTitle() + " - "
                + screen.getTitle());
        this.currentScreen = screen;
    }

    public ScreenBase getScreen() {
        return this.currentScreen;
    }

    public ScreenBase getScreen(int screenId) {
        return this.screens.get(screenId);
    }

    public void openScreen(int screenId) {
        this.openScreen(this.screens.get(screenId));
    }

    public void addScreen(int key, ScreenBase screen) {
        if (!this.screens.containsValue(screen)) {
            this.screens.put(key, screen);
        }
    }

    public int getWidth() {
        return (int) this.window.screenSize.x();
    }

    public int getHeight() {
        return (int) this.window.screenSize.y();
    }

    public IApiContext getApi() {
        return this.window;
    }

    public GameWindowBase getWindow() {
        return this.window;
    }

    public void setWindow(GameWindowBase window) {
        this.window = window;
    }

    public IContentManager getContent() {
        return this.window.content;
    }

    @ExceptionHandling(value = "NATURAL_EXIT", errorCode = 0)
    public void exit() {
        throw ExceptionUtil.gameEndingException(this);
    }
}
