package cz.dat.gaben.api.game;

import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.exception.Gabexception;
import cz.dat.gaben.api.game.GameWindowFactory.Api;
import cz.dat.gaben.api.interfaces.IApiContext;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.Vector2;

public abstract class GameWindowBase implements Runnable, IApiContext {

    protected Vector2 screenSize, centre;
    protected IContentManager content;

    protected int tps = 20, magicConstant = 1000000000;
    protected double tickTime = 1d / tps;
    protected double tickTimeSec = this.tickTime * this.magicConstant;
    protected long time, lastTime, lastInfo;
    protected int fps, ticks, lastTicks;
    protected int shutdownCode;

    protected Game game;
    protected String title;

    public GameWindowBase(int width, int height, IContentManager content, Game game) {
        this.screenSize = new Vector2(width, height);
        this.centre = new Vector2(width / 2, height / 2);
        this.content = content;
        this.game = game;
        game.setWindow(this);
    }

    public Thread start() {
        return this.start(true);
    }

    public Thread start(boolean differentThread) {
        if (this.game == null)
            throw new IllegalStateException("Game object not set");

        if (differentThread) {
            Thread t = new Thread(this);
            t.run();
            return t;
        } else {
            this.run();
            return null;
        }
    }

    /**
     * Called before loop is started. Calls {@link Game#init()}.
     *
     * @see #loop()
     */
    public void init() {
        try {
            this.content.load();
            this.game.init();
        } catch (Gabexception e) {
            if (e.getUrgency() == Gabexception.Urgency.BREAKING) {
                throw e;
            } else if (e.getUrgency() == Gabexception.Urgency.FEATURE_BREAKING) {
                this.game.handleFeatureBreakingException(e);
            }
        }

        GameWindowFactory.closeSplash();
    }

    /**
     * Called after {@link #init()}. Loop implementation should be done here.
     * This method should call {@link #tick()} each tick.
     *
     * @see #tick()
     */
    protected abstract void loop();

    /**
     * Handles the whole game cycle. Calls {@link #init()}, {@link #loop()} and
     * {@link #cleanup()} on the end.
     */
    @Override
    public void run() {
        this.time = System.nanoTime();
        this.lastTime = time;
        this.lastInfo = time;

        try {
            this.init();
            this.loop();
        } catch (Gabexception e) {
            this.shutdown(e.getErrorCode());
        } catch (Exception e) {
            ExceptionUtil.gameBreakingException(e, this);
        }

        this.cleanup();
        if (this.shutdownCode == 0)
            GabeLogger.info("Shutting down nicely");
        else
            GabeLogger.warning("Shutting down with error code " + this.shutdownCode);

        System.exit(this.shutdownCode);
    }

    /**
     * Handles cleanup. Calls {@link Game#cleanup()}.
     */
    public void cleanup() {
        this.game.cleanup();
        this.content.cleanup();
    }

    /**
     * Sets ticks per second
     *
     * @param tps New TPS value
     */
    public void setTps(int tps) {
        this.tps = tps;
        this.tickTime = 1D / tps;
        this.tickTimeSec = this.tickTime * this.magicConstant;
    }

    /**
     * Should be called every game loop cycle. Handles game times and
     * tick/renderTick calling.
     *
     * @see #loop()
     * @see Game#onTick()
     * @see Game#onRenderTick(float)
     * @see Game#onFpsCount()
     */
    protected void tick() {
        float ptt = (this.time - this.lastTime) / ((float) this.tickTimeSec);

        try {
            this.preRenderTick(ptt);
            this.game.onRenderTick(ptt);
            this.postRenderTick();

            this.fps++;

            this.time = System.nanoTime();
            while (time - lastTime >= this.tickTimeSec) {
                this.ticks++;
                this.game.onTick();

                lastTime += this.tickTimeSec;
            }

            if (time - lastInfo >= this.magicConstant) {
                lastInfo += this.magicConstant;
                this.game.onFpsCount();
                lastTicks = ticks;
                fps = 0;
            }
        } catch (Gabexception e) {
            if (e.getUrgency() == Gabexception.Urgency.BREAKING) {
                throw e;
            } else if (e.getUrgency() == Gabexception.Urgency.FEATURE_BREAKING) {
                this.game.handleFeatureBreakingException(e);
            }
        } catch (Exception e) {
            throw ExceptionUtil.gameBreakingException(e, this);
        }
    }

    public String getTitle() {
        return this.title;
    }

    /**
     * Sets the title, variable {@link #title}. This will not directly change
     * title of the window.
     *
     * @param title New title
     * @see #setWindowTitle(String)
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Sets title of the window
     *
     * @param title New title
     */
    public abstract void setWindowTitle(String title);

    /**
     * Resizes the window
     *
     * @param width  New window width
     * @param height New window height
     */
    public abstract void resize(int width, int height);

    /**
     * Makes window full-screened
     *
     * @param fullscreen     Determines if the window will be on full-screen
     * @param keepResolution If true, window will keep it's resolution and not change the
     *                       resolution to current system resolution
     */
    public abstract void resize(boolean fullscreen, boolean keepResolution);

    /**
     * @return State of window (true if full-screened)
     */
    public abstract boolean isFullscreened();

    /**
     * Sets FPS limit and vSync state
     *
     * @param fpsLimit New FPS limit (0 for unlimited)
     * @param vSync    Enable vertical synchronization
     */
    public abstract void setFpsLimit(int fpsLimit, boolean vSync);

    /**
     * Called before renderTick tick is passed to the Game object
     *
     * @param ptt Partial tick time
     */
    protected abstract void preRenderTick(float ptt);

    /**
     * Called after renderTick tick is passed to the Game object
     */
    protected abstract void postRenderTick();

    /**
     * Gets current Api type using
     *
     * @return Api type
     */
    public abstract Api getApiType();

    /**
     * Exits game properly
     *
     * @param code Exit code passed to OS
     */
    public void shutdown(int code) {
        this.shutdownCode = code;
    }

    public int getPassedTicks() {
        return this.ticks;
    }

    public IContentManager getContentManager() {
        return this.content;
    }

    public int getWidth() {
        return (int) this.screenSize.x();
    }

    public int getHeight() {
        return (int) this.screenSize.y();
    }

    public Vector2 getCentre() {
        return this.centre;
    }
}
