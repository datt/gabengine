package cz.dat.gaben.api.game;

import cz.dat.gaben.api.impl.lwjgl.LwjglBuilder;
import cz.dat.gaben.api.interfaces.IApiBuilder;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.util.OSUtil;
import cz.dat.gaben.util.SplashScreen;

import javax.swing.*;
import java.awt.*;

public class GameWindowFactory {
    private static boolean spEnable;
    private static SplashScreen splash;

    public static void enableSplashscreen(boolean enable,
                                          Image splashscreenImage) {
        // TODO: Splashscreen is apparently broken on systems with X server
        GameWindowFactory.spEnable = OSUtil.isWindows() && enable;

        if (GameWindowFactory.spEnable) {
            if (splashscreenImage == null)
                GameWindowFactory.splash = new SplashScreen();
            else
                GameWindowFactory.splash = new SplashScreen(splashscreenImage);
        }
    }

    public static GameWindowBase createGame(Api api, int width, int height,
                                            IContentManager contentManager, Game game) {
        if (GameWindowFactory.spEnable) {
            GameWindowFactory.splash.setEnabled(true);
            GameWindowFactory.splash.setVisible(true);
        }

        IApiBuilder b;

        switch (api) {
            case LWJGL:
                b = new LwjglBuilder();
                break;
            case UNKNOWN:
            default:
                return null;
        }

        if (!b.canBeBuilt())
            return null;

        b.setContentManager(contentManager);
        b.setWindowSize(width, height);
        b.setGame(game);

        return b.buildGameWindow();
    }

    public static GameWindowBase createGameOrDie(Api api, int width, int height,
                                                 IContentManager contentManager, boolean showPopup, Game game) {
        GameWindowBase b = GameWindowFactory.createGame(api, width, height, contentManager, game);

        if(b == null) {
            if(GameWindowFactory.spEnable)
                GameWindowFactory.splash.dispose();

            if(showPopup) {
                try {
                    UIManager.setLookAndFeel(
                            UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | UnsupportedLookAndFeelException | IllegalAccessException e) {
                    e.printStackTrace();
                }

                JOptionPane.showMessageDialog(null, "It isn't possible to launch the application because of the graphical API initialization problems.\n" +
                        "Contact the developer of this application for more details and help.\nError: API " + api.toString() + " refused to build.", "GabeNgine",
                        JOptionPane.ERROR_MESSAGE, null);
            }
            System.exit(1);
        }

        return b;
    }

    public static GameWindowBase createPreferredGame(int width, int height,
                                                     IContentManager contentManager, Game game) {
        GameWindowBase ret;
        ret = GameWindowFactory.createGame(Api.LWJGL, width, height,
                contentManager, game);

        return ret;
    }

    static void closeSplash() {
        if (GameWindowFactory.splash != null) {
            GameWindowFactory.splash.setVisible(false);
            GameWindowFactory.splash.dispose();
        }
    }

    public enum Api {
        LWJGL, UNKNOWN
    }
}
