package cz.dat.gaben.api.game;

import cz.dat.gaben.api.interfaces.IInputManager.IInputListener;
import cz.dat.gaben.api.interfaces.ITickListener;

public abstract class ScreenBase implements ITickListener, IInputListener {
    protected Game game;
    protected String title;

    private boolean wasOpened = false;

    public ScreenBase(Game game, String title) {
        this.game = game;
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public boolean wasOpened() {
        return this.wasOpened;
    }

    public void onOpening() {
        this.wasOpened = true;
    }

    public void onClosing() {
    }

    public Game getGame() {
        return this.game;
    }

    @Override
    public void onKeyDown(int key) {
    }

    @Override
    public void onKeyUp(int key) {
    }

    @Override
    public void onChar(char typedChar) {
    }

    @Override
    public void onMousePositionChange(float x, float y) {
    }

    @Override
    public void onMouseButtonDown(int button) {
    }

    @Override
    public void onMouseButtonUp(int button) {
    }

    @Override
    public void onMouseScroll(float x, float y) {
    }
}
