package cz.dat.gaben.api.exception;

public class ShaderException extends Exception {

    private String info;
    private FailedPhase failedPhase;

    public ShaderException(String logInfo, FailedPhase phase) {
        super("Couldn't init shaders");
        this.info = logInfo;
        this.failedPhase = phase;
    }

    public String getLogInfo() {
        return this.info;
    }

    public FailedPhase getFailedPhase() {
        return this.failedPhase;
    }

    public enum FailedPhase {
        LINKING, VALIDATING, COMPILATION
    }
}


