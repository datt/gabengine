package cz.dat.gaben.api.exception;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface ExceptionHandling {
    String value();
    int errorCode() default 100;
}