package cz.dat.gaben.api.exception;

import cz.dat.gaben.util.GabeLogger;

public class ExceptionUtil {
    public static Gabexception nonBreakingException(String message, Throwable original, Object source) {
        Gabexception e = new Gabexception(message, original, source, false, false);
        GabeLogger.exception(e);
        return e;
    }

    public static Gabexception featureBreakingException(String message, Throwable original, Object source) {
        Gabexception e = new Gabexception(message, original, source, true, true);
        GabeLogger.exception(e);
        return e;
    }

    public static Gabexception gameBreakingException(String message, Throwable original, Object source) {
        Gabexception e = new Gabexception(message, original, source, true, false);
        GabeLogger.exception(e);
        return e;
    }

    public static Gabexception gameEndingException(Object source) {
        return new Gabexception("Ending", new Exception(), source, true, false);
    }

    public static Gabexception nonBreakingException(Throwable original, Object source) {
        return ExceptionUtil.nonBreakingException(original.getMessage(), original, source);
    }

    public static Gabexception featureBreakingException(Throwable original, Object source) {
        return ExceptionUtil.featureBreakingException(original.getMessage(), original, source);
    }

    public static Gabexception gameBreakingException(Throwable original, Object source) {
        return ExceptionUtil.gameBreakingException(original.getMessage(), original, source);
    }

    public static Gabexception nonBreakingException(String message, Object source) {
        return ExceptionUtil.nonBreakingException(message, new Exception(), source);
    }

    public static Gabexception featureBreakingException(String message, Object source) {
        return ExceptionUtil.featureBreakingException(message, new Exception(), source);
    }

    public static Gabexception gameBreakingException(String message, Object source) {
        return ExceptionUtil.gameBreakingException(message, new Exception(), source);
    }
}
