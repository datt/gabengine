package cz.dat.gaben.api.exception;

public class ContextCreationException extends Exception {
    private FailedPhase failedPhase;

    public ContextCreationException(String message, FailedPhase failedPhase) {
        super(message, null, true, true);
        this.failedPhase = failedPhase;
    }

    public FailedPhase getFailedPhase() {
        return this.failedPhase;
    }

    public enum FailedPhase {
        GLFW_INIT, WINDOW_INIT
    }
}
