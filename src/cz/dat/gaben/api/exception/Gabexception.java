package cz.dat.gaben.api.exception;

import cz.dat.gaben.util.GabeLogger;

import java.lang.reflect.Method;

public class Gabexception extends RuntimeException {
    public enum Urgency {
        NON_BREAKING, BREAKING, FEATURE_BREAKING
    }

    private String feature;
    private Object source;
    private Urgency urgency;
    private int errorCode;

    Gabexception(String message, Throwable cause, Object source, boolean breaking, boolean featureBreaking) {
        super(message, cause, true, false);
        this.source = source;
        this.urgency = (breaking ? (featureBreaking ? Urgency.FEATURE_BREAKING : Urgency.BREAKING) : Urgency.NON_BREAKING);

        if (cause != null) {
            outer:
            for (StackTraceElement e : cause.getStackTrace()) {
                try {
                    for (Method m : Class.forName(e.getClassName()).getDeclaredMethods()) {
                        if(m.getName().equals(e.getMethodName())) {
                            ExceptionHandling mhat = m.getAnnotation(ExceptionHandling.class);

                            if (mhat != null) {
                                this.feature = mhat.value();
                                this.errorCode = mhat.errorCode();
                                break outer;
                            }
                        }
                    }
                } catch (ClassNotFoundException e1) {
                    GabeLogger.warning("Error creating exception. RIP");
                }
            }
        }

        ExceptionHandling hat = source.getClass().getAnnotation(ExceptionHandling.class);

        if (hat != null) {
            this.feature = hat.value();
            this.errorCode = hat.errorCode();
        }

        if(this.feature == null)
            this.feature = source.getClass().getName();
    }

    public Object getSource() {
        return this.source;
    }

    public Urgency getUrgency() {
        return this.urgency;
    }

    public String getFeatureBroken() {
        return this.feature;
    }

    public int getErrorCode() {
        return this.errorCode;
    }
}
