package cz.dat.gaben.api.exception;

public class FontLoadingException extends Exception {

    private String fontName;

    public FontLoadingException(String fontName, Throwable cause) {
        super("Couldn't load font " + fontName, cause);
        this.fontName = fontName;
    }

    public String getFailedFontName() {
        return this.fontName;
    }
}
