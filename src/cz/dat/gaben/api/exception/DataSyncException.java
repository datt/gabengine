package cz.dat.gaben.api.exception;

public class DataSyncException extends Exception {
    private FailedOperation failedOperation;

    public DataSyncException(String message, FailedOperation failedOperation) {
        super(message);
        this.failedOperation = failedOperation;
    }

    public DataSyncException(String message, FailedOperation failedOperation, Throwable cause) {
        super(message, cause);
        this.failedOperation = failedOperation;
    }

    public FailedOperation getFailedOperation() {
        return this.failedOperation;
    }

    public enum FailedOperation {
        LOAD, SAVE, PARSE, LOAD_CLASS
    }
}
