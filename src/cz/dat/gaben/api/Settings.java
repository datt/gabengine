package cz.dat.gaben.api;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;

import java.util.HashMap;
import java.util.Map;

public class Settings {

    private Map<Integer, Integer> texParameters = new HashMap<>();

    public Settings() {
        this.setMagFilter(Filters.MAG_FILTER_DEF);
        this.setMinFilter(Filters.MIN_FILTER_DEF);
        this.setWrapS(Wrap.WRAPS_DEF);
        this.setWrapT(Wrap.WRAPT_DEF);
    }

    public void setTexParameter(int parameter, int value) {
        this.texParameters.put(parameter, value);
    }

    public int getMinFilter() {
        return texParameters.get(GL11.GL_TEXTURE_MIN_FILTER);
    }

    public void setMinFilter(int filter) {
        this.texParameters.put(GL11.GL_TEXTURE_MIN_FILTER, filter);
    }

    public int getMagFilter() {
        return texParameters.get(GL11.GL_TEXTURE_MAG_FILTER);
    }

    public void setMagFilter(int filter) {
        if (filter == Filters.LINEAR || filter == Filters.NEAREST)
            this.texParameters.put(GL11.GL_TEXTURE_MAG_FILTER, filter);
    }

    public int getWrapS() {
        return texParameters.get(GL11.GL_TEXTURE_WRAP_S);
    }

    public void setWrapS(int wrapS) {
        this.texParameters.put(GL11.GL_TEXTURE_WRAP_S, wrapS);
    }

    public int getWrapT() {
        return texParameters.get(GL11.GL_TEXTURE_WRAP_T);
    }

    public void setWrapT(int wrapT) {
        this.texParameters.put(GL11.GL_TEXTURE_WRAP_T, wrapT);
    }

    public Map<Integer, Integer> getTexParameters() {
        return texParameters;
    }

    public class Filters {
        public static final int NEAREST = GL11.GL_NEAREST;
        public static final int LINEAR = GL11.GL_LINEAR;
        public static final int MIPMAP_NEAREST = GL11.GL_NEAREST_MIPMAP_NEAREST;
        public static final int MIPMAP_LINEAR = GL11.GL_LINEAR_MIPMAP_LINEAR;
        public static final int MIPMAP_LINEAR_NEAREST = GL11.GL_LINEAR_MIPMAP_NEAREST;
        public static final int MIPMAP_NEAREST_LINEAR = GL11.GL_NEAREST_MIPMAP_LINEAR;

        public static final int MIN_FILTER_DEF = Filters.MIPMAP_LINEAR;
        public static final int MAG_FILTER_DEF = Filters.LINEAR;
    }

    public class Wrap {
        public static final int CLAMP_TO_EDGE = GL12.GL_CLAMP_TO_EDGE;
        public static final int CLAMP_TO_BORDER = GL13.GL_CLAMP_TO_BORDER;
        public static final int MIRRORED_REPEAT = GL14.GL_MIRRORED_REPEAT;
        public static final int REPEAT = GL11.GL_REPEAT;

        public static final int WRAPS_DEF = Wrap.REPEAT;
        public static final int WRAPT_DEF = Wrap.REPEAT;
    }
}
