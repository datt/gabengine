package cz.dat.gaben.api.data;

import cz.dat.gaben.api.exception.DataSyncException;

public interface IDataProvider {
    public <T> void setData(String key, T value, Class<T> type);

    public <T> T getData(String key, Class<T> type);

    public void removeData(String key);

    public Class getDataType(String key);

    public void sync(MergeType mergeType) throws DataSyncException;

    public enum MergeType {
        KEEP_MEMORY, KEEP_LOCAL, PREFER_MEMORY, PREFER_LOCAL
    }
}
