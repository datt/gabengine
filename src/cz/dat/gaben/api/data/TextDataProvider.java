package cz.dat.gaben.api.data;

import cz.dat.gaben.api.exception.DataSyncException;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.BiConsumer;

public class TextDataProvider implements IDataProvider {

    private final String splitSeq = "\u6927";
    private File file;
    private Map<String, String> stringData = new HashMap<>();
    private Map<String, Integer> intData = new HashMap<>();
    private Map<String, Float> floatData = new HashMap<>();
    private Map<String, Boolean> boolData = new HashMap<>();
    private Map<String, Map.Entry<Class, Object>> otherData = new HashMap<>();
    private Map<Class, Integer> allTypes = new HashMap<>();
    private List<String> allKeys = new ArrayList<>();

    public TextDataProvider(String file) throws FileNotFoundException {
        File f = new File(file);
        if (!f.isFile() || !f.exists())
            throw new FileNotFoundException();

        this.file = f;
    }

    @Override
    public <T> void setData(String key, T value, Class<T> type) {
        if (this.allKeys.contains(key))
            this.removeData(key);

        this.allKeys.add(key);
        if (type == String.class) {
            this.stringData.put(key, value.toString());
        } else if (type == Integer.class) {
            this.intData.put(key, (Integer) value);
        } else if (type == Float.class) {
            this.floatData.put(key, (Float) value);
        } else if (type == Boolean.class) {
            this.boolData.put(key, (Boolean) value);
        } else {
            this.otherData.put(key, new AbstractMap.SimpleImmutableEntry<>(type, value));

            if (!this.allTypes.containsKey(type))
                this.allTypes.put(type, 1);
            else
                this.allTypes.put(type, this.allTypes.get(type) + 1);
        }
    }

    @Override
    public <T> T getData(String key, Class<T> type) {
        if (this.allKeys.contains(key)) {
            if (type == String.class) {
                return (T) this.stringData.get(key);
            } else if (type == Integer.class) {
                return (T) this.intData.get(key);
            } else if (type == Float.class) {
                return (T) this.floatData.get(key);
            } else if (type == Boolean.class) {
                return (T) this.boolData.get(key);
            } else {
                Map.Entry<Class, Object> e = this.otherData.get(key);
                if (e != null && e.getKey().equals(type)) {
                    return (T) e.getValue();
                }
            }
        }

        return null;
    }

    @Override
    public void removeData(String key) {
        if (this.allKeys.contains(key)) {
            this.allKeys.remove(key);

            this.stringData.remove(key);
            this.intData.remove(key);
            this.floatData.remove(key);
            this.boolData.remove(key);

            Map.Entry<Class, Object> poorData = this.otherData.remove(key);
            if (poorData != null) {
                if (this.allTypes.get(poorData.getKey()) == 1)
                    this.allTypes.remove(poorData.getKey());
            }
        }

    }

    @Override
    public Class getDataType(String key) {
        if (this.allKeys.contains(key)) {
            if (this.stringData.containsKey(key))
                return String.class;

            if (this.intData.containsKey(key))
                return Integer.class;

            if (this.floatData.containsKey(key))
                return Float.class;

            if (this.boolData.containsKey(key))
                return Boolean.class;

            if (this.otherData.containsKey(key))
                return this.otherData.get(key).getKey();
        }

        return null;
    }

    @Override
    public void sync(MergeType type) throws DataSyncException {
        switch (type) {
            case KEEP_MEMORY:
                try {
                    this.save(true);
                } catch (IOException ioE) {
                    throw new DataSyncException("Can't save memory contents",
                            DataSyncException.FailedOperation.SAVE, ioE);
                }

                return;
            case KEEP_LOCAL:
                this.allTypes.clear();
                this.allKeys.clear();
                this.intData.clear();
                this.floatData.clear();
                this.boolData.clear();
                this.stringData.clear();
                this.otherData.clear();

                try {
                    this.load(true);
                } catch (IOException ioE) {
                    throw new DataSyncException("Can't load file contents",
                            DataSyncException.FailedOperation.LOAD, ioE);
                } catch (ClassNotFoundException cnfE) {
                    throw new DataSyncException("Can't deserialize class " + cnfE.getMessage(),
                            DataSyncException.FailedOperation.LOAD_CLASS, cnfE);
                }

                return;
            case PREFER_MEMORY:
                try {
                    this.load(false);
                } catch (IOException ioE) {
                    throw new DataSyncException("Can't load file contents",
                            DataSyncException.FailedOperation.LOAD, ioE);
                } catch (ClassNotFoundException cnfE) {
                    throw new DataSyncException("Can't deserialize class " + cnfE.getMessage(),
                            DataSyncException.FailedOperation.LOAD_CLASS, cnfE);
                }

                try {
                    this.save(true);
                } catch (IOException ioE) {
                    throw new DataSyncException("Can't save memory contents",
                            DataSyncException.FailedOperation.SAVE, ioE);
                }

                return;
            case PREFER_LOCAL:
                try {
                    this.load(true);
                } catch (IOException ioE) {
                    throw new DataSyncException("Can't load file contents",
                            DataSyncException.FailedOperation.LOAD, ioE);
                } catch (ClassNotFoundException cnfE) {
                    throw new DataSyncException("Can't deserialize class " + cnfE.getMessage(),
                            DataSyncException.FailedOperation.LOAD_CLASS, cnfE);
                }

                try {
                    this.save(false);
                } catch (IOException ioE) {
                    throw new DataSyncException("Can't save memory contents",
                            DataSyncException.FailedOperation.SAVE, ioE);
                }
        }
    }

    private List<String> readLines() {
        List<String> ret = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(this.file));
            String line;

            while ((line = reader.readLine()) != null) {
                ret.add(line);
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ret;
    }

    private void load(boolean overwriteDataInMemory) throws IOException, ClassNotFoundException {
        List<String> parts = new ArrayList<>();
        int splitSeqInt = ((int) this.splitSeq.charAt(0));

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(this.file), Charset.forName("UTF-8")));

        StringBuilder b = new StringBuilder();
        while (reader.ready()) {
            int r = reader.read();

            if (r == splitSeqInt) {
                parts.add(b.toString());
                b.setLength(0);
            } else {
                b.append((char) r);
            }
        }

        reader.close();

        String className = null;
        String key = null;
        String value = null;

        for (String part : parts) {
            if (part.equals("\n"))
                continue;

            //TODO: Check format and throw parse error if it's not valid
            if (className == null) {
                className = part;
            } else if (key == null) {
                key = part;
            } else {
                value = part;

                if (overwriteDataInMemory || !this.allKeys.contains(key)) {
                    Class c;

                    try {
                        c = Class.forName(className);
                    } catch (ClassNotFoundException e) {
                        throw new ClassNotFoundException(className, e);
                    }

                    if (c == String.class) {
                        this.setData(key, value, c);
                    } else if (c == Integer.class) {
                        this.setData(key, Integer.parseInt(value), c);
                    } else if (c == Float.class) {
                        this.setData(key, Float.parseFloat(value), c);
                    } else if (c == Boolean.class) {
                        this.setData(key, Boolean.parseBoolean(value), c);
                    } else {
                        this.setData(key, this.deserializeObject(value), c);
                    }
                }

                className = null;
                key = null;
                value = null;
            }
        }
    }

    private void save(boolean overwriteDataInFile) throws IOException {
        final List<String> lines;
        Map<String, Integer> uCantTouchThis = new HashMap<>();

        if (!overwriteDataInFile)
            lines = this.readLines();
        else
            lines = null;

        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.file), Charset.forName("UTF-8")));

        BiConsumer<String, Map.Entry<Class, Object>> serialized = (k, v) -> {
            try {
                if (!uCantTouchThis.containsKey(k)) {
                    writer.write(v.getKey().getName());
                    writer.write(this.splitSeq);
                    writer.write(k);
                    writer.write(this.splitSeq);
                    writer.write(this.serializeObject(v.getValue()));
                    writer.write(this.splitSeq);
                    writer.write("\n");
                    writer.write(this.splitSeq);
                } else {
                    writer.write(lines.get(uCantTouchThis.get(k)));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        };

        BiConsumer<String, Object> string = (k, v) -> {
            try {
                if (!uCantTouchThis.containsKey(k)) {
                    writer.write(v.getClass().getName());
                    writer.write(this.splitSeq);
                    writer.write(k);
                    writer.write(this.splitSeq);
                    writer.write(v.toString());
                    writer.write(this.splitSeq);
                    writer.write("\n");
                    writer.write(this.splitSeq);
                } else {
                    writer.write(lines.get(uCantTouchThis.get(k)));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        };

        if (!overwriteDataInFile) {
            for (int i = 0; i < lines.size(); i++) {
                String[] parts = lines.get(i).split(this.splitSeq);
                if (parts.length >= 3) {
                    uCantTouchThis.put(parts[(parts[0].length() > 0 ? 0 : 1)], i);
                }
            }
        }

        this.stringData.forEach(string);
        this.intData.forEach(string);
        this.floatData.forEach(string);
        this.boolData.forEach(string);

        writer.flush();
        writer.close();
    }

    private String serializeObject(Object o) throws IOException {
        ByteArrayOutputStream array = new ByteArrayOutputStream();

        ObjectOutputStream out = new ObjectOutputStream(array);
        out.writeObject(o);
        out.close();
        byte[] bytes = array.toByteArray();
        array.close();
        return Base64.getEncoder().encodeToString(bytes);
    }

    private <T> T deserializeObject(String s) throws IOException, ClassNotFoundException {
        byte[] bytes = Base64.getDecoder().decode(s);
        ByteArrayInputStream array = new ByteArrayInputStream(bytes);
        Object ret;

        ObjectInputStream in = new ObjectInputStream(array);
        ret = in.readObject();
        in.close();
        array.close();

        return (T) ret;
    }
}
