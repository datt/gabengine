package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.util.Vector3;
import org.lwjgl.openal.AL10;

import java.util.List;

public interface ISound {
    int ALWAYS_PLAY_PRIORITY = Integer.MIN_VALUE;

    void init();

    float getLength();

    float getBitrate();

    float getSampleFrequency();

    default ISoundInstance play() {
        return this.play(0, true);
    }

    ISoundInstance play(int priority, boolean applyProperites);

    ISoundInstance getFreeInstance(int priority);

    List<ISoundInstance> getAllPlayingInstances();

    void stopAll();

    boolean isStreaming();

    boolean isLooping();

    void setLooping(boolean looping);

    float getGain();

    void setGain(float gain);

    float getPitch();

    void setPitch(float pitch);

    void setSpacePosition(float x, float y, float z);

    Vector3 getSpacePosition();

    default void setSpacePosition(Vector3 pos) {
        this.setSpacePosition(pos.x(), pos.y(), pos.z());
    }

    void setSpaceVelocity(float x, float y, float z);

    Vector3 getSpaceVelocity();

    default void setSpaceVelocity(Vector3 pos) {
        this.setSpaceVelocity(pos.x(), pos.y(), pos.z());
    }

    void setSoundPositioning(boolean relativeToListener);

    boolean isPositionedRelativeToListener();

    void setDirection(float x, float y, float z);

    Vector3 getDirection();

    default void setDirection(Vector3 dir) {
        this.setDirection(dir.x(), dir.y(), dir.z());
    }

    void setCone(float innerAngle, float outerAngle, float outerGain);

    Vector3 getCone();

    default void setCone(Vector3 cone) {
        this.setCone(cone.x(), cone.y(), cone.z());
    }

    enum SoundState {
        LOADING(AL10.AL_INITIAL), STOPPED(AL10.AL_STOPPED), PLAYING(AL10.AL_PLAYING), PAUSED(AL10.AL_PAUSED);

        private int alPrimitive;

        SoundState(int glPrimitive) {
            this.alPrimitive = glPrimitive;
        }

        public static SoundState getForAl(int al) {
            for (SoundState soundState : SoundState.values()) {
                if(soundState.alPrimitive == al)
                    return soundState;
            }

            return null;
        }

        public int getAl() {
            return alPrimitive;
        }
    }
}
