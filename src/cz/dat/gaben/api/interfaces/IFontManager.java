package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.api.interfaces.IFont.FontDecoration;
import cz.dat.gaben.api.interfaces.IFont.FontStyle;

public interface IFontManager {
    IFont getFont(String name);

    void loadFont(String name, String path, float nominalSize, IFont.FontStyle style, IFont.FontDecoration[] decorations);

    default void loadFont(String name, String path, float nominalSize, IFont.FontStyle style) {
        this.loadFont(name, path, nominalSize, style, new FontDecoration[0]);
    }

    default void loadFont(String name, String path, float nominalSize) {
        this.loadFont(name, path, nominalSize, FontStyle.PLAIN);
    }

    void cleanup();

}
