package cz.dat.gaben.api.interfaces;

public interface ITickListener {
    void tick();

    void renderTick(float ptt);
}
