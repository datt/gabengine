package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.util.Vector2;

import java.awt.image.BufferedImage;

public interface IInputManager {
    /**
     * Determines if key is down
     *
     * @param key Key
     * @return Key state
     */
    boolean isKeyDown(int key);

    /**
     * Determines if mosue button is down
     *
     * @param button Button
     * @return Button state
     */
    boolean isMouseButtonDown(int button);

    /**
     * Gets text currently saved in the clipboard
     *
     * @return Text from clipboard
     */
    String getClipboardString();

    /**
     * Gets current mouse position. You should use {@link IInputListener#onMousePositionChange(float, float)} instead
     *
     * @return Vector of current mouse position
     */
    Vector2 getMousePosition();

    /**
     * Adds an input event listener
     *
     * @param l Listener to add
     */
    void addEventListener(IInputListener l);

    /**
     * Removes an input event listener
     *
     * @param l Listener to remove
     */
    void removeEventListener(IInputListener l);

    /**
     * Sets mouse hook - if mouse is hooked, it's fixed inside of the window
     */
    void setMouseCursorHooked(boolean hooked);

    /**
     * Sets if the mouse is visible when on top of the window
     */
    void setMouseCursorVisible(boolean visible);

    /**
     * Sets cursor of the window
     *
     * @param image Image to set as cursor
     * @param hotX  X coordinate of anchor point
     * @param hotY  Y coordinate of anchor point
     */
    void setCursor(BufferedImage image, int hotX, int hotY);

    void cleanup();

    interface IInputListener {
        void onKeyDown(int key);

        void onKeyUp(int key);

        void onChar(char typedChar);

        void onMousePositionChange(float x, float y);

        void onMouseButtonDown(int button);

        void onMouseButtonUp(int button);

        void onMouseScroll(float x, float y);
    }

    class Keys {
        public static int UNKNOWN;
        public static int SPACE;
        public static int APOSTROPHE;
        public static int COMMA;
        public static int MINUS;
        public static int PERIOD;
        public static int SLASH;
        public static int N0;
        public static int N1;
        public static int N2;
        public static int N3;
        public static int N4;
        public static int N5;
        public static int N6;
        public static int N7;
        public static int N8;
        public static int N9;
        public static int SEMICOLON;
        public static int EQUAL;
        public static int A;
        public static int B;
        public static int C;
        public static int D;
        public static int E;
        public static int F;
        public static int G;
        public static int H;
        public static int I;
        public static int J;
        public static int K;
        public static int L;
        public static int M;
        public static int N;
        public static int O;
        public static int P;
        public static int Q;
        public static int R;
        public static int S;
        public static int T;
        public static int U;
        public static int V;
        public static int W;
        public static int X;
        public static int Y;
        public static int Z;
        public static int LEFT_BRACKET;
        public static int BACKSLASH;
        public static int RIGHT_BRACKET;
        public static int GRAVE_ACCENT;
        public static int ESCAPE;
        public static int ENTER;
        public static int TAB;
        public static int BACKSPACE;
        public static int INSERT;
        public static int DELETE;
        public static int RIGHT;
        public static int LEFT;
        public static int DOWN;
        public static int UP;
        public static int PAGE_UP;
        public static int PAGE_DOWN;
        public static int HOME;
        public static int END;
        public static int CAPS_LOCK;
        public static int SCROLL_LOCK;
        public static int NUM_LOCK;
        public static int PRINT_SCREEN;
        public static int PAUSE;
        public static int F1;
        public static int F2;
        public static int F3;
        public static int F4;
        public static int F5;
        public static int F6;
        public static int F7;
        public static int F8;
        public static int F9;
        public static int F10;
        public static int F11;
        public static int F12;
        public static int F13;
        public static int F14;
        public static int F15;
        public static int F16;
        public static int F17;
        public static int F18;
        public static int F19;
        public static int F20;
        public static int F21;
        public static int F22;
        public static int F23;
        public static int F24;
        public static int F25;
        public static int KP_0;
        public static int KP_1;
        public static int KP_2;
        public static int KP_3;
        public static int KP_4;
        public static int KP_5;
        public static int KP_6;
        public static int KP_7;
        public static int KP_8;
        public static int KP_9;
        public static int KP_DECIMAL;
        public static int KP_DIVIDE;
        public static int KP_MULTIPLY;
        public static int KP_SUBTRACT;
        public static int KP_ADD;
        public static int KP_ENTER;
        public static int KP_EQUAL;
        public static int LEFT_SHIFT;
        public static int LEFT_CONTROL;
        public static int LEFT_ALT;
        public static int LEFT_SUPER;
        public static int RIGHT_SHIFT;
        public static int RIGHT_CONTROL;
        public static int RIGHT_ALT;
        public static int RIGHT_SUPER;
        public static int MENU;
        public static int LAST;

        public static int MOUSE_LEFT;
        public static int MOUSE_MIDDLE;
        public static int MOUSE_RIGHT;
    }
}
