package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.util.*;

public interface IRenderer {

    void cleanup();

    /**
     * Renders stacked primitives to the screen
     */
    void flush();

    /**
     * Begins stacking vertexes for selected primitive mode
     *
     * @param mode Primitive mode
     */
    void primitiveMode(PrimitiveMode mode);

    /**
     * Sets the mode of shapes rendering (filled or outlined)
     *
     * @param mode Shape rendering mode
     */
    void shapeMode(ShapeMode mode);

    /**
     * Binds texture
     *
     * @param texture Texture object
     */
    void texture(ITexture texture);
    
    void shader(IShader shader);
    
    void defaultShader();

    /**
     * Enables or disables texture usage
     *
     * @param enable Texture usage
     */
    void enableTexture(boolean enable);

    /**
     * Sets width of rendered lines
     *
     * @param width Lines width
     */
    void lineWidth(float width);

    /**
     * Sets size of rendered point
     *
     * @param size Point size diameter
     */
    void pointSize(float size);

    void texCoord(float s, float t);

    default void texCoord(Vector2 coord) {
        this.texCoord(coord.x(), coord.y());
    }

    /**
     * Sets color to use
     * A = 255 (1f)
     *
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     */
    default void color(float r, float g, float b) {
        this.color(r, g, b, 1f);
    }

    /**
     * Sets color to use
     * A = 255
     *
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     */
    default void color(int r, int g, int b) {
        this.color(r, g, b, 255);
    }

    /**
     * Sets color to use (values 0.0f - 1.0f)
     *
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     * @param a Alpha value
     */
    void color(float r, float g, float b, float a);

    /**
     * Sets color to use (values 0 - 255)
     *
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     * @param a Alpha value
     */
    default void color(int r, int g, int b, int a) {
        this.color(r / 255f, g / 255f, b / 255f, a / 255f);
    }

    /**
     * Sets color to use
     *
     * @param color Packed color int
     */
    default void color(int color) {
        this.color(Color.r(color), Color.g(color), Color.b(color), Color.a(color));
    }

    default void clearColor(int color) {
        this.clearColor(Color.r(color), Color.g(color), Color.b(color), Color.a(color));
    }

    /**
     * Sets color to use for clearing the screen
     *
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     * @param a Alpha value
     */
    default void clearColor(int r, int g, int b, int a) {
        this.clearColor(r / 255f, g / 255f, b / 255f, a / 255f);
    }

    /**
     * Sets color to use for clearing the screen
     *
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     */
    default void clearColor(float r, float g, float b) {
        this.clearColor(r, g, b, 1f);
    }

    /**
     * Sets color to use for clearing the screen
     *
     * @param r Red value
     * @param g Green value
     * @param b Blue value
     * @param a Alpha value
     */
    void clearColor(float r, float g, float b, float a);

    /**
     * Adds vertex when primitive rendering
     *
     * @param x X position
     * @param y Y position
     * @see #primitiveMode(PrimitiveMode)
     */
    void vertex(float x, float y);

    /**
     * Adds vertex when primitive rendering
     *
     * @param pos Vertex position
     * @see #primitiveMode(PrimitiveMode)
     */
    default void vertex(Vector2 pos) {
        this.vertex(pos.x(), pos.y());
    }
      
    Matrix4 getMatrix();

    void setMatrix(Matrix4 mat);

    Matrix4 identityMatrix();

    /**
     * Clears screen with specified color
     */
    void clear();

    void drawPoint(float x0, float y0);

    default void drawPoint(Vector2 pos0) {
        this.drawPoint(pos0.x(), pos0.y());
    }

    void drawLine(float x0, float y0, float x1, float y1);

    default void drawLine(Vector2 pos0, Vector2 pos1) {
        this.drawLine(pos0.x(), pos0.y(), pos1.x(), pos1.y());
    }

    void drawTriangle(float x0, float y0, float x1, float y1, float x2, float y2);

    default void drawTriangle(Vector2 pos0, Vector2 pos1, Vector2 pos2) {
        this.drawTriangle(pos0.x(), pos0.y(), pos1.x(), pos1.y(), pos2.x(), pos2.y());
    }

    void drawQuad(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3);

    default void drawQuad(Vector2 pos0, Vector2 pos1, Vector2 pos2, Vector2 pos3) {
        this.drawQuad(pos0.x(), pos0.y(), pos1.x(), pos1.y(), pos2.x(), pos2.y(), pos3.x(), pos3.y());
    }

    default void drawTexture(ITexture t, float x1, float y1, float x2, float y2) {
        this.shapeMode(ShapeMode.FILLED);
        this.enableTexture(true);
        this.texture(t);
        this.drawRect(x1, y1, x2, y2);
    }

    default void drawTexture(ITexture t, float x, float y) {
        this.drawTexture(t, x, y, x + t.getSize().x(), y + t.getSize().y());
    }

    default void drawTexture(ITexture t, float x, float y, Anchor anchor) {
        switch(anchor) {
            case CENTER_LEFT:
            case CENTER_CENTER:
            case CENTER_RIGHT:
                y -= t.getSize().y() / 2;
                break;
            case BOTTOM_LEFT:
            case BOTTOM_CENTER:
            case BOTTOM_RIGHT:
                y -= t.getSize().y();
                break;
            default:
                break;
        }

        switch(anchor) {
            case TOP_CENTER:
            case CENTER_CENTER:
            case BOTTOM_CENTER:
                x -= t.getSize().x() / 2;
                break;
            case TOP_RIGHT:
            case CENTER_RIGHT:
            case BOTTOM_RIGHT:
                x -= t.getSize().x();
                break;
            default:
                break;
        }

        this.drawTexture(t, x, y, x + t.getSize().x(), y + t.getSize().y());
    }

    default void drawTextureScaled(ITexture t, float x, float y, float scale) {
        this.drawTexture(t, x, y, x + t.getSize().x() * scale, y + t.getSize().y() * scale);
    }

    default void drawTextureCropped(ITexture t, float x, float y, float cropX, float cropY) {
        this.drawTexture(t, 0, 0, cropX, cropY, x, y, x + t.getSize().x()
                * cropX, y + t.getSize().y() * cropY);
    }

    default void drawTexture(ITexture t, float tX1, float tY1, float tX2, float tY2, float x1,
                            float y1, float x2, float y2) {
        this.shapeMode(ShapeMode.FILLED);
        this.enableTexture(true);
        this.texture(t);
        this.primitiveMode(PrimitiveMode.TRIANGLES);
        this.texCoord(tX1, tY1);
        this.vertex(x1, y1);
        this.texCoord(tX1, tY2);
        this.vertex(x1, y2);
        this.texCoord(tX2, tY2);
        this.vertex(x2, y2);
        this.vertex(x2, y2);
        this.texCoord(x2, y1);
        this.vertex(x2, y1);
        this.texCoord(tX1, tY1);
        this.vertex(x1, y1);
    }

    default void drawTextureRotated(ITexture t, float x, float y, float angle, float scale) {
        this.shapeMode(ShapeMode.FILLED);
        this.enableTexture(true);
        this.texture(t);
        
        float wh = t.getSize().x()/2f*scale;
        float hh = t.getSize().y()/2f*scale;
        
        float x0 = -wh;
        float y0 = -hh;
        float x1 = wh;
        float y1 = -hh;
        float x2 = wh;
        float y2 = hh;
        float x3 = -wh;
        float y3 = hh;
        
        angle = (float) (angle / 180*Math.PI);
        
        float sina = (float) Math.sin(angle);
        float cosa = (float) Math.cos(angle);
        
        float tx, ty; 
        
        tx = x0*cosa - y0*sina;
        ty = x0*sina + y0*cosa;
        x0 = tx;
        y0 = ty;
        
        tx = x1*cosa - y1*sina;
        ty = x1*sina + y1*cosa;
        x1 = tx;
        y1 = ty;
        
        tx = x2*cosa - y2*sina;
        ty = x2*sina + y2*cosa;
        x2 = tx;
        y2 = ty;
        
        tx = x3*cosa - y3*sina;
        ty = x3*sina + y3*cosa;
        x3 = tx;
        y3 = ty;
        
        this.primitiveMode(PrimitiveMode.TRIANGLES);
        this.texCoord(0, 0);
        this.vertex(x0+x, y0+y);
        this.texCoord(1, 0);
        this.vertex(x1+x, y1+y);
        this.texCoord(1, 1);
        this.vertex(x2+x, y2+y);
        
        this.texCoord(0, 0);
        this.vertex(x0+x, y0+y);
        this.texCoord(1, 1);
        this.vertex(x2+x, y2+y);
        this.texCoord(0, 1);
        this.vertex(x3+x, y3+y);
        
    }
    
    default void drawTexture(ITexture t, float tX1, float tY1, float tX2, float tY2, float x, float y) {
        this.drawTexture(t, tX1, tY1, tX2, tY2, x, y, x + t.getSize().x(), y
                + t.getSize().y());
    }

    default void drawTexture(ITexture t, Vector2 pos) {
        this.drawTexture(t, pos.x(), pos.y());
    }

    default void drawTexture(ITexture t, Vector2 pos0, Vector2 pos1) {
        this.drawTexture(t, pos0.x(), pos0.y(), pos1.x(), pos1.y());
    }

    default void drawTexture(ITexture t, Vector2 texPos0, Vector2 texPos1,
                                    Vector2 pos0, Vector2 pos1) {
        this.drawTexture(t, texPos0.x(), texPos0.y(), texPos1.x(), texPos1.y(),
                pos0.x(), pos0.y(), pos1.x(), pos1.y());
    }

    default void drawTexture(ITexture t, Vector2 texPos0, Vector2 texPos1,
                                    Vector2 pos) {
        this.drawTexture(t, texPos0.x(), texPos0.y(), texPos1.x(), texPos1.y(),
                pos.x(), pos.y());
    }

    default void drawTextureCropped(ITexture t, Vector2 pos, float cropX,
                                           float cropY) {
        this.drawTextureCropped(t, pos.x(), pos.y(), cropX, cropY);
    }

    default void drawTextureCropped(ITexture t, Vector2 pos, Vector2 crop) {
        this.drawTextureCropped(t, pos.x(), pos.y(), crop.x(), crop.y());
    }


    /**
     * Renders circle
     *
     * @param x X of circle's center
     * @param y Y of circle's center
     * @param r R
     */
    void drawCircle(float x, float y, float r);

    default void drawCircle(Vector2 center, float r) {
        this.drawCircle(center.x(), center.y(), r);
    }

    void drawArc(float x, float y, float r, float startAngle, float angle);

    default void drawArc(Vector2 center, float r, float startAngle, float angle) {
        this.drawArc(center.x(), center.y(), r, startAngle, angle);
    }

    default void drawRect(Rectangle rect) {
        this.drawRect(rect.x1(), rect.y1(), rect.x2(), rect.y2());
    }

    /**
     * Draws rectangle
     *
     * @param pos0 Top left corner point
     * @param pos1 Bottom right corner point
     */
    default void drawRect(Vector2 pos0, Vector2 pos1) {
        this.drawRect(pos0.x(), pos0.y(), pos1.x(), pos1.y());
    }

    void drawRect(float x0, float y0, float x1, float y1);

    void drawSegCircle(float x, float y, float r, float segs);

    default void drawSegCircle(Vector2 pos, float r, float segs) {
        this.drawSegCircle(pos.x(), pos.y(), r, segs);
    }

    void drawSegArc(float x, float y, float r, float startAngle, float angle, float segs);

    default void drawSegArc(Vector2 pos, float r, float startAngle, float angle, float segs) {
        this.drawSegArc(pos.x(), pos.y(), r, startAngle, angle, segs);
    }

    void drawText(float x, float y, float size, String font, String text);

    default void drawText(Vector2 pos, float size, String font, String text) {
        this.drawText(pos.x(), pos.y(), size, font, text);
    }

    void fbo(IFbo fbo);

    void passthroughShader();

    void setupShader(IShader shader, boolean gabeSamplers);

    void autoScaleFbo(boolean enable);

    void bindFboTexture(IFbo fbo, int texn, int tmu);

    void drawFullscreenQuad();
    
    void setFrontFace(boolean cw);
    
    /**
     * Modes of primitive rendering
     */
    enum PrimitiveMode {
        POINTS(),
        LINES(),
        LINE_LOOP(),
        LINE_STRIP(),
        TRIANGLES(),
        TRIANGLE_FAN(),
        TRIANGLE_STRIP();

        private int glPrimitive;

        public static PrimitiveMode getForGl(int gl) {
            for (PrimitiveMode primitiveMode : PrimitiveMode.values()) {
                if (primitiveMode.glPrimitive == gl)
                    return primitiveMode;
            }

            return null;
        }

        public static void init() {
            POINTS.glPrimitive = GlConstants.POINTS;
            LINES.glPrimitive = GlConstants.LINES;
            LINE_LOOP.glPrimitive = GlConstants.LINE_LOOP;
            LINE_STRIP.glPrimitive = GlConstants.LINE_STRIP;
            TRIANGLES.glPrimitive = GlConstants.TRIANGLES;
            TRIANGLE_FAN.glPrimitive = GlConstants.TRIANGLE_FAN;
            TRIANGLE_STRIP.glPrimitive = GlConstants.TRIANGLE_STRIP;
        }

        public int getGl() {
            return glPrimitive;
        }
    }

    /**
     * Modes of shape rendering
     */
    enum ShapeMode {
        OUTLINE, FILLED
    }
}