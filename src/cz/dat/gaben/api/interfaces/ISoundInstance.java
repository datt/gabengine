package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.util.Vector3;

public interface ISoundInstance {
    ISound getSound();

    boolean isFreeToUse();

    void init();

    void play();

    void pause();

    void stop();

    void seek(float time);

    float getPosition();

    void setPosition(float time);

    boolean isLooping();

    void setLooping(boolean looping);

    float getGain();

    void setGain(float gain);

    float getPitch();

    void setPitch(float pitch);

    void setSpacePosition(float x, float y, float z);

    default void setSpacePosition(Vector3 pos) {
        this.setSpacePosition(pos.x(), pos.y(), pos.z());
    }

    Vector3 getSpacePosition();

    void setSpaceVelocity(float x, float y, float z);

    default void setSpaceVelocity(Vector3 pos) {
        this.setSpaceVelocity(pos.x(), pos.y(), pos.z());
    }

    Vector3 getSpaceVelocity();

    void setSoundPositioning(boolean relativeToListener);

    boolean isPositionedRelativeToListener();

    void setDirection(float x, float y, float z);

    default void setDirection(Vector3 dir) {
        this.setDirection(dir.x(), dir.y(), dir.z());
    }

    Vector3 getDirection();

    void setCone(float innerAngle, float outerAngle, float outerGain);

    default void setCone(Vector3 cone) {
        this.setCone(cone.x(), cone.y(), cone.z());
    }

    Vector3 getCone();

    ISound.SoundState getState();
}
