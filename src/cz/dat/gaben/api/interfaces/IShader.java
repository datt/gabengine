package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.util.Matrix2;
import cz.dat.gaben.util.Matrix3;
import cz.dat.gaben.util.Matrix4;

public interface IShader {
	
    public int getProgramId();
    
    public String getName();
	
	public void setUniform1f(String name, float val1);

    public void setUniform2f(String name, float val1, float val2);

    public void setUniform3f(String name, float val1, float val2, float val3);

    public void setUniform4f(String name, float val1, float val2, float val3, float val4);

    public void setUniform1i(String name, int val1);

    public void setUniform2i(String name, int val1, int val2);

    public void setUniform3i(String name, int val1, int val2, int val3);

    public void setUniform4i(String name, int val1, int val2, int val3, int val4);

    public void setUniformMatrix2f(String name, Matrix2 matrix); 

    public void setUniformMatrix3f(String name, Matrix3 matrix); 
    
    public void setUniformMatrix4f(String name, Matrix4 matrix); 
    
    public boolean hasUniform(String name);
    
    public boolean hasAttribute(String name);
    
    public int getAttribLocation(String name);
    
}
