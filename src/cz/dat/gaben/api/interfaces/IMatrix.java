package cz.dat.gaben.api.interfaces;

public interface IMatrix {

	public void setData(float[] data);
	
	public float[] getData();
	
}
