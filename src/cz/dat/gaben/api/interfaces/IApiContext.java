package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.api.Settings;

public interface IApiContext {
    IRenderer getRenderer();

    ITextureManager getTexture();

    IFontManager getFont();

    IFontRenderer getFontRenderer();

    ISoundManager getSound();

    IInputManager getInput();

    IShaderManager getShader();
    
    IFboManager getFbo();
    
    Settings getSettings();

    void cleanup();
}
