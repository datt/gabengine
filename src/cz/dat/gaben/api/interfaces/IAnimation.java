package cz.dat.gaben.api.interfaces;

public interface IAnimation extends ITickListener {
    void enable();

    void disable();
}
