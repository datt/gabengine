package cz.dat.gaben.api.interfaces;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.List;
import java.util.function.Function;

public interface IContentManager {
    void load();

    InputStream openStream(String name);

    int getSize(String name);

    List<String> getFilesInDirectory(String name);

    List<String> getDirectoriesInDirectory(String name);

    void cleanup();

    default ByteBuffer readToByteBuffer(String name, boolean direct) throws IOException {
        return this.readToByteBuffer(name, (b) -> direct ? ByteBuffer.allocateDirect(b) :
                ByteBuffer.allocate(b));
    }

    default ByteBuffer readToByteBuffer(String name, Function<Integer, ByteBuffer> bbSupplier) throws IOException {
        ByteBuffer buffer;
        int bytesTotal = this.getSize(name);

        try (InputStream source = this.openStream(name);
             ReadableByteChannel rbc = Channels.newChannel(source)
        ) {

            buffer = bbSupplier.apply(bytesTotal);

            while (true) {
                int bytes = rbc.read(buffer);
                if (bytes == -1)
                    break;
                if (buffer.remaining() == 0) {
                    bytesTotal *= 2;
                    ByteBuffer newBuffer = bbSupplier.apply(bytesTotal);
                    buffer.flip();
                    newBuffer.put(buffer);
                    buffer = newBuffer;
                }
            }
        }


        buffer.flip();
        return buffer;
    }
}
