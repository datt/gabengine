package cz.dat.gaben.api.interfaces;

public interface IShaderManager {

	public void loadShader(String name, String pathv, String pathf);
	public IShader getShader(String name);
	
}
