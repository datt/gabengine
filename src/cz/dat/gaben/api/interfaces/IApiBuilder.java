package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowBase;

public interface IApiBuilder {
    /**
     * Sets resource package for built game windows
     *
     * @param contentManager Content manager instance
     * @see GameWindowBase#GameWindowBase(int, int, cz.dat.gaben.api.interfaces.IContentManager, cz.dat.gaben.api.game.Game)
     */
    void setContentManager(IContentManager contentManager);

    /**
     * Sets window size for built game windows
     *
     * @param width  Window width
     * @param height Window height
     * @see GameWindowBase#GameWindowBase(int, int, cz.dat.gaben.api.interfaces.IContentManager, cz.dat.gaben.api.game.Game)
     */
    void setWindowSize(int width, int height);

    void setGame(Game game);

    /**
     * Tries to build <code>GameWindowBase</code> for current implementation.
     * Does not checks if it can be built.
     *
     * @return New window. <code>null</code> if window build failed.
     */
    GameWindowBase buildGameWindow();

    /**
     * Checks if <code>GameWindowBase</code> of current implementation can be built
     *
     * @return True if it can be built
     */
    boolean canBeBuilt();
}
