package cz.dat.gaben.api.interfaces;

public interface IFbo {
	public int getTextureId(int n);
	public int getFboId();
	
	public int getWidth();
	public int getHeight();
	
	public int getAttachmentCount();
}
