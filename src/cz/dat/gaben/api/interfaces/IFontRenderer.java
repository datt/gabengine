package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Vector2;

public interface IFontRenderer {

	IFont getFont();

	void setFont(IFont font);

	float getSize();

	void setSize(float size);

	Anchor getAnchor();

	void setAnchor(Anchor anchor);

	void drawString(String string, float x, float y);

	default void drawString(String string, Vector2 pos) {
		drawString(string, pos.x(), pos.y());
	}

	float getStringWidth(String string);

	float getStringHeight(String string);

	float getMaxHeight();
}