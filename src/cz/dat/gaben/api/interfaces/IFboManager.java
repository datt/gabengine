package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.api.Settings;

public interface IFboManager {

	public void createFbo(String name, int width, int height, int attachments, Settings s);
	public IFbo getFbo(String name);
	
}
