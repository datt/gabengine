package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.util.Vector2;

import java.util.Map;

public interface ITextureManager {
    void cleanup();

    ITexture getTexture(String id);

    ITexture getTexture(String spritesheetId, String id);

    Map<String, ? extends ITexture> getSpritesheet(String spritesheetId);

    /**
     * Loads one texture file
     *
     * @param id   ID of loaded texture
     * @param contentPath Name of texture in used content manager
     */
    void loadTexture(String id, String contentPath);

    /**
     * Loads spritesheet from one file (images numbered by x-position)
     *
     * @param spritesheetId ID of created spritesheet
     * @param contentPath          Path of spritesheet file
     * @param spriteSize    Size of one image
     */
    void loadSpritesheet(String spritesheetId, String contentPath, Vector2 spriteSize);

    /**
     * Loads spritesheet from multiple files
     *
     * @param spritesheetId ID of created spritesheet
     * @param contentPathMask Path mask (use %n for image number, e.g "images/player_%n", starting with <b>zero</b>)
     * @param numberOfFiles Number of loaded files
     */
    void loadSpritesheet(String spritesheetId, String contentPathMask, int numberOfFiles);

    /**
     * Loads one texture file
     *
     * @param id          ID of loaded texture
     * @param contentPath Name of texture in used content manager
     * @param settings    Custom settings for texture
     */
    void loadTexture(String id, String contentPath, Settings settings);

    /**
     * Loads spritesheet from one file (images numbered by x-position)
     *
     * @param spritesheetId ID of created spritesheet
     * @param contentPath   Path of spritesheet file
     * @param spriteSize    Size of one image
     * @param settings      Map with custom settings for each texture
     */
    void loadSpritesheet(String spritesheetId, String contentPath, Vector2 spriteSize, Map<String, Settings> settings);
    
    void loadSpritesheet(String spritesheetId, String contentPath, Vector2 spriteSize, Settings settings);

    /**
     * Loads spritesheet from multiple files
     *
     * @param spritesheetId   ID of created spritesheet
     * @param contentPathMask Path mask (use %n for image number, e.g "images/player_%n", starting with <b>zero</b>)
     * @param numberOfFiles   Number of loaded files
     * @param settings        Map with custom settings for each texture
     */
    void loadSpritesheet(String spritesheetId, String contentPathMask, int numberOfFiles, Map<String, Settings> settings);

    /**
     * Finishes texture loading
     *
     * @return Loaded successfully
     */
    boolean finishLoading();
}
