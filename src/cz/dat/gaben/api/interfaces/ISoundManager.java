package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.util.Vector3;

public interface ISoundManager {

    void cleanup();

    void setListenerPosition(float x, float y, float z);

    Vector3 getListenerPosition();

    void setListenerVelocity(float x, float y, float z);

    Vector3 getListenerVelocity();

    void setListenerOrientation(float atX, float atY, float atZ, float upX, float upY, float upZ);

    Vector3 getListenerOrientationAt();

    Vector3 getListenerOrientationUp();

    float getGain();

    void setGain(float gain);

    ISound getSound(String name);

    ISound getMusic(String name);

    default ISoundInstance playSoundAlways(String name, float localGain, float pitch, boolean looping) {
        return this.playSound(name, localGain, pitch, looping, ISound.ALWAYS_PLAY_PRIORITY);
    }

    default ISoundInstance playSoundAlways(String name, float localGain, float pitch) {
        return this.playSoundAlways(name, localGain, pitch, false);
    }

    default ISoundInstance playSoundAlways(String name) {
        return this.playSoundAlways(name, 1f, 1f);
    }

    default ISoundInstance playSound(String name, float localGain, float pitch, boolean looping) {
        return this.playSound(name, localGain, pitch, looping, 0);
    }

    default ISoundInstance playSound(String name, float localGain, float pitch) {
        return this.playSound(name, localGain, pitch, false);
    }

    default ISoundInstance playSound(String name) {
        return this.playSound(name, 1f, 1f);
    }

    ISoundInstance playSound(String name, float localGain, float pitch, boolean looping, int priority);

    void stopSounds(String name);

    void stopAllSounds();

    ISoundInstance playMusic(String name, float localGain, boolean looping, boolean solo);

    void stopMusic(String name);

    void stopAllMusics();

    void addSound(String name, String contentPath);

    void addMusic(String name, String contentPath);

    void finishLoading();
}
