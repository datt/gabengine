package cz.dat.gaben.api.interfaces;

import cz.dat.gaben.util.Vector2;

public interface ITexture {
    String getTextureName();

    Vector2 getSize();
}
