package cz.dat.gaben.api.impl;

import cz.dat.gaben.api.interfaces.IContentManager;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DummyContentManager implements IContentManager {

    @Override
    public void load() {
    }

    @Override
    public InputStream openStream(String name) {
        return null;
    }

    @Override
    public int getSize(String name) {
        return -1;
    }

    @Override
    public List<String> getFilesInDirectory(String name) {
        return new ArrayList<>(1);
    }

    @Override
    public List<String> getDirectoriesInDirectory(String name) {
        return new ArrayList<>(1);
    }

    @Override
    public void cleanup() {

    }
}
