package cz.dat.gaben.api.impl;

import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.util.GabeLogger;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExceptionHandling(value = "CONTENT", errorCode = 203)
public class ContentManager implements IContentManager {

    private boolean cp = true;
    private String dir;
    private File dirFile;
    private Map<String, FileInfo> files;
    private List<String> directories;

    public ContentManager(String directory, boolean classPath)
            throws FileNotFoundException {
        if (classPath) {
            if (!directory.startsWith("/"))
                directory = "/" + directory;

            if (!directory.endsWith("/"))
                directory = directory + "/";
        } else {
            File f = new File(directory);
            if (!f.isDirectory())
                throw new FileNotFoundException("Path is not directory");

            if (!f.exists())
                throw new FileNotFoundException("Directory doesn't exist");

            this.dirFile = f;
        }

        this.dir = directory;
        this.cp = classPath;
        this.files = new HashMap<>();
        this.directories = new ArrayList<>();
    }

    public void loadFile(String name, String file, boolean fromClassPath)
            throws FileNotFoundException {
        GabeLogger.info("Loading " + file + " as " + name);
        FileInfo f;

        if (fromClassPath) {
            f = new FileInfo(name, file, true);

            if (this.getClass().getResourceAsStream(file) == null)
                throw new FileNotFoundException(file);
        } else {
            File fileToLoad = new File(file);

            if (fileToLoad.isDirectory() || !fileToLoad.exists()) {
                throw new FileNotFoundException(file);
            }

            f = new FileInfo(name, fileToLoad.getAbsolutePath(), false);
        }

        this.files.put(name, f);
    }

    public void loadAllFiles() {
        if (this.cp) {
            GabeLogger.info("Starting loading from JAR file");

            File jarFile = null;
            try {
                jarFile = new File(this.getClass().getProtectionDomain()
                        .getCodeSource().getLocation().toURI());
            } catch (URISyntaxException e) {
                throw ExceptionUtil.nonBreakingException(e, this);
            }

            GabeLogger.info("Loading from JAR " + jarFile.getAbsolutePath());

            if (jarFile.isFile()) {
                try {
                    GabeLogger.info("Got to jarfile loading");
                    URI uri = ContentManager.class.getResource(this.dir).toURI();
                    Path myPath;

                    if (uri.getScheme().equals("jar")) {
                        FileSystem fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap());
                        myPath = fileSystem.getPath(this.dir);
                    } else {
                        myPath = Paths.get(uri);
                    }

                    Stream<Path> walk = Files.walk(myPath, Integer.MAX_VALUE);

                    for (Iterator<Path> it = walk.iterator(); it.hasNext(); ) {
                        String name = it.next().toString();

                        try {
                            this.loadFile(this.getFinalName(name.replace(this.dir, "")), name, true);
                        } catch (FileNotFoundException e) {
                            throw ExceptionUtil.nonBreakingException("Couldn't load " + name, e, this);
                        }
                    }

                } catch (IOException | URISyntaxException e) {
                    throw ExceptionUtil.nonBreakingException(e, this);
                }

                return;
            }
        }

        try {
            GabeLogger.info("Starting loading from directory");

            final URL url = this.getClass().getResource(this.dir);
            final File fileDir;

            if (this.cp && url != null) {
                GabeLogger.info(url.toString());
                GabeLogger.info(url.toExternalForm());
                fileDir = new File(url.toURI());
                this.dirFile = fileDir;
            } else {
                fileDir = this.dirFile;
            }

            this.loadDir(fileDir, "");
        } catch (URISyntaxException e) {
            throw ExceptionUtil.nonBreakingException(e, this);
        }
    }

    private void loadDir(File fileDir, String prefix) {
        this.directories.add(prefix);

        for (File app : fileDir.listFiles()) {
            String name = app.getName();

            if (app.isFile()) {
                try {
                    this.loadFile(prefix + this.getFinalName(name), app.getAbsolutePath(), false);
                } catch (FileNotFoundException e) {
                    throw ExceptionUtil.nonBreakingException("Error loading file", e, this);
                }
            } else {
                this.loadDir(app, prefix + app.getName() + "/");
            }
        }
    }

    private String getFinalName(String fileName) {
        if (fileName.contains(".")) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        }
    }

    public boolean isInClasspath(String name) {
        return this.files.get(name).cp;
    }

    public String getPath(String name) {
        return this.files.get(name).path;
    }

    public String getPathWithoutBase(String name) {
        return this.files.get(name).pathWithoutBase;
    }

    @Override
    public void load() {
        this.loadAllFiles();
    }

    public InputStream openStream(String name) {
        FileInfo f = this.files.get(name);

        return f == null ? null : f.openStream();
    }

    @Override
    public int getSize(String name) {
        if (!this.files.containsKey(name))
            return -1;

        return this.files.get(name).sizeBytes;
    }

    @Override
    public List<String> getFilesInDirectory(String name) {
        return this.files.keySet().stream().filter(s -> s.startsWith(name)).collect(Collectors.toList());
    }

    @Override
    public List<String> getDirectoriesInDirectory(String name) {
        return this.directories.stream().filter(s -> s.startsWith(name)).collect(Collectors.toList());
    }

    @Override
    public void cleanup() {

    }

    public String getResourcesDirectory() {
        return this.dir;
    }

    public boolean loadingFromClasspath() {
        return this.cp;
    }

    private class FileInfo {
        boolean cp;
        String path;
        String name;
        String pathWithoutBase;
        int sizeBytes;

        public FileInfo(String fileIdentificator, String path, boolean cp) {
            this.path = path;
            this.cp = cp;
            this.name = fileIdentificator;

            if (path.endsWith(fileIdentificator)) {
                pathWithoutBase = fileIdentificator;
            } else {
                pathWithoutBase = fileIdentificator + path.substring(path.lastIndexOf('.'));
            }

            this.calcSize();
        }

        @ExceptionHandling(errorCode = 204, value = "CONTENT_LOAD")
        private void calcSize() {
            InputStream s = this.openStream();
            try {
                this.sizeBytes = this.getBytes(s).length;
            } catch(IOException e) {
                throw ExceptionUtil.featureBreakingException("Couldn't calculate size of a content", e, this);
            }
        }

        private byte[] getBytes(InputStream is) throws IOException {
            int len;
            int size = 1024;
            byte[] buf;

            if (is instanceof ByteArrayInputStream) {
                size = is.available();
                buf = new byte[size];
                len = is.read(buf, 0, size);
            } else {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                buf = new byte[size];
                while ((len = is.read(buf, 0, size)) != -1)
                    bos.write(buf, 0, len);
                buf = bos.toByteArray();
            }
            return buf;
        }

        public InputStream openStream() {
            if (this.cp) {
                return this.getClass().getResourceAsStream(this.path);
            } else {
                try {
                    return new FileInputStream(this.path);
                } catch (FileNotFoundException e) {
                    throw ExceptionUtil.nonBreakingException("Couldn't open file for reading (perhaps it was deleted after startup?)", e, this);
                }
            }
        }
    }

}
