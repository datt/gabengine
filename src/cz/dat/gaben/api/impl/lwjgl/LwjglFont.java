package cz.dat.gaben.api.impl.lwjgl;

import java.util.Map;

import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.util.Vector2;

public class LwjglFont implements IFont {

	private String name;
	private String spriteSheetId;
	private float nominalSize;
	private FontStyle style;
	private FontDecoration[] decorations;
	private float maxHeight;
	Map<Character, Vector2> charSizes;
	
	public LwjglFont(String name, String spriteSheetId, float nominalSize, FontStyle style, FontDecoration[] decorations, Map<Character, Vector2> charSizes, float maxHeight) {
		this.name = name;
		this.spriteSheetId = spriteSheetId;
		this.nominalSize = nominalSize;
		this.style = style;
		this.decorations = decorations;
		this.charSizes = charSizes;
		this.maxHeight = maxHeight;
	}
	
	@Override
	public float getStringWidth(String string) {		
		int strLen = string.length();
		
		float total = 0;
		for (int i = 0; i < strLen; i++) {
			char c = string.charAt(i);
			if (c < LwjglFontManager.CHARACTER_COUNT) {
				total += this.getCharWidth(c);
			}
		}
		
		return total;
	}

	@Override
	public float getStringHeight(String string) {
		int strLen = string.length();
		
		float max = 0;
		for (int i = 0; i < strLen; i++) {
			char c = string.charAt(i);
			if (c < LwjglFontManager.CHARACTER_COUNT) {
				max = Math.max(max, this.getCharHeight(c));
			}
		}
		
		return max;
	}

	@Override
	public float getMaxHeight() {
		return this.maxHeight;
	}

	@Override
	public FontStyle getFontStyle() {
		return this.style;
	}

	@Override
	public FontDecoration[] getFontDecorations() {
		return this.decorations;
	}

	@Override
	public float getNominalSize() {
		return this.nominalSize;
	}
	
	public String getSpriteSheetId() {
		return this.spriteSheetId;
	}
	
	public String getName() {
		return this.name;
	}

	@Override
	public float getCharWidth(char c) {
		return this.charSizes.get(c).x();
	}

	@Override
	public float getCharHeight(char c) {
		return this.charSizes.get(c).y();
	}

}
