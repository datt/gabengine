package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.interfaces.IApiBuilder;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.OSUtil;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class LwjglBuilder implements IApiBuilder {

    private IContentManager resPkg;
    private int wWidth, wHeight;
    private Game game;

    public LwjglBuilder() {
        // LWJGL, why would you do such a thing?
        if(!OSUtil.isWindows() && !OSUtil.isMac()) {
            try {
                System.out.println("Doing hell");

                Class c = Class.forName("org.lwjgl.system.SharedLibraryLoader");
                Method m = c.getDeclaredMethod("load", String.class);
                m.setAccessible(true);
                m.invoke(null, "jemalloc");

                System.out.println("Imma what now?");
            } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setContentManager(IContentManager manager) {
        this.resPkg = manager;
    }

    @Override
    public void setWindowSize(int width, int height) {
        this.wWidth = width;
        this.wHeight = height;
    }

    @Override
    public void setGame(Game game) {
        this.game = game;
    }

    @Deprecated
    @Override
    public boolean canBeBuilt() {
        //return true; // Assume that everything works :P
        
        // If the window can't be built, the game will later crash gracefully anyway.
    	// That means that this method is redundant.

    	try {	
            GabeLogger.info("Testing window creation", this, "canBeBuilt");

            if (!GLFW.glfwInit()) {
                GabeLogger.error("GLFW init failed.", this, "canBeBuilt");
                return false;
            }

            GLFW.glfwDefaultWindowHints();

            GLFW.glfwWindowHint(GLFW.GLFW_SAMPLES, 1);
            GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GL11.GL_FALSE);

            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR,
                    LwjglGameWindow.VERSION_MAJOR);
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR,
                    LwjglGameWindow.VERSION_MINOR);
            GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT,
                    GLFW.GLFW_TRUE);

            if (LwjglGameWindow.VERSION_MAJOR >= 4 || (LwjglGameWindow.VERSION_MAJOR == 3 && LwjglGameWindow.VERSION_MINOR >= 3))
                GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);

            long hndl = GLFW.glfwCreateWindow(1, 1, "-", 0, 0);
            if (hndl == 0) {
                GabeLogger.error("GLFW window creation failed.", this, "canBeBuilt");
                return false;
            }

            GLFW.glfwDestroyWindow(hndl);
            GLFW.glfwTerminate();

            GabeLogger.info("LWJGL can be built.", this, "canBeBuilt");
            return true;
        } catch (Exception e) {
            GabeLogger.error(ExceptionUtil.gameBreakingException(e, this));
            return false;
        }
    }

    @Override
    public GameWindowBase buildGameWindow() {
        if (this.resPkg == null || this.game == null)
            throw new IllegalStateException("Resources package and game must be defined");

        return new LwjglGameWindow(this.wWidth, this.wHeight, this.resPkg, this.game);
    }
}
