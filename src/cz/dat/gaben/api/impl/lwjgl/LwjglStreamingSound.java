package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.api.interfaces.ISound;
import cz.dat.gaben.api.interfaces.ISoundInstance;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.Vector3;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.stb.STBVorbis;
import org.lwjgl.stb.STBVorbisInfo;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LwjglStreamingSound implements ISound {

    LwjglSoundManager manager;
    Vector3 spacePos = Vector3.ZERO, spaceVel = Vector3.ZERO, direction = Vector3.ZERO,
            cone = new Vector3(360f, 360f, 0f);
    boolean relativeToListener = false;
    float gain = 1f, pitch = 1f;
    boolean looping = false;
    float length, bitrate, sampleFreq;
    IntBuffer buffers;
    Decoder d;
    private LwjglStreamingSoundInstance soundInstance;
    private List<ISoundInstance> playingInstances = new LinkedList<>();
    private IContentManager content;
    private String contentPath;

    LwjglStreamingSound(LwjglSoundManager man, String contentPath) {
        this.manager = man;
        this.contentPath = contentPath;
        this.content = man.context.getContentManager();
    }

    void dispose() {
        this.stopAll();

        if (this.d != null)
            STBVorbis.stb_vorbis_close(this.d.handle);

        AL10.alDeleteBuffers(this.buffers);

        if (this.soundInstance != null) {
            AL10.alDeleteSources(this.soundInstance.source);
        }
    }

    public void update() {
        if (this.soundInstance != null)
            this.d.update(this.soundInstance.source, this.soundInstance.isLooping());
    }

    @Override
    public void init() {
        this.buffers = BufferUtils.createIntBuffer(2);
        AL10.alGenBuffers(this.buffers);
        this.d = new Decoder(this.content, this.contentPath);
        System.out.println("INIT DONE");

        this.manager.checkALError("init");
    }

    @Override
    public float getLength() {
        return this.length;
    }

    @Override
    public float getBitrate() {
        return this.bitrate;
    }

    @Override
    public float getSampleFrequency() {
        return this.sampleFreq;
    }

    @Override
    public ISoundInstance play(int priority, boolean applyProp) {
        LwjglStreamingSoundInstance i = (LwjglStreamingSoundInstance) this.getFreeInstance(priority);
        if (i != null) {
            if (applyProp) {
                i.priority = priority;
                i.setCone(this.cone);
                i.setDirection(this.direction);
                i.setSpacePosition(this.spacePos);
                i.setSpaceVelocity(this.spaceVel);
                i.setGain(this.gain);
                i.setLooping(this.looping);
                i.setPitch(this.pitch);
                i.setSoundPositioning(this.relativeToListener);
            }
            i.play();
            this.playingInstances.add(i);
        }
        return i;
    }

    @Override
    public ISoundInstance getFreeInstance(int priority) {
        if (this.soundInstance == null) {
            this.soundInstance = new LwjglStreamingSoundInstance(this);
            this.soundInstance.init();
            return this.soundInstance;
        }

        if (this.soundInstance.getState() == SoundState.STOPPED || priority == ISound.ALWAYS_PLAY_PRIORITY ||
                (this.soundInstance.priority < priority &&
                        this.soundInstance.priority != ISound.ALWAYS_PLAY_PRIORITY)) {

            this.soundInstance.stop();
            return this.soundInstance;
        }

        return null;
    }

    @Override
    public List<ISoundInstance> getAllPlayingInstances() {
        for (Iterator<ISoundInstance> iterator = this.playingInstances.iterator(); iterator.hasNext(); ) {
            ISoundInstance playingInstance = iterator.next();
            if (playingInstance.getState() != SoundState.PLAYING)
                iterator.remove();
        }

        return this.playingInstances;
    }

    @Override
    public void stopAll() {
        if (this.soundInstance != null)
            this.soundInstance.stop();
    }

    @Override
    public boolean isStreaming() {
        return true;
    }

    @Override
    public boolean isLooping() {
        return this.looping;
    }

    @Override
    public void setLooping(boolean looping) {
        this.looping = looping;
    }

    @Override
    public float getGain() {
        return this.gain;
    }

    @Override
    public void setGain(float gain) {
        this.gain = gain;
    }

    @Override
    public float getPitch() {
        return this.pitch;
    }

    @Override
    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    @Override
    public void setSpacePosition(float x, float y, float z) {
        this.spacePos = new Vector3(x, y, z);
    }

    @Override
    public Vector3 getSpacePosition() {
        return this.spacePos;
    }

    @Override
    public void setSpaceVelocity(float x, float y, float z) {
        this.spaceVel = new Vector3(x, y, z);
    }

    @Override
    public Vector3 getSpaceVelocity() {
        return this.spaceVel;
    }

    @Override
    public void setSoundPositioning(boolean relativeToListener) {
        this.relativeToListener = relativeToListener;
    }

    @Override
    public boolean isPositionedRelativeToListener() {
        return this.relativeToListener;
    }

    @Override
    public void setDirection(float x, float y, float z) {
        this.direction = new Vector3(x, y, z);
    }

    @Override
    public Vector3 getDirection() {
        return this.direction;
    }

    @Override
    public void setCone(float innerAngle, float outerAngle, float outerGain) {
        this.cone = new Vector3(innerAngle, outerAngle, outerGain);
    }

    @Override
    public Vector3 getCone() {
        return this.cone;
    }

    class Decoder {

        private static final int BUFFER_SIZE = 4096 * 4;

        final ByteBuffer vorbis;

        final long handle;
        final int channels;
        final int sampleRate;
        final int format;

        final int lengthSamples;
        final float lengthSeconds;

        final ShortBuffer pcm;

        int samplesLeft;
        boolean shouldExit;

        Decoder(IContentManager content, String path) {
            try {
                vorbis = content.readToByteBuffer(path, true);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            IntBuffer error = BufferUtils.createIntBuffer(1);
            handle = STBVorbis.stb_vorbis_open_memory(vorbis, error, null);
            if (handle == 0)
                throw new RuntimeException("Failed to open Ogg Vorbis file. Error: " + error.get(0));

            try (STBVorbisInfo info = STBVorbisInfo.malloc()) {
                STBVorbis.stb_vorbis_get_info(handle, info);

                this.channels = info.channels();
                this.sampleRate = info.sample_rate();
            }

            this.format = getFormat(channels);

            this.lengthSamples = STBVorbis.stb_vorbis_stream_length_in_samples(handle);
            this.lengthSeconds = STBVorbis.stb_vorbis_stream_length_in_seconds(handle);

            this.pcm = BufferUtils.createShortBuffer(BUFFER_SIZE);

            samplesLeft = lengthSamples;
        }

        private int getFormat(int channels) {
            switch (channels) {
                case 1:
                    return AL10.AL_FORMAT_MONO16;
                case 2:
                    return AL10.AL_FORMAT_STEREO16;
                default:
                    throw new UnsupportedOperationException("Unsupported number of channels: " + channels);
            }
        }

        private boolean stream(int buffer) {
            int samples = 0;

            while (samples < BUFFER_SIZE) {
                pcm.position(samples);
                int samplesPerChannel = STBVorbis.stb_vorbis_get_samples_short_interleaved(handle, channels, pcm);
                if (samplesPerChannel == 0)
                    break;

                samples += samplesPerChannel * channels;
            }

            if (samples == 0)
                return false;

            pcm.position(0);
            AL10.alBufferData(buffer, format, pcm, sampleRate);
            samplesLeft -= samples / channels;

            return true;
        }

        float getProgress() {
            return 1.0f - samplesLeft / (float) (lengthSamples);
        }

        float getProgressTime() {
            return this.getProgress() * lengthSeconds;
        }

        void rewind() {
            STBVorbis.stb_vorbis_seek_start(handle);
            samplesLeft = lengthSamples;
        }

        void skip(int direction) {
            seek(Math.min(Math.max(0, STBVorbis.stb_vorbis_get_sample_offset(handle) + direction * sampleRate), lengthSamples));
        }

        void skipTo(float offset0to1) {
            seek(Math.round(lengthSamples * offset0to1));
        }

        private void seek(int sample_number) {
            STBVorbis.stb_vorbis_seek(handle, sample_number);
            samplesLeft = lengthSamples - sample_number;
        }

        private void checkALError(String method) {
            int e;
            if ((e = AL10.alGetError()) != AL10.AL_NO_ERROR) {
                GabeLogger.error(AL10.alGetString(e), this, method);
            }
        }

        void removeBuffers(int source) {
            int queued = AL10.alGetSourcei(source, AL10.AL_BUFFERS_QUEUED);
            while (queued > 0) {
                AL10.alSourceUnqueueBuffers(source);
                queued--;
            }
        }

        boolean play(int source, IntBuffer buffers) {
            this.removeBuffers(source);
            this.rewind();


            for (int i = 0; i < buffers.limit(); i++) {
                if (!stream(buffers.get(i)))
                    return false;
            }

            AL10.alSourceQueueBuffers(source, buffers);
            this.checkALError("play.sourceQueueBuffers");
            AL10.alSourcePlay(source);
            this.checkALError("play.sourcePlay");
            this.shouldExit = false;

            return true;
        }

        boolean update(int source, boolean loop) {
            int processed = AL10.alGetSourcei(source, AL10.AL_BUFFERS_PROCESSED);

            for (int i = 0; i < processed; i++) {
                int buffer = AL10.alSourceUnqueueBuffers(source);

                if (!stream(buffer)) {
                    boolean shouldExit = true;

                    if (loop) {
                        rewind();
                        shouldExit = !stream(buffer);
                    }

                    if (shouldExit)
                        return false;
                }

                AL10.alSourceQueueBuffers(source, buffer);
            }

            if (this.shouldExit) {
                AL10.alSourceStop(source);
                return false;
            }

            if (processed == 2)
                AL10.alSourcePlay(source);

            return true;
        }

    }
}
