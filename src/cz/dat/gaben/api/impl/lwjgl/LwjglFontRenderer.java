package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.interfaces.IApiContext;
import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFontRenderer;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.Anchor;

public class LwjglFontRenderer implements IFontRenderer {

    private LwjglFont currentFont = null;

    private float requiredSize = 28;
    private float scale = 1;

    private Anchor anchor = Anchor.TOP_LEFT;

    private IApiContext context;

    public LwjglFontRenderer(IApiContext context) {
        this.context = context;
    }

    @Override
    public IFont getFont() {
        return this.currentFont;
    }

    @Override
    public void setFont(IFont font) {
        if (font != currentFont) {
            this.currentFont = (LwjglFont) font;
            this.updateScale();
        }
    }

    @Override
    public float getSize() {
        return this.requiredSize;
    }

    @Override
    public void setSize(float size) {
        this.requiredSize = size;
        this.updateScale();
    }

    private void updateScale() {
        if (this.currentFont != null) {
            this.scale = this.requiredSize / this.currentFont.getNominalSize();
        }
    }

    @Override
    public Anchor getAnchor() {
        return this.anchor;
    }

    @Override
    public void setAnchor(Anchor anchor) {
        this.anchor = anchor;
    }

    @Override
    public void drawString(String string, float x, float y) {
        switch (this.anchor) {
            case CENTER_LEFT:
            case CENTER_CENTER:
            case CENTER_RIGHT:
                y -= this.getStringHeight(string) / 2;
                break;
            case BOTTOM_LEFT:
            case BOTTOM_CENTER:
            case BOTTOM_RIGHT:
                y -= this.getStringHeight(string);
                break;
            default:
                break;
        }

        switch (this.anchor) {
            case TOP_CENTER:
            case CENTER_CENTER:
            case BOTTOM_CENTER:
                x -= this.getStringWidth(string) / 2;
                break;
            case TOP_RIGHT:
            case CENTER_RIGHT:
            case BOTTOM_RIGHT:
                x -= this.getStringWidth(string);
                break;
            default:
                break;
        }

        int strLen = string.length();

        float offset = 0;

        for (int i = 0; i < strLen; i++) {
            int c = string.charAt(i);
            this.context.getRenderer().drawTextureScaled(this.context.getTexture()
                    .getTexture(this.currentFont.getSpriteSheetId(), "CHAR_" + c), x + offset, y, this.scale);
            offset += this.currentFont.getCharWidth((char) c) * this.scale;
        }
    }

    @Override
    public float getStringWidth(String string) {
        return this.currentFont.getStringWidth(string) * this.scale;
    }

    @Override
    public float getStringHeight(String string) {
        return this.currentFont.getStringHeight(string) * this.scale;
    }

    @Override
    public float getMaxHeight() {
        ITexture texture = this.context.getTexture().getSpritesheet(this.currentFont.getSpriteSheetId()).values().iterator().next();
        return texture.getSize().y() * this.scale;
    }

}
