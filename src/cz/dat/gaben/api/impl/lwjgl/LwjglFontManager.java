package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.Settings.Wrap;
import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.exception.FontLoadingException;
import cz.dat.gaben.api.interfaces.IApiContext;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFont.FontDecoration;
import cz.dat.gaben.api.interfaces.IFont.FontStyle;
import cz.dat.gaben.api.interfaces.IFontManager;
import cz.dat.gaben.util.Vector2;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@ExceptionHandling(value = "FONTS", errorCode = 201)
public class LwjglFontManager implements IFontManager {

	public static final int CHARACTER_COUNT = 128;
	
	private IApiContext context;
	private IContentManager content;
	
	private Map<String, LwjglFont> fonts = new HashMap<>();

	public LwjglFontManager(LwjglGameWindow context) {
		this.context = context;
		this.content = context.getContentManager();
	}

    @Override
    public IFont getFont(String name) {
        return this.fonts.get(name);
    }

	@SuppressWarnings("unchecked")
	@Override
    @ExceptionHandling("FONT_LOAD")
	public void loadFont(String name, String path, float nominalSize, FontStyle style, IFont.FontDecoration[] decorations) {
    	Font awtFont = null;

    	try {
			InputStream s = this.content.openStream(path);
			awtFont = Font.createFont(Font.TRUETYPE_FONT, s);
		} catch (FontFormatException | IOException e) {
			throw ExceptionUtil.featureBreakingException(new FontLoadingException(name, e), this);
		}

    	int awtStyle = Font.PLAIN;
    	
    	switch(style) {
    	case PLAIN:	
    		awtStyle = Font.PLAIN;
    		break;
    	case BOLD:
    		awtStyle = Font.BOLD;
    		break;
    	case ITALIC:
    		awtStyle = Font.ITALIC;
    		break;
    	case BOLD_ITALIC:
    		awtStyle = Font.BOLD | Font.ITALIC;
    		break;
    	}
    	
    	awtFont = awtFont.deriveFont(awtStyle, nominalSize);

		Map attributes = awtFont.getAttributes();
		for (FontDecoration d : decorations) {
    		switch(d) {
    		case LINE_THROUGH:
    			attributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
    			break;
    		case UNDERLINE:
    			attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
    			break;
    		}
    	}
    	
    	awtFont = awtFont.deriveFont(attributes);
    	
    	int maxW = 0;
    	int maxH = 0;
    	
    	BufferedImage dummy = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
    	Graphics2D gd = (Graphics2D) dummy.getGraphics();
    	gd.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    	gd.setFont(awtFont);
		FontMetrics dfontMetrics = gd.getFontMetrics();
    	
    	Map<Character, Vector2> charSizes = new HashMap<>();
		
		for (int i = 0; i < CHARACTER_COUNT; i++) {
    		int charwidth = dfontMetrics.charWidth((char)i);
    		int charheight = dfontMetrics.getHeight();
    		
    		charSizes.put((char)i, new Vector2(charwidth, charheight));

    		if (charwidth <= 0) {
    			charwidth = 1;
    		}

    		if (charheight <= 0) {
    			charheight = 1;
    		}
    		
    		if (maxW < charwidth) {
    			maxW = charwidth;
    		}
    		
    		if (maxH < charheight) {
    			maxH = charheight;
    		}
    	}
    	
    	BufferedImage[] images = new BufferedImage[CHARACTER_COUNT];
    	
    	for (int i = 0; i < CHARACTER_COUNT; i++) {
    		images[i] = getCharImage(awtFont, dfontMetrics, (char) i, maxW, maxH);
    	}
    	
    	gd.dispose();

		String nameId = "_FONT_" + name + "_" + nominalSize + "_" + style.toString() + "_" + gd.hashCode();
		Spritesheet ss = new Spritesheet();
		
		Settings s = new Settings();
		
		s.setWrapS(Wrap.CLAMP_TO_EDGE);
		s.setWrapT(Wrap.CLAMP_TO_EDGE);

    	for (int i = 0; i < CHARACTER_COUNT; i++) {
            String id = "CHAR_" + i;
			ss.sTextures.put(id, ((LwjglTextureManager) this.context.getTexture()).addTexture(nameId, id, true, images[i]));
			ss.sTexturesSettings.put(id, s);
		}

		((LwjglTextureManager) this.context.getTexture()).getSpritesheets().put(nameId, ss);

    	LwjglFont f = new LwjglFont(name, nameId, nominalSize, style, decorations, charSizes, maxH);
    	this.fonts.put(name, f);
    }
    
	private BufferedImage getCharImage(Font font, FontMetrics fm, char ch, int iw, int ih) {
		BufferedImage fontImage;
		fontImage = new BufferedImage(iw, ih, BufferedImage.TYPE_INT_ARGB);
		Graphics2D gt = (Graphics2D) fontImage.getGraphics();
		gt.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		gt.setFont(font);

		gt.setColor(Color.WHITE);
		gt.drawString(String.valueOf(ch), 0, fm.getAscent());

		gt.dispose();

		return fontImage;
	}

	@Override
    public void cleanup() {	
	
	}
}	
