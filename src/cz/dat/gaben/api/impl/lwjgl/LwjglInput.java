package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.util.Vector2;
import org.lwjgl.glfw.*;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class LwjglInput implements IInputManager {

    private LwjglGameWindow context;
    private GLFWKeyCallback keyCallback;
    private GLFWMouseButtonCallback mouseCallback;
    private GLFWCursorPosCallback mousePosCallback;
    private GLFWCharCallback charCallback;
    private GLFWScrollCallback mouseScrollCallback;

    private List<IInputListener> listeners;
    private Map<BufferedImage, Long> cursors;

    private Vector2 mouse = new Vector2();

    public LwjglInput(LwjglGameWindow lwjglGameWindow) {
        this.context = lwjglGameWindow;
        this.listeners = new CopyOnWriteArrayList<>();
        this.cursors = new HashMap<>();
        //this.listeners = new ArrayList<>();

        this.keyCallback = new GLFWKeyCallback() {

            @Override
            @ExceptionHandling(value = "INPUT", errorCode = 400)
            public void invoke(long window, int key, int scanCode, int action,
                               int mods) {
                if (window == context.windowHandle) {
                    if (action == GLFW.GLFW_PRESS) {
                        for (IInputListener l : listeners) {
                            l.onKeyDown(key);
                        }
                    } else if (action == GLFW.GLFW_RELEASE) {
                        for (IInputListener l : listeners) {
                            l.onKeyUp(key);
                        }
                    }
                }
            }

        };

        this.charCallback = new GLFWCharCallback() {

            @Override
            @ExceptionHandling(value = "INPUT", errorCode = 401)
            public void invoke(long window, int arg1) {
                if (window == context.windowHandle) {
                    for (IInputListener l : listeners) {
                        l.onChar((char) arg1);
                    }
                }
            }

        };

        this.mouseCallback = new GLFWMouseButtonCallback() {

            @Override
            public void invoke(long window, int button, int action, int mods) {
                if (window == context.windowHandle) {
                    if (action == GLFW.GLFW_PRESS) {
                        for (IInputListener l : listeners) {
                            l.onMouseButtonDown(button);
                        }
                    } else if (action == GLFW.GLFW_RELEASE) {
                        for (IInputListener l : listeners) {
                            l.onMouseButtonUp(button);
                        }
                    }
                }
            }
        };

        this.mouseScrollCallback = new GLFWScrollCallback() {

            @Override
            public void invoke(long window, double x, double y) {
                if (window == context.windowHandle) {
                    for (IInputListener l : listeners) {
                        l.onMouseScroll((float) x, (float) y);
                    }
                }
            }
        };

        this.mousePosCallback = new GLFWCursorPosCallback() {

            @Override
            public void invoke(long window, double x, double y) {
                if (window == context.windowHandle) {
                    for (IInputListener l : listeners) {
                        l.onMousePositionChange((float) x, (float) y);
                    }

                    mouse = new Vector2((float) x, (float) y);
                }
            }
        };

        GLFW.glfwSetKeyCallback(this.context.windowHandle, this.keyCallback);
        GLFW.glfwSetMouseButtonCallback(this.context.windowHandle,
                this.mouseCallback);
        GLFW.glfwSetCursorPosCallback(this.context.windowHandle,
                this.mousePosCallback);
        GLFW.glfwSetCharCallback(this.context.windowHandle, this.charCallback);
        GLFW.glfwSetScrollCallback(this.context.windowHandle,
                this.mouseScrollCallback);
    }

    @Override
    public boolean isKeyDown(int key) {
        return GLFW.glfwGetKey(this.context.windowHandle, key) == GLFW.GLFW_PRESS;
    }

    @Override
    public boolean isMouseButtonDown(int button) {
        return GLFW.glfwGetMouseButton(this.context.windowHandle, button) == GLFW.GLFW_PRESS;
    }

    @Override
    public String getClipboardString() {
        try {
            return GLFW.glfwGetClipboardString(this.context.windowHandle);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void addEventListener(IInputListener l) {
        if (!this.listeners.contains(l))
            this.listeners.add(l);
    }

    @Override
    public void removeEventListener(IInputListener l) {
        if (this.listeners.contains(l))
            this.listeners.remove(l);
    }

    @Override
    public void setMouseCursorVisible(boolean visible) {
        GLFW.glfwSetInputMode(this.context.windowHandle, GLFW.GLFW_CURSOR,
                visible ? GLFW.GLFW_CURSOR_NORMAL : GLFW.GLFW_CURSOR_HIDDEN);
    }

    @Override
    public void cleanup() {
        this.keyCallback.free();
        this.mouseCallback.free();
        this.mousePosCallback.free();
        this.mouseScrollCallback.free();
        this.charCallback.free();
    }

    @Override
    public void setMouseCursorHooked(boolean hooked) {
        GLFW.glfwSetInputMode(this.context.windowHandle, GLFW.GLFW_CURSOR,
                hooked ? GLFW.GLFW_CURSOR_NORMAL : GLFW.GLFW_CURSOR_DISABLED);
    }

    @Override
    public void setCursor(BufferedImage img, int spotX, int spotY) {
        // TODO
    }

    @Override
    public Vector2 getMousePosition() {
        return this.mouse;
    }

}
