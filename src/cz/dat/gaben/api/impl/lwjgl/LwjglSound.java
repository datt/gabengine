package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.api.interfaces.ISound;
import cz.dat.gaben.api.interfaces.ISoundInstance;
import cz.dat.gaben.util.Vector3;
import cz.dat.gaben.util.WavLoader;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.stb.STBVorbis;
import org.lwjgl.stb.STBVorbisInfo;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LwjglSound implements ISound {

    private static int maxInstances = 4;
    int buffer;
    LwjglSoundManager manager;
    float length, bitrate, sampleFreq;
    int lengthSamples;
    Vector3 spacePos = Vector3.ZERO, spaceVel = Vector3.ZERO, direction = Vector3.ZERO,
            cone = new Vector3(360f, 360f, 0f);
    boolean relativeToListener = false;
    float gain = 1f, pitch = 1f;
    boolean looping = false;
    private LwjglSoundInstance[] instances = new LwjglSoundInstance[LwjglSound.maxInstances];
    private List<ISoundInstance> playingInstances = new LinkedList<>();
    private IContentManager content;
    private String contentPath;

    LwjglSound(LwjglSoundManager man, String contentPath) {
        this.manager = man;
        this.contentPath = contentPath;
        this.content = man.context.getContentManager();
    }

    public static void setMaximumInstances(int instances) {
        LwjglSound.maxInstances = instances;
    }

    void dispose() {
        this.stopAll();
    }

    @ExceptionHandling(errorCode = 301, value = "SOUND_LOAD")
    private ShortBuffer readVorbis(STBVorbisInfo info) {
        ByteBuffer vorbis;
        try {
            vorbis = this.content.readToByteBuffer(this.contentPath, true);
        } catch (IOException e) {
            throw ExceptionUtil.featureBreakingException("Couldn't read content", e, this);
        }

        IntBuffer error = BufferUtils.createIntBuffer(1);
        long decoder = STBVorbis.stb_vorbis_open_memory(vorbis, error, null);
        if (decoder == 0)
            throw ExceptionUtil.featureBreakingException("Failed to open Ogg Vorbis file. Error: " + error.get(0), this);

        STBVorbis.stb_vorbis_get_info(decoder, info);

        int channels = info.channels();

        this.lengthSamples = STBVorbis.stb_vorbis_stream_length_in_samples(decoder) * channels;
        this.length = STBVorbis.stb_vorbis_stream_length_in_seconds(decoder);
        this.sampleFreq = info.sample_rate();

        ShortBuffer pcm = BufferUtils.createShortBuffer(this.lengthSamples);

        pcm.limit(STBVorbis.stb_vorbis_get_samples_short_interleaved(decoder, channels, pcm) * channels);
        STBVorbis.stb_vorbis_close(decoder);

        return pcm;
    }

    @Override
    public void init() {
        this.buffer = AL10.alGenBuffers();
        if (!this.readWav())
            this.readOgg();

        this.bitrate = AL10.alGetBufferi(this.buffer, AL10.AL_SIZE) / (this.length);
        this.manager.checkALError("LwjglSound.init");
    }

    private void readOgg() {
        try (STBVorbisInfo info = STBVorbisInfo.malloc()) {
            ShortBuffer pcm = this.readVorbis(info);
            AL10.alBufferData(this.buffer, info.channels() == 1 ? AL10.AL_FORMAT_MONO16 : AL10.AL_FORMAT_STEREO16,
                    pcm, info.sample_rate());
            this.manager.checkALError("LwjglSound.readOgg");
        }
    }

    private boolean readWav() {
        WavLoader v = new WavLoader(this.content.openStream(this.contentPath), BufferUtils::createByteBuffer);
        if (!v.read())
            return false;
        this.sampleFreq = v.getSampleRate();
        this.lengthSamples = (int) (v.getDataSize() / (v.getBitsPerSample() / 8));
        this.length = (v.getDataSize() * 8f) / (v.getBitsPerSample() * v.getChannels() * v.getSampleRate());

        ByteBuffer buf = v.getBuffer();
        AL10.alBufferData(this.buffer, v.getChannels() == 1 ?
                        (v.getBitsPerSample() == 8 ? AL10.AL_FORMAT_MONO8 : AL10.AL_FORMAT_MONO16)
                        : (v.getBitsPerSample() == 8 ? AL10.AL_FORMAT_STEREO8 : AL10.AL_FORMAT_STEREO16),
                buf, (int) v.getSampleRate());
        this.manager.checkALError("LwjglSound.readWav");
        return true;
    }

    @Override
    public float getLength() {
        return this.length;
    }

    @Override
    public float getBitrate() {
        return this.bitrate;
    }

    @Override
    public float getSampleFrequency() {
        return this.sampleFreq;
    }

    @Override
    public ISoundInstance play(int priority, boolean applyProp) {
        LwjglSoundInstance i = (LwjglSoundInstance) this.getFreeInstance(priority);
        if (i != null) {
            if (applyProp) {
                i.priority = priority;
                i.setCone(this.cone);
                i.setDirection(this.direction);
                i.setSpacePosition(this.spacePos);
                i.setSpaceVelocity(this.spaceVel);
                i.setGain(this.gain);
                i.setLooping(this.looping);
                i.setPitch(this.pitch);
                i.setSoundPositioning(this.relativeToListener);
            }
            i.play();
            this.playingInstances.add(i);
        }
        return i;
    }

    @Override
    public ISoundInstance getFreeInstance(int priority) {
        //System.out.print("[" + this.contentPath + "] ");
        for (int i = 0; i < this.instances.length; i++) {
            if (this.instances[i] == null) {
                this.instances[i] = new LwjglSoundInstance(this);
                this.instances[i].init();
                //System.out.println("Returning new instance");
                return this.instances[i];
            }

            if (this.instances[i].isFreeToUse()) {
                //System.out.println("Returning free instance");
                return this.instances[i];
            }
        }

        if (priority == ISound.ALWAYS_PLAY_PRIORITY) {
            //System.out.println("ERMAHGHERD SUCH PRIORITY, MUCH WOW");
            this.instances[0].stop();
            return this.instances[0];

        }

        for (LwjglSoundInstance instance : this.instances) {
            if (instance.priority < priority && instance.priority != ISound.ALWAYS_PLAY_PRIORITY) {
                instance.stop();
                //System.out.println("Returning instance with lower priority");
                return instance;
            }
        }

        //System.out.println("No sound for me :(");

        return null;
    }

    @Override
    public List<ISoundInstance> getAllPlayingInstances() {
        for (Iterator<ISoundInstance> iterator = this.playingInstances.iterator(); iterator.hasNext(); ) {
            ISoundInstance playingInstance = iterator.next();
            if (playingInstance.getState() != SoundState.PLAYING)
                iterator.remove();
        }

        return this.playingInstances;
    }

    @Override
    public void stopAll() {
        for (LwjglSoundInstance instance : this.instances) {
            if (instance != null)
                instance.stop();
        }
    }

    @Override
    public boolean isStreaming() {
        return false;
    }


    @Override
    public boolean isLooping() {
        return this.looping;
    }

    @Override
    public void setLooping(boolean looping) {
        this.looping = looping;
    }


    @Override
    public float getGain() {
        return this.gain;
    }

    @Override
    public void setGain(float gain) {
        this.gain = gain;
    }

    @Override
    public float getPitch() {
        return this.pitch;
    }

    @Override
    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    @Override
    public void setSpacePosition(float x, float y, float z) {
        this.spacePos = new Vector3(x, y, z);
    }

    @Override
    public Vector3 getSpacePosition() {
        return this.spacePos;
    }

    @Override
    public void setSpaceVelocity(float x, float y, float z) {
        this.spaceVel = new Vector3(x, y, z);
    }

    @Override
    public Vector3 getSpaceVelocity() {
        return this.spaceVel;
    }

    @Override
    public void setSoundPositioning(boolean relativeToListener) {
        this.relativeToListener = relativeToListener;
    }

    @Override
    public boolean isPositionedRelativeToListener() {
        return this.relativeToListener;
    }

    @Override
    public void setDirection(float x, float y, float z) {
        this.direction = new Vector3(x, y, z);
    }

    @Override
    public Vector3 getDirection() {
        return this.direction;
    }

    @Override
    public void setCone(float innerAngle, float outerAngle, float outerGain) {
        this.cone = new Vector3(innerAngle, outerAngle, outerGain);
    }

    @Override
    public Vector3 getCone() {
        return this.cone;
    }
}
