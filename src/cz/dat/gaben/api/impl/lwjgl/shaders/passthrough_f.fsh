#version 140

in vec4 color;
in vec2 tex;

out vec4 fragColor;

uniform sampler2D fbotex;

void main(void)
{
    fragColor = texture(fbotex, tex);
}