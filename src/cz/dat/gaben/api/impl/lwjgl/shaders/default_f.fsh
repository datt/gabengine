#version 140

in vec4 color;
in vec3 tex;
flat in int tmu;

out vec4 fragColor;

uniform sampler2DArray samplers[8];

void main(void)
{
    if (tmu >= 0) {
    
    	vec4 sample = texture(samplers[tmu], tex.xyz);
    	
    	if(sample.a < 0.01) {
    		discard;
    	}
    	
    	fragColor = color * sample;
    } else {
    	fragColor = color;
    }	
}