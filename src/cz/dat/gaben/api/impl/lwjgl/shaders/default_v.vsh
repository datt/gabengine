#version 140
 
uniform mat4 projectionMatrix;
uniform mat4 modelviewMatrix;

in vec2 in_vertex;
in vec4 in_color;
in vec4 in_tex;

out vec4 color;
out vec3 tex;
flat out int tmu;
 
void main()
{
    gl_Position =  projectionMatrix * modelviewMatrix * vec4(in_vertex, 0.0, 1.0);
    
    color = in_color;

    tex = in_tex.stp;
    tmu = int(in_tex.q);
}
