package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.api.interfaces.ITextureManager;
import cz.dat.gaben.util.Vector2;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Spritesheet {
    public Map<String, LwjglTexture> sTextures = new HashMap<>();
    public Map<String, Settings> sTexturesSettings = new HashMap<>();
}

@ExceptionHandling(value = "TEXTURES", errorCode = 202)
public class LwjglTextureManager implements ITextureManager {

    private IContentManager content;
    private Settings settings;
    private Map<String, Spritesheet> spritesheets;
    private Spritesheet defaultSs;
    private List<LwjglTexture> texturesToLoad;

    public LwjglTextureManager(Settings s, IContentManager content) {
        this.content = content;
        this.settings = s;

        this.texturesToLoad = new ArrayList<>();
        this.spritesheets = new HashMap<>();

        this.defaultSs = new Spritesheet();
        this.spritesheets.put("__DEFAULT__", this.defaultSs);
    }

    //--------- INNER METHODS - U CAN'T TOUCH THIS ---------//
    LwjglTexture addTexture(String id, String idInSpritesheet, boolean isSpritesheet, BufferedImage img) {
        LwjglTexture ret = isSpritesheet ? new LwjglTexture(idInSpritesheet, id, img) :
                new LwjglTexture(id, img);

        this.texturesToLoad.add(ret);
        return ret;
    }

    @ExceptionHandling("TEXTURE_LOAD")
    LwjglTexture addTexture(String id, String idInSpritesheet, boolean isSpritesheet, String contentPath) {
        try {
            return this.addTexture(id, idInSpritesheet, isSpritesheet,
                    ImageIO.read(this.content.openStream(contentPath)));
        } catch (IOException e) {
            throw ExceptionUtil.featureBreakingException("Couldn't read image " + contentPath, e, this);
        }
    }

    Map<String, Spritesheet> getSpritesheets() {
        return this.spritesheets;
    }

    int createGlTexture(int width, int height, int depth) {
        int texId = GL11.glGenTextures();
        GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, texId);
        GL12.glTexImage3D(GL30.GL_TEXTURE_2D_ARRAY, 0, GL11.GL_RGBA8, width, height, depth, 0, GL12.GL_BGRA, GL11.GL_UNSIGNED_BYTE, (ByteBuffer) null);
        return texId;
    }

    private void decode(BufferedImage img, IntBuffer buf) {
        int[] pixels = new int[img.getWidth() * img.getHeight()];
        img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());

        for (int y = 0; y < img.getHeight(); y++) {
            for (int x = 0; x < img.getWidth(); x++) {
                int pixel = pixels[y * img.getWidth() + x];
                buf.put(pixel);
            }
        }
    }

    private void setGlParameters(Settings s) {
    	GL30.glGenerateMipmap(GL30.GL_TEXTURE_2D_ARRAY);

        s.getTexParameters().forEach((k, v) ->
                GL11.glTexParameteri(GL30.GL_TEXTURE_2D_ARRAY, k, v));
    }

    //--------- PUBLIC LOADING METHODS ---------//
    @Override
    public void loadTexture(String id, String contentPath) {
        this.loadTexture(id, contentPath, this.settings);
    }

    @Override
    public void loadSpritesheet(String spritesheetId, String contentPath, Vector2 spriteSize) {
        this.loadSpritesheet(spritesheetId, contentPath, spriteSize, new HashMap<>());
    }

    @Override
    public void loadSpritesheet(String spritesheetId, String contentPathMask, int numberOfFiles) {
        this.loadSpritesheet(spritesheetId, contentPathMask, numberOfFiles, new HashMap<>());
    }

    @Override
    public void loadTexture(String id, String contentPath, Settings settings) {
        this.addTexture(id, "__DEFAULT__", false, contentPath);
        this.defaultSs.sTexturesSettings.put(id, settings);
    }

    @Override
    @ExceptionHandling("TEXTURE_LOAD")
    public void loadSpritesheet(String spritesheetId, String contentPath, Vector2 spriteSize, Map<String, Settings> settings) {
        Spritesheet ss = new Spritesheet();
        this.spritesheets.put(spritesheetId, ss);

        Settings defSet = settings.get("__DEFAULT__apply_for_all");

        if(defSet == null)
            ss.sTexturesSettings = settings;

        try {
            BufferedImage img = ImageIO.read(this.content.openStream(contentPath));
            for (int y = 0; y < img.getHeight(); y += spriteSize.y()) {
                for (int x = 0; x < img.getWidth(); x += spriteSize.x()) {
                    BufferedImage crop = img.getSubimage(x, y, (int) spriteSize.x(), (int) spriteSize.y());
                    String name = "TEX_" + (int)(x / spriteSize.x()) + "_" + (int)(y / spriteSize.y());
                    ss.sTextures.put(name, this.addTexture(spritesheetId, name, true, crop));
                    if(defSet != null)
                        ss.sTexturesSettings.put(name, defSet);
                }
            }
        } catch (IOException e) {
            throw ExceptionUtil.featureBreakingException("Couldn't read a spritesheet image from " + contentPath, e, this);
        }
    }
    
    @Override
    public void loadSpritesheet(String spritesheetId, String contentPath, Vector2 spriteSize, Settings settings) {
        HashMap<String, Settings> df = new HashMap<>();
        df.put("__DEFAULT__apply_for_all", settings);
        this.loadSpritesheet(spritesheetId, contentPath, spriteSize, df);
    }

    @Override
    public void loadSpritesheet(String spritesheetId, String contentPathMask, int numberOfFiles, Map<String, Settings> settings) {
        Spritesheet ss = new Spritesheet();
        this.spritesheets.put(spritesheetId, ss);
        ss.sTexturesSettings = settings;

        for (int i = 0; i < numberOfFiles; i++) {
            String path = contentPathMask.replace("%n", i + "");
            if (this.content.openStream(path) != null) {
                String name = path.substring(path.indexOf('/'));
                ss.sTextures.put(name, this.addTexture(spritesheetId, name, true, path));
            }
        }
    }

    //--------- PUBLIC GETTING METHODS ---------//
    @Override
    public ITexture getTexture(String id) {
        return this.defaultSs.sTextures.get(id);
    }

    @Override
    public ITexture getTexture(String spritesheetId, String id) {
        return this.spritesheets.get(spritesheetId).sTextures.get(id);
    }

    @Override
    public Map<String, ? extends ITexture> getSpritesheet(String spritesheetId) {
        return this.spritesheets.get(spritesheetId).sTextures;
    }

    //--------- LOADING ---------//
    @Override
    public boolean finishLoading() {
        int maxTextures = GL11.glGetInteger(GL30.GL_MAX_ARRAY_TEXTURE_LAYERS);

        List<Texture2DArray> arrays = new ArrayList<>();

        for (LwjglTexture toLoad : this.texturesToLoad) {
            Vector2 size = toLoad.getSize();

            boolean hasSize = false;
            Settings set = this.settings;
            Map<String, Settings> texSetMap =
                    this.spritesheets.get(toLoad.getSpritesheetId()).sTexturesSettings;

            if (texSetMap.containsKey(toLoad.getTextureName()))
                set = texSetMap.get(toLoad.getTextureName());

            Texture2DArray texSuitableArray = null;

            for (Texture2DArray t : arrays) {
                if (t.size.equals(size) && set == t.textureSettings && t.isUsable) {
                    hasSize = true;
                    texSuitableArray = t;

                    if (t.textures.size() >= maxTextures - 1)
                        t.isUsable = false;
                    break;
                }
            }

            if (!hasSize) {
                texSuitableArray = new Texture2DArray(size, set);
                arrays.add(texSuitableArray);
            }

            texSuitableArray.textures.add(toLoad);
        }


        for (Texture2DArray toLoad : arrays) {
            Vector2 glTextureSize = toLoad.size;
            List<LwjglTexture> sizedTextures = toLoad.textures;

            int glTexId = this.createGlTexture((int) glTextureSize.x(), (int) glTextureSize.y(),
                    (sizedTextures.size() > maxTextures ? maxTextures : sizedTextures.size()));

            IntBuffer dataBuffer = BufferUtils.createIntBuffer(4 * (int) glTextureSize.x() * (int) glTextureSize.y());

            for (int i = 0; i < sizedTextures.size(); i++) {
                LwjglTexture loading = sizedTextures.get(i);
                BufferedImage dec = loading.getImage();

                this.decode(dec, dataBuffer);
                dataBuffer.flip();

                GL12.glTexSubImage3D(GL30.GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, (int) glTextureSize.x(), (int) glTextureSize.y(),
                        1, GL12.GL_BGRA, GL11.GL_UNSIGNED_BYTE, dataBuffer);

                dataBuffer.clear();

                loading.initGl(glTexId, i);
                this.spritesheets.get(loading.getSpritesheetId()).sTextures.put(loading.getTextureName(), loading);
            }

            this.setGlParameters(toLoad.textureSettings);
            this.texturesToLoad.clear();
        }

        return true;
    }

    @Override
    public void cleanup() {

    }

    private class Texture2DArray {
        public Vector2 size;
        public List<LwjglTexture> textures;
        public Settings textureSettings;
        public boolean isUsable = true;

        public Texture2DArray(Vector2 size, Settings set) {
            this.size = size;
            this.textureSettings = set;
            this.textures = new ArrayList<>();
        }
    }

}
