package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.Vector2;

import java.awt.image.BufferedImage;

public class LwjglTexture implements ITexture {

    private String textureId, spritesheetId;
    private int glId = Integer.MIN_VALUE;
    private int layer = Integer.MIN_VALUE;
    private boolean isInSpritesheet = false;

    private Vector2 size;
    private BufferedImage image;

    public LwjglTexture(String id, String spritesheetId, BufferedImage img) {
        this(id, img);
        this.isInSpritesheet = true;
        this.spritesheetId = spritesheetId;
    }

    public LwjglTexture(String id, BufferedImage img) {
        this.textureId = id;
        this.image = img;
        this.spritesheetId = "__DEFAULT__";
        this.size = new Vector2(img.getWidth(), img.getHeight());
    }

    void initGl(int glId, int layer) {
        this.glId = glId;
        this.layer = layer;
    }

    BufferedImage getImage() {
        return this.image;
    }

    @Override
    public String getTextureName() {
        return this.textureId;
    }

    @Override
    public Vector2 getSize() {
        return this.size;
    }

    public String getSpritesheetId() {
        return this.spritesheetId;
    }

    public boolean isInSpritesheet() {
        return this.isInSpritesheet;
    }

    public boolean canBeUsed() {
        return (this.glId != Integer.MIN_VALUE);
    }

    public int getGlId() {
        return glId;
    }

    public int getLayer() {
        return layer;
    }

}