package cz.dat.gaben.api.impl.lwjgl;

import org.lwjgl.opengl.GL30;

import cz.dat.gaben.api.interfaces.IFbo;

public class LwjglFbo implements IFbo {

	private String name;
	private int width, height;
	private int textureIds[], fboId;
	
	public LwjglFbo(String name, int width, int height, int[] textureIds, int fboId) {
		this.name = name;
		this.width = width;
		this.height = height;
		
		this.textureIds = textureIds;
		this.fboId = fboId;
	}
	
	@Override
	public int getTextureId(int n) {
		return this.textureIds[n];
	}

	@Override
	public int getFboId() {
		return this.fboId;
	}

	@Override
	public int getWidth() {
		return this.width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}

	@Override
	public int getAttachmentCount() {
		return this.textureIds.length;
	}

}
