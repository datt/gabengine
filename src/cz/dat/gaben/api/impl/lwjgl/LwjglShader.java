package cz.dat.gaben.api.impl.lwjgl;

import java.nio.FloatBuffer;

import cz.dat.gaben.api.ShaderVariables;
import cz.dat.gaben.api.interfaces.IShader;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.Matrix2;
import cz.dat.gaben.util.Matrix3;
import cz.dat.gaben.util.Matrix4;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;

public class LwjglShader implements IShader {

    private String name;
	private int progId = 0;
	private ShaderVariables variables;
	
	private FloatBuffer tempData;
	
    public LwjglShader(String name, int progId, ShaderVariables variables) {
    	this.name = name;
    	this.progId = progId;
    	this.variables = variables;
    	this.tempData = BufferUtils.createFloatBuffer(16);
    }

    @Override
    public int getProgramId() {
        return this.progId;
    }

    @Override
    public String getName() {
    	return this.name;
    }

    @Override
    public void setUniform1f(String name, float val1) {
        if (hasUniform(name)) {
            GL20.glUniform1f(this.variables.getUniform(name), val1);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform float " + name + " not found!");
        }
    }

    @Override
    public void setUniform2f(String name, float val1, float val2) {
        if (hasUniform(name)) {
        	GL20.glUniform2f(this.variables.getUniform(name), val1, val2);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform vec2 " + name + " not found!");
        }
    }

    @Override
    public void setUniform3f(String name, float val1, float val2, float val3) {
        if (hasUniform(name)) {
        	GL20.glUniform3f(this.variables.getUniform(name), val1, val2, val3);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform vec3 " + name + " not found!");
        }
    }

    @Override
    public void setUniform4f(String name, float val1, float val2, float val3, float val4) {
        if (hasUniform(name)) {
        	GL20.glUniform4f(this.variables.getUniform(name), val1, val2, val3, val4);
        } else {
            GabeLogger.warning("[SHADER ERROR] Uniform vec4 " + name + " not found!");
        }
    }

    @Override
    public void setUniform1i(String name, int val1) {
    	if (hasUniform(name)) {
        	GL20.glUniform1i(this.variables.getUniform(name), val1);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform int " + name + " not found!");
        }
    }

    @Override
    public void setUniform2i(String name, int val1, int val2) {
        if (hasUniform(name)) {
        	GL20.glUniform2i(this.variables.getUniform(name), val1, val2);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform Matrix2i " + name + " not found!");
        }
    }

    @Override
    public void setUniform3i(String name, int val1, int val2, int val3) {
        if (hasUniform(name)) {
        	GL20.glUniform3i(this.variables.getUniform(name), val1, val2, val3);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform Matrix3i " + name + " not found!");
        }
    }

    @Override
    public void setUniform4i(String name, int val1, int val2, int val3, int val4) {
        if (hasUniform(name)) {
        	GL20.glUniform4i(this.variables.getUniform(name), val1, val2, val3, val4);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform Matrix4i " + name + " not found!");
        }
    }

    @Override
    public void setUniformMatrix2f(String name, Matrix2 matrix) {
        if (hasUniform(name)) {
        	this.tempData.clear();
        	this.tempData.put(matrix.getData());
        	this.tempData.flip();
        	GL20.glUniformMatrix2fv(this.variables.getUniform(name), false, this.tempData);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform Matrix2f " + name + " not found!");
        }
    }

    @Override
    public void setUniformMatrix3f(String name, Matrix3 matrix) {
        if (hasUniform(name)) {
        	this.tempData.clear();
        	this.tempData.put(matrix.getData());
        	this.tempData.flip();
        	GL20.glUniformMatrix3fv(this.variables.getUniform(name), false, this.tempData);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform Matrix3f " + name + " not found!");
        }
    }

    @Override
    public void setUniformMatrix4f(String name, Matrix4 matrix) {
        if (hasUniform(name)) {
        	this.tempData.clear();
        	this.tempData.put(matrix.getData());
        	this.tempData.flip();
        	GL20.glUniformMatrix4fv(this.variables.getUniform(name), false, this.tempData);
        } else {
        	GabeLogger.warning("[SHADER ERROR] Uniform Matrix4f " + name + " not found!");
        }
    }

    @Override
    public int getAttribLocation(String name) {
        return this.variables.getAttribute(name);
    }

    @Override
	public boolean hasUniform(String name) {
        return this.variables.hasUniform(name);
    }
    
    @Override
    public boolean hasAttribute(String name) {
    	return this.variables.hasAttribute(name);
    }
}
