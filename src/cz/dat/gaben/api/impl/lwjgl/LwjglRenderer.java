package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.interfaces.IFbo;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.IShader;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.BiMap;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Matrix4;
import org.lwjgl.opengl.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.PriorityQueue;
import java.util.Queue;

public class LwjglRenderer implements IRenderer {

    public static final int BUFFER_SIZE = 1024 * 1024;
    public static final int VERTEX_SIZE = (2 + 1 + 4) << 2;
    public static final int MAX_VERTICES = BUFFER_SIZE / VERTEX_SIZE;

    private IShader currentShader = null;
    private IFbo currentFbo = null;
    private boolean autoScaleFbo = false;

    private float fboScaleX = 1, fboScaleY = 1;

    private LwjglGameWindow context;
    private FloatBuffer drawBuffer;
    private int vaoId;
    private int vboId;
    private PrimitiveMode primitiveMode = PrimitiveMode.TRIANGLES;
    private ShapeMode shapeMode = ShapeMode.FILLED;

    private boolean textureEnabled = false;

    private float colorPacked;

    private BiMap<Integer, Integer> textureTmuPairs;
    private Queue<Integer> freeTmus;
    private Queue<Integer> tmuQueue;

    private boolean[] tmuUsedInDrawCall;

    private float texCoordS = 0f;
    private float texCoordT = 0f;
    private int texLayer = -1;
    private int texTmu = -1;

    private LwjglTexture currentTexture;

    private int vertexCount = 0;

    private Matrix4 projectionMat;
    private Matrix4 identityMat;

    private Matrix4 currentModelviewMat;

    public LwjglRenderer(LwjglGameWindow lwjglGameWindow) {
        this.context = lwjglGameWindow;

        this.identityMat = this.currentModelviewMat = Matrix4.createIdentityMatrix();
        this.projectionMat = this.createOrthoMatrix();

        this.context.getShader().loadShader(LwjglShaderManager.DEFAULT_SHADER, null, null);
        this.defaultShader();
        this.setupShader(this.currentShader, true);

        this.context.getShader().loadShader(LwjglShaderManager.PASS_SHADER, null, null);
        this.passthroughShader();
        this.setupShader(this.currentShader, false);

        this.defaultShader();

        this.tmuUsedInDrawCall = new boolean[8];

        GL11.glFrontFace(GL11.GL_CW);
        GL11.glEnable(GL11.GL_CULL_FACE);

        GL11.glEnable(GL11.GL_BLEND);
        GL20.glBlendEquationSeparate(GL14.GL_FUNC_ADD, GL14.GL_FUNC_ADD);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        ByteBuffer bb = ByteBuffer.allocateDirect(LwjglRenderer.BUFFER_SIZE);
        bb.order(ByteOrder.nativeOrder());
        this.drawBuffer = bb.asFloatBuffer();

        this.vaoId = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoId);

        this.vboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);

        int vertexLoc = this.currentShader.getAttribLocation("in_vertex");
        int colorLoc = this.currentShader.getAttribLocation("in_color");
        int textureLoc = this.currentShader.getAttribLocation("in_tex");

        GL20.glVertexAttribPointer(vertexLoc, 2, GL11.GL_FLOAT, false, VERTEX_SIZE, 0);
        GL20.glVertexAttribPointer(colorLoc, 4, GL11.GL_UNSIGNED_BYTE, true, VERTEX_SIZE, 2 << 2);
        GL20.glVertexAttribPointer(textureLoc, 4, GL11.GL_FLOAT, false, VERTEX_SIZE, 3 << 2);

        GL20.glEnableVertexAttribArray(vertexLoc);
        GL20.glEnableVertexAttribArray(colorLoc);
        GL20.glEnableVertexAttribArray(textureLoc);

        this.textureTmuPairs = new BiMap<>();
        this.tmuQueue = new PriorityQueue<>();
        this.freeTmus = new PriorityQueue<>();

        for (int i = 0; i < 8; i++) {
            this.freeTmus.add(i);
        }

        this.clearColor(Color.WHITE);
    }

    public Matrix4 createOrthoMatrix() {
        return Matrix4.createOrthoMatrix(0, this.context.getWidth(), this.context.getHeight(), 0, 0, 1);
    }

    @Override
    public void cleanup() {
        GL15.glDeleteBuffers(this.vboId);
        GL30.glDeleteVertexArrays(this.vaoId);
        GL20.glUseProgram(0);
    }

    @Override
    public void flush() {
        if (this.vertexCount > 0) {
            GL11.glFlush();
            this.drawBuffer.flip();
            GL15.glBufferData(GL15.GL_ARRAY_BUFFER, this.drawBuffer, GL15.GL_DYNAMIC_DRAW);
            GL11.glDrawArrays(this.primitiveMode.getGl(), 0, this.vertexCount);
            this.drawBuffer.clear();
            this.vertexCount = 0;

            for (int i = 0; i < 8; i++) {
                this.tmuUsedInDrawCall[i] = false;
            }
        }
    }

    @Override
    public void primitiveMode(PrimitiveMode mode) {
        if (this.primitiveMode != mode) {
            this.flush();
            this.primitiveMode = mode;
        }
    }

    @Override
    public void shapeMode(ShapeMode mode) {
        if (this.shapeMode != mode) {
            this.flush();
            this.shapeMode = mode;
        }
    }

    @Override
    public void texture(ITexture tex) {
        LwjglTexture texC = (LwjglTexture) tex;
        if (this.currentTexture != texC) {
            this.currentTexture = texC;
            this.autoBindTexture(texC);
        }
    }

    private void autoBindTexture(LwjglTexture tex) {
        this.texLayer = tex.getLayer();
        int glId = tex.getGlId();
        int tmu;

        if (this.textureTmuPairs.containsBackward(glId)) {
            tmu = this.textureTmuPairs.getBackward(glId);
        } else {
            tmu = this.freeTmu();

            if (this.tmuUsedInDrawCall[tmu]) {
                this.flush();
            }

            GL13.glActiveTexture(GL13.GL_TEXTURE0 + tmu);
            GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, glId);
            this.textureTmuPairs.put(tmu, glId);
        }
        this.texTmu = tmu;

        while (this.tmuQueue.contains(tmu)) {
            this.tmuQueue.remove(tmu);
        }
        this.tmuQueue.add(tmu);
    }

    private int freeTmu() {
        if (!freeTmus.isEmpty()) {
            return freeTmus.poll();
        } else {
            int oldest = this.getOldestTmu();
            this.textureTmuPairs.removeForward(oldest);
            return oldest;
        }
    }

    private int getOldestTmu() {
        return this.tmuQueue.poll();
    }

    @Override
    public void enableTexture(boolean enable) {
        this.textureEnabled = enable;
    }

    @Override
    public void texCoord(float s, float t) {
        this.texCoordS = s;
        this.texCoordT = t;
    }

    @Override
    public void color(float r, float g, float b, float a) {
        this.color(Color.color(r, g, b, a));
    }

    @Override
    public void color(int r, int g, int b, int a) {
        this.color(Color.color(r, g, b, a));
    }

    @Override
    public void color(int color) {
        this.colorPacked = Float.intBitsToFloat(color);
    }

    @Override
    public void clearColor(float r, float g, float b, float a) {
        GL11.glClearColor(r, g, b, a);
    }

    @Override
    public void vertex(float x, float y) {
        if (vertexCount == LwjglRenderer.MAX_VERTICES) {
            flush();
        }

        this.drawBuffer.put(x * this.fboScaleX).put(y * this.fboScaleY);
        this.drawBuffer.put(this.colorPacked);
        this.drawBuffer.put(this.texCoordS).put(this.texCoordT).put(this.texLayer).put(this.textureEnabled ? this.texTmu : -1);
        this.vertexCount++;

        if (this.texTmu >= 0) {
            this.tmuUsedInDrawCall[this.texTmu] = true;
        }
    }

    @Override
    public void drawPoint(float x0, float y0) {
        this.primitiveMode(PrimitiveMode.POINTS);
        this.vertex(x0, y0);
    }

    @Override
    public void drawLine(float x0, float y0, float x1, float y1) {
        this.primitiveMode(PrimitiveMode.LINES);
        this.vertex(x0, y0);
        this.vertex(x1, y1);
    }

    @Override
    public void drawTriangle(float x0, float y0, float x1, float y1, float x2, float y2) {
        if (this.shapeMode == ShapeMode.FILLED) {
            this.primitiveMode(PrimitiveMode.TRIANGLES);
            this.vertex(x0, y0);
            this.vertex(x1, y1);
            this.vertex(x2, y2);
        } else {
            this.drawLine(x0, y0, x1, y1);
            this.drawLine(x1, y1, x2, y2);
            this.drawLine(x2, y2, x0, y0);
        }
    }

    public void drawTriangle(float x0, float y0, float s0, float t0, float x1, float y1, float s1, float t1, float x2, float y2, float s2, float t2) {
        this.primitiveMode(PrimitiveMode.TRIANGLES);
        this.texCoord(s0, t0);
        this.vertex(x0, y0);
        this.texCoord(s1, t1);
        this.vertex(x1, y1);
        this.texCoord(s2, t2);
        this.vertex(x2, y2);
    }

    @Override
    public void drawQuad(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3) {
        if (this.shapeMode == ShapeMode.FILLED) {
            this.drawTriangle(x0, y0, 0, 0, x1, y1, 1, 0, x2, y2, 1, 1);
            this.drawTriangle(x0, y0, 0, 0, x2, y2, 1, 1, x3, y3, 0, 1);
        } else {
            this.drawLine(x0, y0, x1, y1);
            this.drawLine(x1, y1, x2, y2);
            this.drawLine(x2, y2, x3, y3);
            this.drawLine(x3, y3, x0, y0);
        }
    }


    @Override
    public void drawCircle(float x, float y, float r) {
        this.drawSegCircle(x, y, r, 128); // 128 segs should be enough
    }

    @Override
    public void drawArc(float x, float y, float r, float startAngle, float angle) {
        this.drawSegArc(x, y, r, startAngle, startAngle, 64);
    }


    @Override
    public void drawRect(float x0, float y0, float x1, float y1) {
        this.drawQuad(x0, y0, x1, y0, x1, y1, x0, y1);
    }

    @Override
    public void drawSegCircle(float cx, float cy, float r, float segs) {
        float theta = (float) (2 * Math.PI / segs);
        float c = (float) Math.cos(theta);
        float s = (float) Math.sin(theta);
        float t;

        float x = r;
        float y = 0;

        this.primitiveMode(this.shapeMode == ShapeMode.FILLED ?
                PrimitiveMode.TRIANGLE_FAN : PrimitiveMode.LINE_LOOP);

        for (int ii = 0; ii < segs; ii++) {
            this.vertex(x + cx, y + cy);
            t = x;
            x = c * x - s * y;
            y = s * t + c * y;
        }
        this.flush();
    }

    @Override
    public void drawSegArc(float cx, float cy, float r, float startAngle, float angle, float segs) {
        //TODO
    }

    @Override
    public void lineWidth(float width) {
        GL11.glLineWidth(width);
    }

    @Override
    public void pointSize(float size) {
        GL11.glPointSize(size);
    }

    @Override
    public void drawText(float x, float y, float size, String font, String text) {
        this.context.getFontRenderer().setSize(size);
        this.context.getFontRenderer().setFont(this.context.getFont().getFont(font));
        this.context.getFontRenderer().drawString(text, x, y);
    }

    @Override
    public void clear() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void shader(IShader shader) {
        int id = shader.getProgramId();
        if (this.currentShader == null || this.currentShader.getProgramId() != id) {
            this.flush();
            this.currentShader = shader;
            GL20.glUseProgram(id);
            this.currentShader.setUniformMatrix4f("modelviewMatrix", this.currentModelviewMat);
        }
    }

    @Override
    public void defaultShader() {
        this.shader(this.context.getShader().getShader("__DEFAULT__"));
    }

    @Override
    public void passthroughShader() {
        this.shader(this.context.getShader().getShader("__PASSTHROUGH__"));
    }

    @Override
    public void setupShader(IShader shader, boolean gabeSamplers) {
        shader.setUniformMatrix4f("projectionMatrix", this.projectionMat);
        if (gabeSamplers) {
            for (int i = 0; i < 8; i++) {
                String s = "samplers[" + i + "]";
                shader.setUniform1i(s, i);
            }
        }
    }

    @Override
    public void fbo(IFbo fbo) {
        if (fbo == null && this.currentFbo != null) {
            this.flush();
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
            this.currentFbo = null;
            this.updateFboScale();
            GL11.glViewport(0, 0, this.context.getWidth(), this.context.getHeight());
        } else if (this.currentFbo != fbo) {
            this.flush();
            this.currentFbo = fbo;
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fbo.getFboId());
            this.updateFboScale();
            GL11.glViewport(0, 0, fbo.getWidth(), fbo.getHeight());
        }
    }

    @Override
    public void autoScaleFbo(boolean enable) {
        this.autoScaleFbo = enable;
        this.updateFboScale();
    }

    private void updateFboScale() {
        if (this.currentFbo != null && this.autoScaleFbo) {
            float nw = this.context.getWidth();
            float nh = this.context.getHeight();
            this.fboScaleX = this.currentFbo.getWidth() / nw;
            this.fboScaleY = this.currentFbo.getHeight() / nh;
        } else {
            this.fboScaleX = 1;
            this.fboScaleY = 1;
        }
    }

    @Override
    public void bindFboTexture(IFbo fbo, int texn, int tmu) {
        GL13.glActiveTexture(GL13.GL_TEXTURE0 + tmu);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, fbo.getTextureId(texn));
    }

    @Override
    public void drawFullscreenQuad() {
        float w = this.context.getWidth();
        float h = this.context.getHeight();
        this.drawRect(0, 0, w, h);
    }

    @Override
    public Matrix4 getMatrix() {
        return this.currentModelviewMat;
    }

    @Override
    public void setMatrix(Matrix4 mat) {
        this.flush();
        this.currentModelviewMat = mat;
        this.currentShader.setUniformMatrix4f("modelviewMatrix", mat);
    }

    @Override
    public Matrix4 identityMatrix() {
        return this.identityMat;
    }

    @Override
    public void setFrontFace(boolean cw) {
        this.flush();
        GL11.glFrontFace(cw ? GL11.GL_CW : GL11.GL_CCW);
    }

}