package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.interfaces.ISound;
import cz.dat.gaben.api.interfaces.ISoundInstance;
import cz.dat.gaben.api.interfaces.ISoundManager;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.Vector3;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.*;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.*;

public class LwjglSoundManager implements ISoundManager {

    LwjglGameWindow context;
    private float gain = 1f;
    private Vector3 position = Vector3.ZERO, velocity = Vector3.ZERO,
            orientationAt = new Vector3(0f, 0f, -1f),
            orientationUp = new Vector3(0f, 1f, 0f);
    private Map<String, LwjglSound> normalSounds;
    // TODO: stream me :(
    // private Map<String, LwjglStreamingSound> streamingSounds;
    private Map<String, LwjglSound> streamingSounds;
    private List<LoadEntry> toLoad;

    private long alcDevice, alcContext;
    private ALCCapabilities alcCap;
    private ALCapabilities alCap;

    public LwjglSoundManager(LwjglGameWindow context) {
        this.context = context;
        this.normalSounds = new HashMap<>();
        this.streamingSounds = new HashMap<>();
        this.toLoad = new LinkedList<>();
        this.position = this.velocity = this.orientationAt = this.orientationUp = Vector3.ZERO;

        this.initOAL();
    }

    public void updateAllMusic() {
        /*this.streamingSounds.forEach((k, v) -> v.update());*/
    }

    private void initOAL() {
        this.alcDevice = ALC10.alcOpenDevice((ByteBuffer) null);
        this.alcCap = ALC.createCapabilities(this.alcDevice);
        this.alcContext = ALC10.alcCreateContext(this.alcDevice, (IntBuffer) null);
        this.checkALCError();
        ALC10.alcMakeContextCurrent(this.alcContext);
        this.alCap = AL.createCapabilities(this.alcCap);
        this.checkALCError();
        this.checkALError("init");
    }

    @Override
    public void cleanup() {
        this.normalSounds.forEach((k, v) -> v.dispose());
        this.streamingSounds.forEach((k, v) -> v.dispose());

        ALC10.alcDestroyContext(this.alcContext);
        ALC10.alcCloseDevice(this.alcDevice);
    }

    @Override
    public void setListenerPosition(float x, float y, float z) {
        AL10.alListener3f(AL10.AL_POSITION, x, y, z);
        this.position = new Vector3(x, y, z);
        this.checkALError("setListenerPosition");
    }

    @Override
    public void setListenerVelocity(float x, float y, float z) {
        AL10.alListener3f(AL10.AL_VELOCITY, x, y, z);
        this.velocity = new Vector3(x, y, z);
        this.checkALError("setListenerVelocity");
    }

    @Override
    public void setListenerOrientation(float atX, float atY, float atZ, float upX, float upY, float upZ) {
        FloatBuffer b = BufferUtils.createFloatBuffer(6);
        b.put(atX).put(atY).put(atZ).put(upX).put(upY).put(upZ);
        b.flip();
        AL10.alListenerfv(AL10.AL_ORIENTATION, b);
        this.orientationAt = new Vector3(atX, atY, atZ);
        this.orientationUp = new Vector3(upX, upY, upZ);
        this.checkALError("setListenerOrientation");
    }

    @Override
    public Vector3 getListenerVelocity() {
        return this.velocity;
    }

    @Override
    public Vector3 getListenerPosition() {
        return this.position;
    }

    @Override
    public Vector3 getListenerOrientationAt() {
        return this.orientationAt;
    }

    @Override
    public Vector3 getListenerOrientationUp() {
        return this.orientationUp;
    }

    @Override
    public float getGain() {
        return this.gain;
    }

    @Override
    public void setGain(float gain) {
        this.gain = gain;
        AL10.alListenerf(AL10.AL_GAIN, gain);
        this.checkALError("setGain");
    }

    @Override
    public ISound getSound(String name) {
        return this.normalSounds.get(name);
    }

    @Override
    public ISound getMusic(String name) {
        return this.streamingSounds.get(name);
    }

    @Override
    public ISoundInstance playSound(String name, float localGain, float pitch, boolean looping, int priority) {
        if (this.normalSounds.containsKey(name)) {
            ISound s = this.normalSounds.get(name);
            s.setGain(localGain);
            s.setPitch(pitch);
            s.setLooping(looping);
            return s.play(priority, true);
        }

        return null;
    }

    @Override
    public void stopSounds(String name) {
        if (this.normalSounds.containsKey(name))
            this.normalSounds.get(name).stopAll();
    }

    @Override
    public void stopAllSounds() {
        this.normalSounds.forEach((k, v) -> v.stopAll());
    }

    @Override
    public ISoundInstance playMusic(String name, float localGain, boolean looping, boolean solo) {
        if (this.streamingSounds.containsKey(name)) {
            ISound s = this.streamingSounds.get(name);
            s.setGain(localGain);
            s.setLooping(looping);
            if (solo) {
                this.stopAllMusics();
            }

            return s.play();
        }

        return null;
    }

    @Override
    public void stopMusic(String name) {
        if (this.streamingSounds.containsKey(name))
            this.streamingSounds.get(name).stopAll();
    }

    @Override
    public void stopAllMusics() {
        this.streamingSounds.forEach((k, v) -> v.stopAll());
    }

    private void addLoadEntry(String name, String path, boolean streaming) {
        Optional<LoadEntry> entry;
        if ((entry = this.toLoad.stream().filter(e -> e.name.equals(name)).findFirst()).isPresent()) {
            entry.get().path = path;
            entry.get().streaming = streaming;
        } else {
            LoadEntry e = new LoadEntry();
            e.name = name;
            e.path = path;
            e.streaming = streaming;
            this.toLoad.add(e);
        }
    }

    @Override
    public void addSound(String name, String contentPath) {
        this.addLoadEntry(name, contentPath, false);
    }

    @Override
    public void addMusic(String name, String contentPath) {
        this.addLoadEntry(name, contentPath, true);
    }

    @Override
    public void finishLoading() {
        for (LoadEntry entry : this.toLoad) {
            if(entry.streaming) {
                //LwjglStreamingSound s = new LwjglStreamingSound(this, entry.path);
                LwjglSound s = new LwjglSound(this, entry.path);
                s.init();
                this.streamingSounds.put(entry.name, s);
            } else {
                LwjglSound s = new LwjglSound(this, entry.path);
                s.init();
                this.normalSounds.put(entry.name, s);
            }
        }

        this.toLoad.clear();
    }

    void checkALCError() {
        int e;
        if((e = ALC10.alcGetError(this.alcDevice)) != ALC10.ALC_NO_ERROR) {
            GabeLogger.error(ALC10.alcGetString(this.alcDevice, e), this, "checkALCError");
        }
    }

    void checkALError(String method) {
        int e;
        if((e = AL10.alGetError()) != AL10.AL_NO_ERROR) {
            GabeLogger.error(AL10.alGetString(e), this, method);
        }
    }

    private class LoadEntry {
        public String name;
        public String path;
        public boolean streaming;
    }
}
