package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.exception.ContextCreationException;
import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.game.GameWindowFactory.Api;
import cz.dat.gaben.api.interfaces.*;
import cz.dat.gaben.util.Vector2;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

public class LwjglGameWindow extends GameWindowBase {

    public static final int VERSION_MAJOR = 3, VERSION_MINOR = 1, FORWARD_COMPATIBILITY = 1;
    protected GLFWErrorCallback errorCallback;
    protected long windowHandle;
    private LwjglSoundManager sound;
    private LwjglInput input;
    private LwjglRenderer renderer;
    private LwjglTextureManager texture;
    private LwjglFontManager font;
    private LwjglFontRenderer fontr;
    private LwjglShaderManager shader;
    private LwjglFboManager fbo;
    private Settings settings;
    private boolean isFullscreen = false;
    private boolean isVSynced = false;
    private Vector2 lastScreenSize = new Vector2();

    public LwjglGameWindow(int width, int height, IContentManager content, Game game) {
        super(width, height, content, game);
        this.title = "GabeNgine LWJGL Game";
    }

    /**
     * Initializes GLFW window and LWJGL context
     */
    @Override
    @ExceptionHandling("GLFW_INIT")
    public void init() {
        GLFW.glfwSetErrorCallback(this.errorCallback = GLFWErrorCallback.createPrint());

        GLFWKeys.use();
        LwjglGlConstants.use();
        IRenderer.PrimitiveMode.init();

        if (!GLFW.glfwInit()) {
            throw ExceptionUtil.gameBreakingException(new ContextCreationException(
                    "GLFW init failed", ContextCreationException.FailedPhase.GLFW_INIT), this);
        }

        GLFW.glfwDefaultWindowHints();

        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GL11.GL_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_SAMPLES, 1);
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GL11.GL_FALSE);

        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR,
                LwjglGameWindow.VERSION_MAJOR);
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR,
                LwjglGameWindow.VERSION_MINOR);
        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT,
                LwjglGameWindow.FORWARD_COMPATIBILITY);

        if (LwjglGameWindow.VERSION_MAJOR >= 4 || (LwjglGameWindow.VERSION_MAJOR == 3 && LwjglGameWindow.VERSION_MINOR >= 3))
            GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);

        this.windowHandle = GLFW.glfwCreateWindow(this.getWidth(), this.getHeight(),
                this.getTitle(), 0, 0);
        if (this.windowHandle == 0) {
            throw ExceptionUtil.gameBreakingException(new ContextCreationException("Window creation failed (handle == 0)",
                    ContextCreationException.FailedPhase.WINDOW_INIT), this);
        }

        GLFW.glfwHideWindow(this.windowHandle);

        GLFWVidMode vidmode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());

        GLFW.glfwSetWindowPos(this.windowHandle,
                (vidmode.width() - this.getWidth()) / 2,
                (vidmode.height() - this.getHeight()) / 2);

        GLFW.glfwMakeContextCurrent(this.windowHandle);
        GLFW.glfwSwapInterval(0);
        GLFW.glfwMakeContextCurrent(this.windowHandle);

        GL.createCapabilities();
        GL11.glViewport(0, 0, this.getWidth(), this.getHeight());

        this.settings = new Settings();
        this.sound = new LwjglSoundManager(this);
        this.input = new LwjglInput(this);
        this.shader = new LwjglShaderManager(this);
        this.renderer = new LwjglRenderer(this);
        this.texture = new LwjglTextureManager(this.settings, this.content);
        this.font = new LwjglFontManager(this);
        this.fontr = new LwjglFontRenderer(this);
        this.fbo = new LwjglFboManager();

        super.init();
        GLFW.glfwShowWindow(this.windowHandle);
    }

    @Override
    protected void loop() {
        while (!GLFW.glfwWindowShouldClose(this.windowHandle)) {
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
            this.tick();
            if (this.ticks % 2 == 0)
                this.sound.updateAllMusic();
            GLFW.glfwSwapBuffers(this.windowHandle);
            GLFW.glfwPollEvents();
        }
    }

    @Override
    public void setWindowTitle(String title) {
        GLFW.glfwSetWindowTitle(this.windowHandle, title);
    }

    /**
     * Destroys LWJGL context, window and calls {@link System#exit(int)}
     */
    @Override
    public void cleanup() {
        this.sound.cleanup();
        this.input.cleanup();

        if (this.windowHandle != 0)
            GLFW.glfwDestroyWindow(this.windowHandle);

        GLFW.glfwTerminate();
        this.errorCallback.free();
        super.cleanup();
    }

    @Override
    public void resize(int width, int height) {
        if (this.screenSize.x() != width && this.screenSize.y() != height) {
            this.screenSize = new Vector2(width, height);
            this.centre = new Vector2(width / 2, height / 2);

            GLFW.glfwSetWindowSize(this.windowHandle, width, height);
            this.renderer.createOrthoMatrix();
            GL11.glViewport(0, 0, this.getWidth(), this.getHeight());
        }
    }

    @Override
    public void resize(boolean fullscreen, boolean keepResolution) {
        if (isFullscreen != fullscreen) {
            this.isFullscreen = fullscreen;
            long monitorHandle = GLFW.glfwGetPrimaryMonitor();

            if (!keepResolution && fullscreen) {
                GLFWVidMode m = GLFW.glfwGetVideoMode(monitorHandle);
                this.lastScreenSize = new Vector2(this.getWidth(), this.getHeight());
                this.screenSize = new Vector2(m.width(), m.height());
                this.centre = new Vector2(m.width() / 2, m.height() / 2);
            } else if (!keepResolution) {
                this.screenSize = new Vector2(this.lastScreenSize.x(), this.lastScreenSize.y());
                this.centre = new Vector2(this.lastScreenSize.x() / 2, this.lastScreenSize.y() / 2);
            }

            long newWindow = GLFW.glfwCreateWindow(this.getWidth(), this.getHeight(),
                    this.title, fullscreen ? monitorHandle : 0, this.windowHandle);

            GLFW.glfwDestroyWindow(this.windowHandle);
            this.windowHandle = newWindow;
            GLFW.glfwMakeContextCurrent(this.windowHandle);
            GLFW.glfwSwapInterval(this.isVSynced ? 1 : 0);
            GL.createCapabilities();
            this.renderer.createOrthoMatrix();
            GL11.glViewport(0, 0, this.getWidth(), this.getHeight());
            GLFW.glfwShowWindow(this.windowHandle);
        }
    }

    @Override
    public boolean isFullscreened() {
        return this.isFullscreen;
    }

    @Override
    public void setFpsLimit(int fpsLimit, boolean vSync) {
        GLFW.glfwSwapInterval(vSync ? 1 : 0);
        this.isVSynced = vSync;
    }

    @Override
    protected void preRenderTick(float ptt) {
    }

    @Override
    protected void postRenderTick() {
        this.getRenderer().flush();
    }

    @Override
    public IRenderer getRenderer() {
        return this.renderer;
    }

    @Override
    public ITextureManager getTexture() {
        return this.texture;
    }

    @Override
    public IFontManager getFont() {
        return this.font;
    }

    @Override
    public ISoundManager getSound() {
        return this.sound;
    }

    @Override
    public IInputManager getInput() {
        return this.input;
    }

    @Override
    public Settings getSettings() {
        return this.settings;
    }

    @Override
    public Api getApiType() {
        return Api.LWJGL;
    }

    @Override
    public void shutdown(int code) {
        super.shutdown(code);
        GLFW.glfwSetWindowShouldClose(this.windowHandle, true);
    }

	@Override
	public IFontRenderer getFontRenderer() {
		return this.fontr;
	}

	@Override
	public IShaderManager getShader() {
		return this.shader;
	}
	
	@Override
	public IFboManager getFbo() {
		return this.fbo;
	}

}
