package cz.dat.gaben.api.impl.lwjgl;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.interfaces.IFbo;
import cz.dat.gaben.api.interfaces.IFboManager;

public class LwjglFboManager implements IFboManager {
	
	//TODO CLEANUP
	
	Map<String, IFbo> fbos = new HashMap<>();
	
	@Override
	public void createFbo(String name, int width, int height, int attachments,
			Settings s) {
		
		int fboId = GL30.glGenFramebuffers();
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fboId);
		int[] texIds = new int[attachments];
		
		for (int i = 0; i < attachments; i++) {
			int id = GL11.glGenTextures();
			texIds[i] = id;
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, id);
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, (ByteBuffer)null);

	        s.getTexParameters().forEach((k, v) ->
            GL11.glTexParameteri(GL11.GL_TEXTURE_2D, k, v));
	        GL30.glFramebufferTexture2D(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0+i, GL11.GL_TEXTURE_2D, id, 0);
		}
		
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
		
		LwjglFbo fbo = new LwjglFbo(name, width, height, texIds, fboId);
		
		this.fbos.put(name, fbo);
	}

	@Override
	public IFbo getFbo(String name) {
		return this.fbos.get(name);
	}

}
