package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.ShaderFile;
import cz.dat.gaben.api.ShaderVariables;
import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.interfaces.IShader;
import cz.dat.gaben.api.interfaces.IShaderManager;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ExceptionHandling(value = "SHADERS", errorCode = 200)
public class LwjglShaderManager implements IShaderManager {

    public static final String DEFAULT_SHADER = "__DEFAULT__";
    public static final String PASS_SHADER = "__PASSTHROUGH__";
    private Map<String, LwjglShader> shaders;
    private GameWindowBase context;

    public LwjglShaderManager(GameWindowBase context) {
		this.context = context;
		this.shaders = new HashMap<>();
	}

    private static String getShaderLogInfo(int obj) {
        return GL20.glGetShaderInfoLog(obj, GL20.glGetShaderi(obj, GL20.GL_INFO_LOG_LENGTH));
    }

    private static String getProgramLogInfo(int obj) {
        return GL20.glGetProgramInfoLog(obj, GL20.glGetProgrami(obj, GL20.GL_INFO_LOG_LENGTH));
    }

    @Override
	public void loadShader(String name, String pathv, String pathf) {
		int progId = 0, vertId = 0, fragId = 0;

		ShaderFile vert;
		ShaderFile frag;

		if (name.equals(LwjglShaderManager.DEFAULT_SHADER)) {
            vert = readShader(LwjglShaderManager.class.getResourceAsStream("/cz/dat/gaben/api/impl/lwjgl/shaders/default_v.vsh"));
            frag = readShader(LwjglShaderManager.class.getResourceAsStream("/cz/dat/gaben/api/impl/lwjgl/shaders/default_f.fsh"));
        } else if (name.equals(LwjglShaderManager.PASS_SHADER)) {
            vert = readShader(LwjglShaderManager.class.getResourceAsStream("/cz/dat/gaben/api/impl/lwjgl/shaders/passthrough_v.vsh"));
            frag = readShader(LwjglShaderManager.class.getResourceAsStream("/cz/dat/gaben/api/impl/lwjgl/shaders/passthrough_f.fsh"));
        } else {
			vert = readShader(this.context.getContentManager().openStream(pathv));
			frag = readShader(this.context.getContentManager().openStream(pathf));
		}

        vertId = compileShader(name + "_vert", vert.getCode(), GL20.GL_VERTEX_SHADER);
        fragId = compileShader(name + "_frag", frag.getCode(), GL20.GL_FRAGMENT_SHADER);
        progId = attachShaders(name, vertId, fragId);

        GL20.glDeleteShader(vertId);
        GL20.glDeleteShader(fragId);

        Map<String, Integer> uniforms = new HashMap<>();
        Map<String, Integer> attributes = new HashMap<>();

        for (String s : vert.getUniforms()) {
        	int loc = GL20.glGetUniformLocation(progId, s);
        	uniforms.put(s, loc);
        }

        for (String s : frag.getUniforms()) {
        	int loc = GL20.glGetUniformLocation(progId, s);
        	uniforms.put(s, loc);
        }

        for (String s : vert.getAttributes()) {
        	int loc = GL20.glGetAttribLocation(progId, s);
        	attributes.put(s, loc);
        }

        ShaderVariables variables = new ShaderVariables(uniforms, attributes);

        LwjglShader shader = new LwjglShader(name, progId, variables);
        this.shaders.put(name, shader);
	}

    private ShaderFile readShader(InputStream in) {

        StringBuilder source = new StringBuilder();
        BufferedReader reader;

        List<String> uniforms = new ArrayList<>();
        List<String> attributes = new ArrayList<>();

        try {
            reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String line;
            while ((line = reader.readLine()) != null) {

                if (line.startsWith("uniform ")) {
            		String name = line.split(" ")[2];
            		name = name.replace(";", "");

                    if (name.endsWith("]")) {
                        int size = Integer.parseInt(name.substring(0, name.lastIndexOf("]")).substring(name.lastIndexOf("[") + 1));
                        name = name.substring(0, name.lastIndexOf("["));
            			for (int i = 0; i < size; i++) {
            				uniforms.add(name + "[" + i + "]");
            			}

                    } else {
            			uniforms.add(name);
            		}

                }

                if (line.startsWith("in ")) {
            		String name = line.split(" ")[2];
            		name = name.replace(";", "");
            		attributes.add(name);
            	}

                source.append(line).append("\n");
            }
            reader.close();
        } catch (Exception e) {
            throw ExceptionUtil.featureBreakingException(e, this);
        }

        ShaderFile sf = new ShaderFile(source.toString(), uniforms, attributes);

        return sf;
    }

    private int compileShader(String name, String shader, int type) {
        int id = 0;
        try {
            id = GL20.glCreateShader(type);

            GL20.glShaderSource(id, shader);
            GL20.glCompileShader(id);

            if (GL20.glGetShaderi(id, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
                throw ExceptionUtil.featureBreakingException("Error compiling shader [" + name + "] " + getShaderLogInfo(id), this);
            }


            return id;
        } catch (Exception e) {
            GL20.glDeleteShader(id);
            e.printStackTrace();
            return 0;
        }
    }

    private int attachShaders(String name, int vert, int frag) {
        int prog = GL20.glCreateProgram();

        try {
            GL20.glAttachShader(prog, vert);
            GL20.glAttachShader(prog, frag);

            GL20.glLinkProgram(prog);

            if (GL20.glGetProgrami(prog, GL20.GL_LINK_STATUS) == GL11.GL_FALSE) {
                throw ExceptionUtil.featureBreakingException("Error linking shader program [" + name + "] "
                        + getProgramLogInfo(prog), this);
            }

            /*GL20.glValidateProgram(prog);

            if (GL20.glGetProgrami(prog, GL20.GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
                throw ExceptionUtil.featureBreakingException("Error validating shader program [" + name + "] "
                        + getProgramLogInfo(prog), this);
            }*/
        } catch (Exception e) {
            GL20.glDeleteProgram(prog);
            e.printStackTrace();
        }

        return prog;
    }
	
	@Override
	public IShader getShader(String name) {
		return this.shaders.get(name);
	}

}
