package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.interfaces.IInputManager;

public class GLFWKeys {
    public static void use() {
        IInputManager.Keys.UNKNOWN = -1;
        IInputManager.Keys.SPACE = 32;
        IInputManager.Keys.APOSTROPHE = 39;
        IInputManager.Keys.COMMA = 44;
        IInputManager.Keys.MINUS = 45;
        IInputManager.Keys.PERIOD = 46;
        IInputManager.Keys.SLASH = 47;
        IInputManager.Keys.N0 = 48;
        IInputManager.Keys.N1 = 49;
        IInputManager.Keys.N2 = 50;
        IInputManager.Keys.N3 = 51;
        IInputManager.Keys.N4 = 52;
        IInputManager.Keys.N5 = 53;
        IInputManager.Keys.N6 = 54;
        IInputManager.Keys.N7 = 55;
        IInputManager.Keys.N8 = 56;
        IInputManager.Keys.N9 = 57;
        IInputManager.Keys.SEMICOLON = 59;
        IInputManager.Keys.EQUAL = 61;
        IInputManager.Keys.A = 65;
        IInputManager.Keys.B = 66;
        IInputManager.Keys.C = 67;
        IInputManager.Keys.D = 68;
        IInputManager.Keys.E = 69;
        IInputManager.Keys.F = 70;
        IInputManager.Keys.G = 71;
        IInputManager.Keys.H = 72;
        IInputManager.Keys.I = 73;
        IInputManager.Keys.J = 74;
        IInputManager.Keys.K = 75;
        IInputManager.Keys.L = 76;
        IInputManager.Keys.M = 77;
        IInputManager.Keys.N = 78;
        IInputManager.Keys.O = 79;
        IInputManager.Keys.P = 80;
        IInputManager.Keys.Q = 81;
        IInputManager.Keys.R = 82;
        IInputManager.Keys.S = 83;
        IInputManager.Keys.T = 84;
        IInputManager.Keys.U = 85;
        IInputManager.Keys.V = 86;
        IInputManager.Keys.W = 87;
        IInputManager.Keys.X = 88;
        IInputManager.Keys.Y = 89;
        IInputManager.Keys.Z = 90;
        IInputManager.Keys.LEFT_BRACKET = 91;
        IInputManager.Keys.BACKSLASH = 92;
        IInputManager.Keys.RIGHT_BRACKET = 93;
        IInputManager.Keys.GRAVE_ACCENT = 96;
        IInputManager.Keys.ESCAPE = 256;
        IInputManager.Keys.ENTER = 257;
        IInputManager.Keys.TAB = 258;
        IInputManager.Keys.BACKSPACE = 259;
        IInputManager.Keys.INSERT = 260;
        IInputManager.Keys.DELETE = 261;
        IInputManager.Keys.RIGHT = 262;
        IInputManager.Keys.LEFT = 263;
        IInputManager.Keys.DOWN = 264;
        IInputManager.Keys.UP = 265;
        IInputManager.Keys.PAGE_UP = 266;
        IInputManager.Keys.PAGE_DOWN = 267;
        IInputManager.Keys.HOME = 268;
        IInputManager.Keys.END = 269;
        IInputManager.Keys.CAPS_LOCK = 280;
        IInputManager.Keys.SCROLL_LOCK = 281;
        IInputManager.Keys.NUM_LOCK = 282;
        IInputManager.Keys.PRINT_SCREEN = 283;
        IInputManager.Keys.PAUSE = 284;
        IInputManager.Keys.F1 = 290;
        IInputManager.Keys.F2 = 291;
        IInputManager.Keys.F3 = 292;
        IInputManager.Keys.F4 = 293;
        IInputManager.Keys.F5 = 294;
        IInputManager.Keys.F6 = 295;
        IInputManager.Keys.F7 = 296;
        IInputManager.Keys.F8 = 297;
        IInputManager.Keys.F9 = 298;
        IInputManager.Keys.F10 = 299;
        IInputManager.Keys.F11 = 300;
        IInputManager.Keys.F12 = 301;
        IInputManager.Keys.F13 = 302;
        IInputManager.Keys.F14 = 303;
        IInputManager.Keys.F15 = 304;
        IInputManager.Keys.F16 = 305;
        IInputManager.Keys.F17 = 306;
        IInputManager.Keys.F18 = 307;
        IInputManager.Keys.F19 = 308;
        IInputManager.Keys.F20 = 309;
        IInputManager.Keys.F21 = 310;
        IInputManager.Keys.F22 = 311;
        IInputManager.Keys.F23 = 312;
        IInputManager.Keys.F24 = 313;
        IInputManager.Keys.F25 = 314;
        IInputManager.Keys.KP_0 = 320;
        IInputManager.Keys.KP_1 = 321;
        IInputManager.Keys.KP_2 = 322;
        IInputManager.Keys.KP_3 = 323;
        IInputManager.Keys.KP_4 = 324;
        IInputManager.Keys.KP_5 = 325;
        IInputManager.Keys.KP_6 = 326;
        IInputManager.Keys.KP_7 = 327;
        IInputManager.Keys.KP_8 = 328;
        IInputManager.Keys.KP_9 = 329;
        IInputManager.Keys.KP_DECIMAL = 330;
        IInputManager.Keys.KP_DIVIDE = 331;
        IInputManager.Keys.KP_MULTIPLY = 332;
        IInputManager.Keys.KP_SUBTRACT = 333;
        IInputManager.Keys.KP_ADD = 334;
        IInputManager.Keys.KP_ENTER = 335;
        IInputManager.Keys.KP_EQUAL = 336;
        IInputManager.Keys.LEFT_SHIFT = 340;
        IInputManager.Keys.LEFT_CONTROL = 341;
        IInputManager.Keys.LEFT_ALT = 342;
        IInputManager.Keys.LEFT_SUPER = 343;
        IInputManager.Keys.RIGHT_SHIFT = 344;
        IInputManager.Keys.RIGHT_CONTROL = 345;
        IInputManager.Keys.RIGHT_ALT = 346;
        IInputManager.Keys.RIGHT_SUPER = 347;
        IInputManager.Keys.MENU = 348;
        IInputManager.Keys.LAST = 348;

        IInputManager.Keys.MOUSE_LEFT = 0;
        IInputManager.Keys.MOUSE_MIDDLE = 1;
        IInputManager.Keys.MOUSE_RIGHT = 2;
    }
}