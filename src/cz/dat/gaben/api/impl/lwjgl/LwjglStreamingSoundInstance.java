package cz.dat.gaben.api.impl.lwjgl;

import cz.dat.gaben.api.interfaces.ISound;
import cz.dat.gaben.api.interfaces.ISoundInstance;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.Vector3;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;

import java.nio.FloatBuffer;

public class LwjglStreamingSoundInstance implements ISoundInstance {

    int priority;
    int source;

    private boolean looping;
    private LwjglStreamingSound sound;
    private FloatBuffer valuesBuffer;

    LwjglStreamingSoundInstance(LwjglStreamingSound sound) {
        this.sound = sound;
    }

    @Override
    public ISound getSound() {
        return this.sound;
    }

    @Override
    public boolean isFreeToUse() {
        return this.getState() == ISound.SoundState.STOPPED;
    }

    @Override
    public void init() {
        this.source = AL10.alGenSources();
        this.checkALError("init");
        this.valuesBuffer = BufferUtils.createFloatBuffer(3);
    }

    @Override
    public void play() {
        this.sound.d.play(this.source, this.sound.buffers);
        this.checkALError("play");
    }

    @Override
    public void pause() {
        AL10.alSourcePause(this.source);
    }

    @Override
    public void stop() {
        this.sound.d.shouldExit = true;
    }

    @Override
    public void seek(float time) {
        // TODO
        this.sound.d.skipTo(time);
    }

    @Override
    public float getPosition() {
        return this.sound.d.getProgressTime();
    }

    @Override
    public void setPosition(float time) {
        // TODO
        this.sound.d.skipTo(time);
    }

    @Override
    public boolean isLooping() {
        return this.looping;
    }

    @Override
    public void setLooping(boolean looping) {
        this.looping = looping;
    }

    @Override
    public float getGain() {
        float f = AL10.alGetSourcef(AL10.AL_GAIN, this.source);
        this.checkALError("getGain");
        return f;
    }

    @Override
    public void setGain(float gain) {
        AL10.alSourcef(this.source, AL10.AL_GAIN, gain);
        this.checkALError("setGain");
    }

    @Override
    public float getPitch() {
        float f = AL10.alGetSourcef(AL10.AL_PITCH, this.source);
        this.checkALError("getPitch");
        return f;
    }

    @Override
    public void setPitch(float pitch) {
        AL10.alSourcef(this.source, AL10.AL_PITCH, pitch);
        this.checkALError("setPitch");
    }

    @Override
    public void setSpacePosition(float x, float y, float z) {
        AL10.alSource3f(this.source, AL10.AL_POSITION, x, y, z);
        this.checkALError("setSpacePosition");
    }

    @Override
    public Vector3 getSpacePosition() {
        Vector3 v = this.getUsingBuffer(AL10.AL_POSITION);
        this.checkALError("getSpacePosition");
        return v;
    }

    @Override
    public void setSpaceVelocity(float x, float y, float z) {
        AL10.alSource3f(this.source, AL10.AL_VELOCITY, x, y, z);
        this.checkALError("setSpaceVelocity");
    }

    @Override
    public Vector3 getSpaceVelocity() {
        Vector3 v = this.getUsingBuffer(AL10.AL_VELOCITY);
        this.checkALError("getSpaceVelocity");
        return v;
    }

    @Override
    public void setSoundPositioning(boolean relativeToListener) {
        AL10.alSourcei(this.source, AL10.AL_SOURCE_RELATIVE, relativeToListener ? AL10.AL_TRUE : AL10.AL_FALSE);
        this.checkALError("setSoundPositioning");
    }

    @Override
    public boolean isPositionedRelativeToListener() {
        boolean b = AL10.alGetSourcei(AL10.AL_SOURCE_RELATIVE, this.source) == AL10.AL_TRUE;
        this.checkALError("isPositionedRelativeToListener");
        return b;
    }

    @Override
    public void setDirection(float x, float y, float z) {
        AL10.alSource3f(this.source, AL10.AL_DIRECTION, x, y, z);
        this.checkALError("setDirection");
    }

    @Override
    public Vector3 getDirection() {
        Vector3 v = this.getUsingBuffer(AL10.AL_DIRECTION);
        this.checkALError("getDirection");
        return v;
    }

    private Vector3 getUsingBuffer(int property) {
        this.valuesBuffer.flip();
        AL10.alSourcefv(this.source, property, this.valuesBuffer);
        this.valuesBuffer.flip();
        return new Vector3(this.valuesBuffer.get(), this.valuesBuffer.get(), this.valuesBuffer.get());
    }

    @Override
    public void setCone(float innerAngle, float outerAngle, float outerGain) {
        AL10.alSourcef(this.source, AL10.AL_CONE_INNER_ANGLE, innerAngle);
        AL10.alSourcef(this.source, AL10.AL_CONE_OUTER_ANGLE, outerAngle);
        AL10.alSourcef(this.source, AL10.AL_CONE_OUTER_GAIN, outerGain);
        this.checkALError("setCone");
    }

    @Override
    public Vector3 getCone() {
        Vector3 v = new Vector3(AL10.alGetSourcef(this.source, AL10.AL_CONE_INNER_ANGLE),
                AL10.alGetSourcef(this.source, AL10.AL_CONE_OUTER_ANGLE),
                AL10.alGetSourcef(this.source, AL10.AL_CONE_OUTER_GAIN));
        this.checkALError("getCone");
        return v;
    }

    @Override
    public ISound.SoundState getState() {
        ISound.SoundState s = ISound.SoundState.getForAl(AL10.alGetSourcei(this.source, AL10.AL_SOURCE_STATE));
        this.checkALError("getState");
        return s;
    }

    private void checkALError(String method) {
        int e;
        if ((e = AL10.alGetError()) != AL10.AL_NO_ERROR) {
            GabeLogger.error(AL10.alGetString(e), this, method);
        }
    }
}
