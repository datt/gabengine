package cz.dat.gaben.api.impl;

import cz.dat.gaben.api.exception.ExceptionHandling;
import cz.dat.gaben.api.exception.ExceptionUtil;
import cz.dat.gaben.api.interfaces.IContentManager;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.NumberPacker;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExceptionHandling(value = "CONTENT", errorCode = 203)
public class PackedContentManager implements IContentManager {

    private String fileName;
    private boolean isRes;
    private LZ4FastDecompressor decompressor;
    private int tempNum;
    private FileSystemEntry root;
    private List<FileSystemEntry> files = new ArrayList<>();

    public PackedContentManager(String fileName, boolean fromResource) {
        this.fileName = fileName;
        this.isRes = fromResource;
        this.tempNum = (int) (Math.random() * Integer.MAX_VALUE);
    }

    @Override
    public void load() {
        this.decompressor = LZ4Factory.fastestInstance().fastDecompressor();

        this.files.clear();
        this.root = new FileSystemEntry(null, "", false, null);
        this.files.add(this.root);

        try {
            FileInfo[] infos = this.readHeader(this.getFileStream());
            byte[] intb = new byte[4];

            InputStream is = this.getFileStream();

            is.skip(7);
            is.read(intb, 0, 4);
            int dataStart = NumberPacker.unpackInt(intb);
            is.skip(dataStart - 7 - 4);

            for (FileInfo info : infos) {
                String tmp = File.createTempFile("gabengine-" + this.tempNum, ".tmp").getAbsolutePath();

                byte[] source = new byte[info.end - info.start + 1];
                byte[] restored = new byte[info.decLength];
                is.read(source);
                this.decompressor.decompress(source, 0, restored, 0, info.decLength);

                FileOutputStream fos = new FileOutputStream(tmp);
                fos.write(restored);
                fos.close();

                FileSystemEntry e = this.makeEntry(info);
                e.fsPath = tmp;
                e.calcSize();
            }
            return;
        } catch (FileNotFoundException e) {
            throw ExceptionUtil.featureBreakingException("Specified resources file doesn't exist.", e, this);
        } catch (IOException e) {
            throw ExceptionUtil.featureBreakingException("Error reading from file.", e, this);
        }
    }

    private FileSystemEntry makeEntry(FileInfo info) {
        String name, parentPath;

        int laos = info.name.lastIndexOf('/');
        if (laos == -1) {
            name = info.name;
            parentPath = "";
        } else {
            name = info.name.substring(laos + 1);
            parentPath = info.name.substring(0, laos);
        }

        FileSystemEntry parentDir = this.findEntry(parentPath, false);

        if (parentDir == null) {
            parentDir = this.root;
            int l = parentPath.indexOf('/');
            String lookingFor = l == -1 ? parentPath : parentPath.substring(0, l);

            while (true) {
                FileSystemEntry newParentDir = this.findEntry(lookingFor, false);

                if (newParentDir == null) {
                    l = lookingFor.lastIndexOf('/');
                    FileSystemEntry e = new FileSystemEntry(parentDir, l == -1 ? lookingFor : lookingFor.substring(l + 1),
                            false, null);
                    parentDir = e;
                    this.files.add(e);

                    if (lookingFor.equals(parentPath)) {
                        parentDir = e;
                        break;
                    }

                    l = parentPath.indexOf('/', lookingFor.length() + 1);
                    if (l == -1) {
                        lookingFor = parentPath;
                    } else {
                        lookingFor = parentPath.substring(0, l);
                    }
                }
            }
        }

        FileSystemEntry e = new FileSystemEntry(parentDir, name, true, info);
        this.files.add(e);
        return e;
    }

    private FileSystemEntry findEntry(String name, boolean file) {
        Stream<FileSystemEntry> s = this.files.stream().filter(fs -> fs.fullPath.equals(name) && fs.isFile == file);
        Optional<FileSystemEntry> o = s.findFirst();
        return o.isPresent() ? o.get() : null;
    }

    private InputStream getFileStream() throws FileNotFoundException {
        InputStream fileIs;

        if (this.isRes) {
            fileIs = this.getClass().getResourceAsStream(this.fileName);
            if (fileIs == null)
                throw new FileNotFoundException(this.fileName);
        } else {
            fileIs = new FileInputStream(new File(this.fileName));
        }

        return fileIs;
    }

    private FileInfo[] readHeader(InputStream s) {
        byte[] intb = new byte[4];

        try {
            byte[] header = new byte[7];
            s.read(header);

            if (!new String(header, "ASCII").equals("RESFILE"))
                throw ExceptionUtil.featureBreakingException("Resource file is in wrong format - header not found", this);

            s.read(intb);
            s.read(intb);
            int fileCount = NumberPacker.unpackInt(intb);
            GabeLogger.info("Resource files count: " + fileCount);

            FileInfo[] ret = new FileInfo[fileCount];
            for (int i = 0; i < fileCount; i++) {
                ret[i] = new FileInfo();

                s.read(intb, 0, 2);
                int fnl = NumberPacker.unpackShort(intb);

                byte[] str = new byte[fnl];
                s.read(str);
                // TODO: change to UTF8
                ret[i].name = new String(str, "UTF-8");

                s.read(intb);
                ret[i].start = NumberPacker.unpackInt(intb);

                s.read(intb);
                ret[i].end = NumberPacker.unpackInt(intb);

                s.read(intb);
                ret[i].decLength = NumberPacker.unpackInt(intb);
            }

            s.close();
            return ret;
        } catch (IOException e) {
            throw ExceptionUtil.featureBreakingException(e, this);
        }
    }

    @Override
    public InputStream openStream(String name) {
        try {
            FileSystemEntry e = this.findEntry(name, true);
            if (e != null)
                return new FileInputStream(e.fsPath);
            else
                throw ExceptionUtil.nonBreakingException("Specified path is not an existing file", this);
        } catch (FileNotFoundException e) {
            throw ExceptionUtil.nonBreakingException("Couldn't open file for reading (perhaps it was deleted after startup?)", e, this);
        }
    }

    //RESFILE
    //data start offset (int 4 bytes)
    //number of files (int 4 bytes)
    //name length (short 2 bytes)
    //file name
    //start (int 4 bytes)
    //end (int 4 bytes)
    //decompressed size (int 4 bytes)

    //name length (short 2 bytes)
    //file name
    //...

    @Override
    public int getSize(String name) {
        FileSystemEntry e = this.findEntry(name, true);
        if (e != null)
            return e.sizeBytes;
        else return -1;
    }

    @Override
    public List<String> getFilesInDirectory(String name) {
        FileSystemEntry e = this.findEntry(name, false);
        if (e != null) {
            return e.children.stream().filter(en -> en.isFile).map(en -> en.fullPath).collect(Collectors.toList());
        } else return null;
    }

    @Override
    public List<String> getDirectoriesInDirectory(String name) {
        FileSystemEntry e = this.findEntry(name, false);
        if (e != null) {
            return e.children.stream().filter(en -> !en.isFile).map(en -> en.fullPath).collect(Collectors.toList());
        } else return null;
    }

    @Override
    public void cleanup() {
        this.files.stream().filter(file -> file.fsPath != null).forEach(file -> {
            new File(file.fsPath).deleteOnExit();
        });
    }

    private class FileInfo {
        public int start, end, decLength;
        public String name;
    }

    private class FileSystemEntry {
        private FileSystemEntry parent;
        private List<FileSystemEntry> children;
        private String fullPath;
        private String name;
        private boolean isFile;
        private FileInfo info;
        private String fsPath;
        private int sizeBytes;

        public FileSystemEntry(FileSystemEntry parent, String name, boolean isFile, FileInfo info) {
            if (!isFile) {
                this.children = new ArrayList<>();
            }

            this.parent = parent;
            this.name = name;
            this.isFile = isFile;
            this.info = info;

            if (parent == null) {
                this.fullPath = "";
            } else {
                if (parent == root) {
                    this.fullPath = this.name;
                } else {
                    this.fullPath = this.parent.fullPath + "/" + this.name;
                }
                this.parent.children.add(this);
            }
        }

        @ExceptionHandling(errorCode = 204, value = "CONTENT_LOAD")
        private void calcSize() {
            try {
                InputStream s = new FileInputStream(this.fsPath);
                this.sizeBytes = this.getBytes(s).length;
            } catch (IOException e) {
                throw ExceptionUtil.featureBreakingException("Couldn't calculate size of a content", e, this);
            }
        }

        private byte[] getBytes(InputStream is) throws IOException {
            int len;
            int size = 1024;
            byte[] buf;

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            buf = new byte[size];
            while ((len = is.read(buf, 0, size)) != -1)
                bos.write(buf, 0, len);
            buf = bos.toByteArray();
            bos.close();

            return buf;
        }
    }
}
