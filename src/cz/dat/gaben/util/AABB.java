package cz.dat.gaben.util;

public class AABB extends Rectangle {

    private float eps = 0;

    public AABB(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    public AABB(AABB original) {
        this(original.x1(), original.y1(), original.width(), original.height());
    }


    public AABB mix(AABB second, float p) {
        float deltaX0 = second.position.x - this.position.x;
        float deltaY0 = second.position.y - this.position.y;
        float deltaW = second.width() - this.width();
        float deltaH = second.height() - this.height();

        float x0 = this.position.x + deltaX0 * p;
        float y0 = this.position.y + deltaY0 * p;
        float x1 = this.width() + deltaW * p;
        float y1 = this.height() + deltaH * p;

        return new AABB(x0, y0, x1, y1);
    }

    public AABB expand(float xa, float ya) {
        float _x0 = this.position.x;
        float _y0 = this.position.y;
        float _x1 = this.maxPosition.x;
        float _y1 = this.maxPosition.y;

        if (xa < 0.0F) {
            _x0 += xa;
        }

        if (xa > 0.0F) {
            _x1 += xa;
        }

        if (ya < 0.0F) {
            _y0 += ya;
        }

        if (ya > 0.0F) {
            _y1 += ya;
        }

        return new AABB(_x0, _y0, _x1, _y1);
    }

    public float clipXCollide(AABB c, float xa) {
        if (c.maxPosition.y > this.position.y && c.position.y < this.maxPosition.y) {

            float max;
            if (xa > 0.0F && c.maxPosition.x <= this.position.x) {
                max = this.position.x - c.maxPosition.x - this.eps;
                if (max < xa) {
                    xa = max;
                }
            }

            if (xa < 0.0F && c.position.x >= this.maxPosition.x) {
                max = this.maxPosition.x - c.position.x + this.eps;
                if (max > xa) {
                    xa = max;
                }
            }
        }

        return xa;
    }

    public float clipYCollide(AABB c, float ya) {
        if (c.maxPosition.x > this.position.x && c.position.x < this.maxPosition.x) {

            float max;
            if (ya > 0.0F && c.maxPosition.y <= this.position.y) {
                max = this.position.y - c.maxPosition.y - this.eps;
                if (max < ya) {
                    ya = max;
                }
            }

            if (ya < 0.0F && c.position.y >= this.maxPosition.y) {
                max = this.maxPosition.y - c.position.y + this.eps;
                if (max > ya) {
                    ya = max;
                }
            }
        }

        return ya;
    }
}
