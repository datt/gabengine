package cz.dat.gaben.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class OpenGLConstantsScrapper {
    public static void main(String[] args) {
        List<Class> cl = new ArrayList<>();

        for (int i = 11; i <= 45; i++) {
            try {
                Class c = Class.forName("org.lwjgl.opengl.GL" + i);
                cl.add(c);
            } catch (ClassNotFoundException ignored) {
            }
        }
/*
        cl.add(AL10.class);
        cl.add(AL11.class);
        cl.add(ALC10.class);
        cl.add(ALC11.class);*/

        System.out.print(OpenGLConstantsScrapper.generateImplementationClass("LwjglGlConstants", "GlConstants", cl.toArray(new Class[0])));
    }

    private static void scrap(Class klazz) {
        for (Field field : klazz.getDeclaredFields()) {
            try {
                System.out.println("[" + field.getGenericType().getTypeName() + "] " + field.getName() + ": " + field.get(null).toString());
            } catch (IllegalAccessException e) {
                System.out.println("Couldn't read " + field.getName());
            }
        }
    }

    private static String generateBaseClass(String name, Class[] klazzez) {
        StringBuilder b = new StringBuilder();
        b.append("public class ").append(name).append(" {\n");

        for (Class c : klazzez) {
            b.append("// --- ").append(c.getCanonicalName().substring(c.getCanonicalName().lastIndexOf('.') + 1)).append(" ---\n");
            for (Field field : c.getDeclaredFields()) {
                b.append("public static ").append(field.getGenericType().getTypeName()).append(' ')
                        .append(field.getName().replace("GL_", "")).append(";\n");
            }
        }

        b.append('}');
        return b.toString();
    }

    private static String generateImplementationClass(String name, String baseName, Class[] klazzez) {
        StringBuilder b = new StringBuilder();
        b.append("public class ").append(name).append(" {\n");
        b.append("public void use() {\n");

        for (Class c : klazzez) {
            b.append("// --- ").append(c.getCanonicalName().substring(c.getCanonicalName().lastIndexOf('.') + 1)).append(" ---\n");
            for (Field field : c.getDeclaredFields()) {

                b.append(baseName).append('.').append(field.getName().replace("GL_", "")).append(" = ")
                        .append(c.getCanonicalName().substring(c.getCanonicalName().lastIndexOf('.') + 1))
                        .append('.').append(field.getName()).append(";\n");

            }
        }

        b.append("}\n}");
        return b.toString();
    }
}
