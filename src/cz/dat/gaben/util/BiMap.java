package cz.dat.gaben.util;

import java.util.Hashtable;
import java.util.Map;

public class BiMap<K, V> {

    private Map<K, V> forward = new Hashtable<>();
    private Map<V, K> backward = new Hashtable<>();

    public synchronized void put(K key, V value) {
        forward.put(key, value);
        backward.put(value, key);
    }

    public synchronized void removeForward(K key) {
        V b = forward.get(key);
        forward.remove(key);
        backward.remove(b);
    }

    public synchronized void removeBackward(V value) {
        K b = backward.get(value);
        backward.remove(value);
        forward.remove(b);
    }

    public synchronized boolean containsForward(K key) {
        return forward.containsKey(key);
    }

    public synchronized boolean containsBackward(V key) {
        return backward.containsKey(key);
    }

    public synchronized V getForward(K key) {
        return forward.get(key);
    }

    public synchronized K getBackward(V key) {
        return backward.get(key);
    }
}
