package cz.dat.gaben.util;

import cz.dat.gaben.api.exception.Gabexception;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GabeLogger {

    private static Logger jl;
    private static boolean verbose = true;

    public static void exception(Gabexception exception) {
        if(exception.getUrgency() == Gabexception.Urgency.BREAKING)
            error(exception);
        else
            warning(exception);

        if(verbose && exception.getCause() != null)
            exception.getCause().printStackTrace();
    }

    public static void debug(String debug) {
        jl.log(Level.FINE, debug);
    }

    public static void debug(String debug, Object caller, String sourceMethod) {
        jl.logp(Level.FINE, caller.getClass().getName(), sourceMethod, debug);
    }

    public static void init(Class cl) {
        jl = Logger.getLogger(cl.getName());
    }

    public static void info(String info) {
        jl.log(Level.INFO, info);
    }

    public static void info(String info, Object caller, String sourceMethod) {
        jl.logp(Level.INFO, caller.getClass().getName(), sourceMethod, info);
    }

    public static void warning(String warning) {
        jl.log(Level.WARNING, warning);
    }

    public static void warning(String warning, Object caller, String sourceMethod) {
        jl.logp(Level.WARNING, caller.getClass().getName(), sourceMethod, warning);

    }

    public static void warning(Gabexception exception) {
        warning("[" + exception.getFeatureBroken() + "] " + exception.getMessage(), exception.getSource(), null);
    }

    public static void error(String error) {
        jl.log(Level.SEVERE, error);
    }

    public static void error(String error, Object caller, String sourceMethod) {
        jl.logp(Level.SEVERE, caller.getClass().getName(), sourceMethod, error);
    }

    public static void error(Gabexception exception) {
        error("[" + exception.getFeatureBroken() + "] " + exception.getMessage(), exception.getSource(), null);
    }

    public static void config(String changedSetting, String message) {
        jl.log(Level.CONFIG, message, changedSetting);
    }
}
