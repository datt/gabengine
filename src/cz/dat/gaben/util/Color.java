package cz.dat.gaben.util;

public class Color {
    // ABGR

    public static final int RED = 0xFF0000FF;
    public static final int GREEN = 0xFF00FF00;
    public static final int BLUE = 0xFFFF0000;
    public static final int WHITE = 0xFFFFFFFF;
    public static final int BLACK = 0xFF000000;
    public static final int TRANSPARENT = 0x00FFFFFF;

    public static int color(float r, float g, float b, float a) {
        return Color.color((int)(r * 255), (int)(g * 255), (int)(b * 255), (int)(a * 255));
    }

    public static int color(int r, int g, int b, int a) {
        return (((a & 0xFF) << 24) | ((b & 0xFF) << 16) | ((g & 0xFF) << 8) | (r & 0xFF));
    }

    public static int r(int color) {
        return color & 0xFF;
    }

    public static int g(int color) {
        return (color >> 8) & 0xFF;
    }

    public static int b(int color) {
        return (color >> 16) & 0xFF;
    }

    public static int a(int color) {
        return (color >> 24) & 0xFF;
    }

    public static float af(int color) { return a(color) / 255f; }

    public static float rf(int color) { return r(color) / 255f; }

    public static float gf(int color) { return g(color) / 255f; }

    public static float bf(int color) { return b(color) / 255f; }

    public static int negative(int other) {
        int r = Color.r(other);
        int g = Color.g(other);
        int b = Color.b(other);
        int a = Color.a(other);

        return Color.color(255 - r, 255 - g, 255 - b, 255 - a);
    }

    public static float[] toHsv(int color) {
        float hue, saturation, brightness;
        float[] hsbvals = new float[3];

        int b = Color.b(color);
        int g = Color.g(color);
        int r = Color.r(color);

        int cmax = (r > g) ? r : g;
        if (b > cmax) cmax = b;
        int cmin = (r < g) ? r : g;
        if (b < cmin) cmin = b;
        brightness = ((float) cmax) / 255.0f;
        if (cmax != 0)
            saturation = ((float) (cmax - cmin)) / ((float) cmax);
        else
            saturation = 0;
        if (saturation == 0)
            hue = 0;
        else {
            float redc = ((float) (cmax - r)) / ((float) (cmax - cmin));
            float greenc = ((float) (cmax - g)) / ((float) (cmax - cmin));
            float bluec = ((float) (cmax - b)) / ((float) (cmax - cmin));
            if (r == cmax)
                hue = bluec - greenc;
            else if (g == cmax)
                hue = 2.0f + redc - bluec;
            else
                hue = 4.0f + greenc - redc;
            hue = hue / 6.0f;
            if (hue < 0)
                hue = hue + 1.0f;
        }

        hsbvals[0] = hue;
        hsbvals[1] = saturation;
        hsbvals[2] = brightness;

        return hsbvals;
    }

    public static int fromHsv(float hue, float saturation, float value) {
        int r = 0, g = 0, b = 0;
        if (saturation == 0) {
            r = g = b = (int) (value * 255.0f + 0.5f);
        } else {
            float h = (hue - (float) Math.floor(hue)) * 6.0f;
            float f = h - (float) java.lang.Math.floor(h);
            float p = value * (1.0f - saturation);
            float q = value * (1.0f - saturation * f);
            float t = value * (1.0f - (saturation * (1.0f - f)));
            switch ((int) h) {
                case 0:
                    r = (int) (value * 255.0f + 0.5f);
                    g = (int) (t * 255.0f + 0.5f);
                    b = (int) (p * 255.0f + 0.5f);
                    break;
                case 1:
                    r = (int) (q * 255.0f + 0.5f);
                    g = (int) (value * 255.0f + 0.5f);
                    b = (int) (p * 255.0f + 0.5f);
                    break;
                case 2:
                    r = (int) (p * 255.0f + 0.5f);
                    g = (int) (value * 255.0f + 0.5f);
                    b = (int) (t * 255.0f + 0.5f);
                    break;
                case 3:
                    r = (int) (p * 255.0f + 0.5f);
                    g = (int) (q * 255.0f + 0.5f);
                    b = (int) (value * 255.0f + 0.5f);
                    break;
                case 4:
                    r = (int) (t * 255.0f + 0.5f);
                    g = (int) (p * 255.0f + 0.5f);
                    b = (int) (value * 255.0f + 0.5f);
                    break;
                case 5:
                    r = (int) (value * 255.0f + 0.5f);
                    g = (int) (p * 255.0f + 0.5f);
                    b = (int) (q * 255.0f + 0.5f);
                    break;
            }
        }

        return Color.color(r, g, b, 1f);
    }

    public static int inverse(int other) {
        float[] hsv = Color.toHsv(other);
        hsv[0] = 1f - hsv[0];
        hsv[1] = 1f - hsv[1];
        hsv[2] = 1f - hsv[2];

        return Color.fromHsv(hsv[0], hsv[1], hsv[2]);
    }

}
