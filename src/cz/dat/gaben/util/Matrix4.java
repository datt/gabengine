package cz.dat.gaben.util;

import org.lwjgl.BufferUtils;

import cz.dat.gaben.api.interfaces.IMatrix;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class Matrix4 implements IMatrix {

    private float[] data;

    public Matrix4() {
        this.data = new float[4*4];
    }
    
    public Matrix4(float[] data) {
        this.data = data;
    }

    public static Matrix4 createOrthoMatrix(float l, float r, float b, float t, float n, float f) {
        float[] dataArray = new float[]{
                2f / (r - l), 0, 0, 0,
                0, 2f / (t - b), 0, 0,
                0, 0, -2f / (f - n), 0,
                -(r + l) / (r - l), -(t + b) / (t - b), -(f + n) / (f - n), 1
        };
        
        Matrix4 mat = new Matrix4(dataArray);

        return mat;
    }
    
    public static Matrix4 createIdentityMatrix() {
        float[] dataArray = new float[]{
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        };
        
        Matrix4 mat = new Matrix4(dataArray);

        return mat;
    }
    
    public Matrix4 mult(Matrix4 mat) {
    	float[] dataArray = new float[16];
    	
    	float[] thisData = this.data;
    	float[] otherData = mat.getData();
    	
    	for (int i = 0; i < 4; i++)
            for (int j = 0; j < 4; j++)
                for (int k = 0; k < 4; k++)
                	dataArray[j*4 + i] += thisData[k*4 + i] * otherData[j*4 + k];
    	
    	return new Matrix4(dataArray);
    }
    
    public Matrix4 translate(float x, float y) {
        float[] dataArray = new float[]{
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                x, y, 0, 1
        };
    	
    	return new Matrix4(dataArray).mult(this);
    }
    
    public Matrix4 scale(float w, float h) {
        float[] dataArray = new float[]{
                w, 0, 0, 0,
                0, h, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        };
    	
        return new Matrix4(dataArray).mult(this);
    }
    
    public Matrix4 rotate(float angleDeg) {
    	float angle = (float) (-angleDeg * Math.PI/180f);
        float sina = (float) Math.sin(angle);
    	float cosa = (float) Math.cos(angle);
    	
        float[] dataArray = new float[]{
                cosa, -sina, 0, 0,
                sina, cosa, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
        };
        
    	return new Matrix4(dataArray).mult(this);
    }
    
    public Matrix4 rotateFrom(float x, float y, float angleDeg) {
        return this.translate(-x, -y).rotate(angleDeg).translate(x, y);
    }
    
    public Matrix4 scaleFrom(float x, float y, float w, float h) {
        return this.translate(-x, -y).scale(w, h).translate(x, y);
    }
    
    public Matrix4 scaleRotateFrom(float x, float y, float w, float h, float angleDeg) {
        return this.translate(-x, -y).scale(w, h).rotate(angleDeg).translate(x, y);
    }

    @Override
    public float[] getData() {
        return this.data;
    }

    @Override
    public void setData(float[] data) {
        this.data = data;
    }

}
