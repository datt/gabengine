package cz.dat.gaben.util;

import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("Duplicates")
public class ResourcePackMaker {

    private static Map<String, CompressedFile> files = new HashMap<>();
    private static LZ4Compressor comp = LZ4Factory.fastestInstance().fastCompressor();

    public static void main(String[] args) {
        loadDir(new File(".\\res_test\\snow_test"), null);
        files.forEach((k, f) -> {
            try {
                compressFile(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        try {
            saveData(new FileOutputStream(new File(".\\res_test\\pack\\snow_resources")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void saveData(OutputStream o) {
        int off = 7 + 4 + 4 + (files.size() * 14);
        off += files.entrySet().stream().mapToInt(v -> v.getValue().nameBytes.length).sum();
        System.out.println(off);
        int pos = 0;

        try {
            o.write("RESFILE".getBytes("ASCII"));
            o.write(NumberPacker.pack(off));
            o.write(NumberPacker.pack(files.size()));

            for (Map.Entry<String, CompressedFile> e : files.entrySet()) {
                o.write(NumberPacker.pack((short) e.getValue().nameBytes.length));
                o.write(e.getValue().nameBytes);
                o.write(NumberPacker.pack(pos));
                o.write(NumberPacker.pack(pos + e.getValue().compressed.length - 1));
                o.write(NumberPacker.pack(e.getValue().originalSize));
                pos += e.getValue().compressed.length;
            }

            for (Map.Entry<String, CompressedFile> e : files.entrySet()) {
                o.write(e.getValue().compressed);
            }

            o.flush();
            o.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //RESFILE
    //data start offset (int 4 bytes)
    //number of files (int 4 bytes)

    //name length (short 2 bytes)
    //file name
    //start (int 4 bytes)
    //end (int 4 bytes)
    //decompressed size (int 4 bytes)

    //name length (short 2 bytes)
    //file name
    //...

    private static void loadDir(File fileDir, String root) {
        if (root == null)
            root = fileDir.getAbsolutePath();

        for (File f : fileDir.listFiles()) {
            String name = f.getName();

            if (f.isFile()) {
                String n = f.getAbsolutePath().replace(root, "").replace("\\", "/");
                int i = n.lastIndexOf('.');
                CompressedFile cf = new CompressedFile();
                cf.file = f;
                cf.name = n.substring(1, i >= 1 ? i : n.length() - 1);
                try {
                    cf.nameBytes = cf.name.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                cf.originalSize = (int) f.length();
                files.put(cf.name, cf);
            } else {
                loadDir(f, root);
            }
        }
    }

    private static void compressFile(CompressedFile f) throws IOException {
        int maxComp = comp.maxCompressedLength(f.originalSize);
        f.compressed = new byte[maxComp];
        comp.compress(getBytes(new FileInputStream(f.file)), 0, f.originalSize, f.compressed, 0, maxComp);
    }

    private static byte[] getBytes(InputStream is) throws IOException {
        int len;
        int size = 1024;
        byte[] buf;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        buf = new byte[size];
        while ((len = is.read(buf, 0, size)) != -1)
            bos.write(buf, 0, len);
        buf = bos.toByteArray();
        bos.close();

        return buf;
    }

    private static class CompressedFile {
        private String name;
        private byte[] nameBytes;
        private File file;
        private byte[] compressed;
        private int originalSize;
    }
}
