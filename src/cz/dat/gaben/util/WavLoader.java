package cz.dat.gaben.util;

import java.io.DataInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.function.Function;

public class WavLoader {
    private InputStream stream;
    private long chunkSize;
    private long subChunk1Size;
    private int format;
    private long channels;
    private long sampleRate;
    private long byteRate;
    private int blockAlign;
    private int bitsPerSample;
    private long dataSize;
    private Function<Integer, ByteBuffer> bufGen;
    private ByteBuffer buffer;

    public WavLoader(InputStream s, Function<Integer, ByteBuffer> bufferGenerator) {
        this.stream = s;
        this.bufGen = bufferGenerator;
    }

    private static int byteArrayToInt(byte[] b) {
        int start = 0;
        int low = b[start] & 0xff;
        int high = b[start + 1] & 0xff;
        return (int) (high << 8 | low);
    }

    private static long byteArrayToLong(byte[] b) {
        int start = 0;
        int i = 0;
        int len = 4;
        int cnt = 0;
        byte[] tmp = new byte[len];
        for (i = start; i < (start + len); i++) {
            tmp[cnt] = b[i];
            cnt++;
        }
        long accum = 0;
        i = 0;
        for (int shiftBy = 0; shiftBy < 32; shiftBy += 8) {
            accum |= ((long) (tmp[i] & 0xff)) << shiftBy;
            i++;
        }
        return accum;
    }

    public long getChunkSize() {
        return this.chunkSize;
    }

    public void setChunkSize(long chunkSize) {
        this.chunkSize = chunkSize;
    }

    public long getSubChunk1Size() {
        return this.subChunk1Size;
    }

    public void setSubChunk1Size(long subChunk1Size) {
        this.subChunk1Size = subChunk1Size;
    }

    public int getFormat() {
        return this.format;
    }

    public void setFormat(int format) {
        this.format = format;
    }

    public long getChannels() {
        return this.channels;
    }

    public void setChannels(long channels) {
        this.channels = channels;
    }

    public long getSampleRate() {
        return this.sampleRate;
    }

    public void setSampleRate(long sampleRate) {
        this.sampleRate = sampleRate;
    }

    public long getByteRate() {
        return this.byteRate;
    }

    public void setByteRate(long byteRate) {
        this.byteRate = byteRate;
    }

    public int getBlockAlign() {
        return this.blockAlign;
    }

    public void setBlockAlign(int blockAlign) {
        this.blockAlign = blockAlign;
    }

    public int getBitsPerSample() {
        return this.bitsPerSample;
    }

    public void setBitsPerSample(int bitsPerSample) {
        this.bitsPerSample = bitsPerSample;
    }

    public long getDataSize() {
        return this.dataSize;
    }

    public void setDataSize(long dataSize) {
        this.dataSize = dataSize;
    }

    public ByteBuffer getBuffer() {
        return this.buffer;
    }

    public boolean read() {
        DataInputStream inFile;
        byte[] tmpLong = new byte[4];
        byte[] tmpInt = new byte[2];

        try {
            inFile = new DataInputStream(this.stream);
            String chunkID = "" + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte();
            if (!chunkID.equals("RIFF"))
                return false;

            inFile.read(tmpLong); // read the ChunkSize
            this.chunkSize = byteArrayToLong(tmpLong);

            String format = "" + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte();
            if (!format.equals("WAVE"))
                return false;

            String subChunk1ID = "" + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte();

            inFile.read(tmpLong); // read the SubChunk1Size
            this.subChunk1Size = byteArrayToLong(tmpLong);

            inFile.read(tmpInt); // read the audio format.  This should be 1 for PCM
            this.format = byteArrayToInt(tmpInt);

            inFile.read(tmpInt); // read the # of channels (1 or 2)
            this.channels = byteArrayToInt(tmpInt);

            inFile.read(tmpLong); // read the samplerate
            this.sampleRate = byteArrayToLong(tmpLong);

            inFile.read(tmpLong); // read the byterate
            this.byteRate = byteArrayToLong(tmpLong);

            inFile.read(tmpInt); // read the blockalign
            this.blockAlign = byteArrayToInt(tmpInt);

            inFile.read(tmpInt); // read the bitspersample
            this.bitsPerSample = byteArrayToInt(tmpInt);

            String dataChunkID = "" + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte();

            inFile.read(tmpLong); // read the size of the data
            this.dataSize = byteArrayToLong(tmpLong);

            this.buffer = this.bufGen.apply((int) dataSize);
            ReadableByteChannel c = Channels.newChannel(inFile);
            c.read(this.buffer);
            this.buffer.flip();

            c.close();
            inFile.close();
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}