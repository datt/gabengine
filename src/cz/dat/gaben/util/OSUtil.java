package cz.dat.gaben.util;

import javax.swing.*;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OSUtil {

    public static boolean isWindows() {
        return System.getProperty("os.name").toUpperCase().contains("WIN");
    }

    public static boolean isMac() {
        return System.getProperty("os.name").toUpperCase().contains("MAC");
    }

    public static boolean isJVM64bit() {
        return System.getProperty("os.arch").contains("64");
    }

    public static boolean isWindows64bit() {
        return System.getenv("ProgramFiles(x86)") != null;
    }

    public static String getAppDataPath() {
        return isWindows() ? System.getenv("APPDATA") : System.getProperty("user.home");
    }


    public static void relaunchOnMac(Class main, String[] args, boolean keepInstance) {
        final String relaunchedArg = "-Dcz.dat.gaben.util.relaunched";

        if (OSUtil.isMac()) {

            String home = System.getProperty("java.home");
            String ourBeloved = Paths.get(home, "bin", "java").toAbsolutePath().toString();
            RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
            List<String> jvmArgs = new ArrayList<>();
            jvmArgs.add(ourBeloved);
            jvmArgs.addAll(bean.getInputArguments());
            if (jvmArgs.contains(relaunchedArg)) {
                return;
            } else {
                jvmArgs.add(relaunchedArg);
            }
            jvmArgs.add("-XstartOnFirstThread");
            jvmArgs.add("-classpath");
            jvmArgs.add(System.getProperty("java.class.path"));
            jvmArgs.add(main.getCanonicalName());
            Collections.addAll(jvmArgs, args);

            if(JOptionPane.showConfirmDialog(null, "We are sorry, GabeNgine is currently not supported on Mac." +
                    " The game will probably not be able to start. Would you like to try anyway?", "GabeNgine",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

                ProcessBuilder b = new ProcessBuilder(jvmArgs);
                b.command().forEach(System.out::println);
                try {
                    if (keepInstance) {
                        b.redirectOutput();
                        b.redirectInput();
                        b.redirectError();
                        b.start().waitFor();
                    } else {
                        b.start();
                        Thread.sleep(2000);
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }

            System.exit(0);
        }
    }
}
