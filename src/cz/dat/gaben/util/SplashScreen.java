package cz.dat.gaben.util;

import cz.dat.gaben.api.game.Game;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.io.IOException;

public class SplashScreen extends JFrame {
    private static final long serialVersionUID = 1L;

    private Image i;

    public SplashScreen() {
        this(SplashScreen.getDefaultImage());
    }

    public SplashScreen(Image image) {
        this.i = image;
        this.setUndecorated(true);
        this.setSize(i.getWidth(null), i.getHeight(null));
        this.setPreferredSize(new Dimension(i.getWidth(null), i.getHeight(null)));
        this.setLocationRelativeTo(null);
        this.setAlwaysOnTop(true);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setLayout(new BorderLayout());
    }

    private static Image getDefaultImage() {
        try {
            return ImageIO.read(Game.class
                    .getResourceAsStream("GabeNgine_small.png"));
        } catch (IOException e) {
            return null;
        }
    }

    /*
    @Override
    public void setVisible(boolean arg0) {
        super.setVisible(arg0);

        if (arg0) {
            try {
                AudioInputStream audioInputStream;
                audioInputStream = AudioSystem.getAudioInputStream(this
                        .getClass().getResourceAsStream("hch.wav"));

                Clip clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                volume.setValue(-20f);

                clip.start();
            } catch (UnsupportedAudioFileException | IOException
                    | LineUnavailableException ignored) {
            }
        }
    }
    */

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.drawImage(this.i, 0, 0, null);
        g.setColor(Color.BLACK);
        g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
    }
}
