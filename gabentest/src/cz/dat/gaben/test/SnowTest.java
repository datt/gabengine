package cz.dat.gaben.test;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.game.GameWindowFactory;
import cz.dat.gaben.api.game.GameWindowFactory.Api;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.impl.PackedContentManager;
import cz.dat.gaben.api.interfaces.*;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.Rectangle;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class SnowTest extends ScreenBase {

    public static final int BLUR_DIVIDER = 1;
    private List<Rectangle> rec = new ArrayList<>();
    private Random rnd = new Random();
    private float speed = 0.2f;
    private ITexture snowTexture, logoTexture;
    private IShader blurShader;
    private IFbo f1, f2;
    private ISoundInstance lastPlayingSound;
    private float distMultiplier = 1.0f;
    private int iterations = 8;

    public SnowTest(Game game, String title) {
        super(game, title);

        this.snowTexture = game.getApi().getTexture().getTexture("snowflake");
        this.logoTexture = game.getApi().getTexture().getTexture("gabengineLogo");

        this.blurShader = game.getApi().getShader().getShader("blur");

        this.f1 = game.getApi().getFbo().getFbo("f1");
        this.f2 = game.getApi().getFbo().getFbo("f2");
    }

    public static void main(String args[]) throws FileNotFoundException {
        GameWindowFactory.enableSplashscreen(true, null);
        GabeLogger.init(SnowTest.class);
        GameWindowBase window = GameWindowFactory.createGameOrDie(Api.LWJGL, 1280,
                720, new PackedContentManager("./res_test/pack/snow_resources", false), true, new Game() {
                    public void init() {
                        super.init();

                        //this.getWindow().setFpsLimit(0, true);

                        this.getApi().getTexture().loadTexture("snowflake", "chromeIcon");
                        this.getApi().getTexture().loadTexture("gabengineLogo", "GabeNgine_small");
                        this.getApi().getTexture().finishLoading();

                        this.getApi().getSound().addSound("snd", "hch");
                        this.getApi().getSound().addMusic("music", "s");
                        this.getApi().getSound().finishLoading();

                        this.getApi().getShader().loadShader("blur", "blur2", "blurf2");
                        this.getApi().getRenderer().shader(this.getApi().getShader().getShader("blur"));
                        this.getApi().getRenderer().setupShader(this.getApi().getShader().getShader("blur"), false);

                        Settings ssss = new Settings();
                        ssss.setMinFilter(Settings.Filters.LINEAR);
                        ssss.setMagFilter(Settings.Filters.LINEAR);
                        ssss.setWrapS(Settings.Wrap.CLAMP_TO_EDGE);
                        ssss.setWrapT(Settings.Wrap.CLAMP_TO_EDGE);

                        this.getApi().getFbo().createFbo("f1", 1280 / BLUR_DIVIDER, 720 / BLUR_DIVIDER, 1, ssss);
                        this.getApi().getFbo().createFbo("f2", 1280 / BLUR_DIVIDER, 720 / BLUR_DIVIDER, 1, ssss);

                        SnowTest s = new SnowTest(this, "Merry Christmas");
                        this.addScreen(0, s);
                        this.openScreen(0);
                    }
                });

        window.start();
    }

    @Override
    public void tick() {
        for (int i = 0; i < 1; i++) {
            int w = 60 + rnd.nextInt(20);
            Rectangle r = new Rectangle(rnd.nextInt(this.game.getWidth() + w) - w,
                    -w * 2, w, w);
            this.rec.add(r);
        }

        for (int i = 0; i < 2; i++) {
            int w = 40 + rnd.nextInt(20);
            Rectangle r = new Rectangle(rnd.nextInt(this.game.getWidth() + w) - w,
                    -w * 2, w, w);
            this.rec.add(r);
        }

        for (int i = 0; i < 4 + rnd.nextInt(12); i++) {
            int w = 20 + rnd.nextInt(20);
            Rectangle r = new Rectangle(rnd.nextInt(this.game.getWidth() + w) - w,
                    -w * 2, w, w);
            this.rec.add(r);
        }

        for (int i = 0; i < 8 + rnd.nextInt(12); i++) {
            int w = 1 + rnd.nextInt(20);
            Rectangle r = new Rectangle(rnd.nextInt(this.game.getWidth() + w) - w,
                    -w * 2, w, w);
            this.rec.add(r);
        }


        for (Iterator<Rectangle> iterator = rec.iterator(); iterator.hasNext(); ) {
            Rectangle r = iterator.next();
            r.setPosition(r.x1(), r.y1() + this.speed * r.width());

            if (r.y1() > 720) {
                iterator.remove();
            }
        }

        if (this.game.getApi().getInput().isKeyDown(IInputManager.Keys.A)) {
            this.distMultiplier -= 0.05f;
            GabeLogger.info("" + this.distMultiplier, this, "Distance multiplier");
        }

        if (this.game.getApi().getInput().isKeyDown(IInputManager.Keys.D)) {
            this.distMultiplier += 0.05f;
            GabeLogger.info("" + this.distMultiplier, this, "Distance multiplier");
        }
        
        if (this.distMultiplier < 0) {
            this.distMultiplier = 0;
        }
    }

    @Override
    public void renderTick(float ptt) {
        IRenderer g = this.game.getApi().getRenderer();

        g.fbo(this.f1);
        g.defaultShader();

        g.clear();
        g.color(0xFFFFFFFF);

        g.enableTexture(true);

        for (Rectangle r : this.rec) {
            float cY = (this.speed * r.width()) * ptt;
            g.drawTexture(this.snowTexture, r.x1(), r.y1() + cY, r.x2(), r.y2() + cY);
        }

        IFbo drawFbo = this.f2;
        IFbo readFbo = this.f1;

        for (int i = 0; i < this.iterations; i++) {
            g.fbo(i == (this.iterations - 1) ? null : drawFbo);
            g.shader(this.blurShader);
            g.bindFboTexture(readFbo, 0, 0);
            this.blurShader.setUniform1f("iteration", (i + 1.0f) * this.distMultiplier);
            this.blurShader.setUniform2f("screenSize", 1280 / BLUR_DIVIDER, 720 / BLUR_DIVIDER);
            g.drawFullscreenQuad();

            IFbo temp = drawFbo;
            drawFbo = readFbo;
            readFbo = temp;
        }

        g.defaultShader();
        g.drawTexture(this.logoTexture, this.getGame().getWidth() / 2 - 320, this.game.getHeight() / 2 - 84);
        if (this.lastPlayingSound != null && this.lastPlayingSound.getState() == ISound.SoundState.PLAYING) {
            g.color(Color.BLACK);
            g.enableTexture(false);
            g.drawRect(10, this.game.getHeight() - 20,
                    (this.game.getWidth() - 10) *
                            (this.lastPlayingSound.getPosition() / this.lastPlayingSound.getSound().getLength()),
                    this.game.getHeight() - 10);
        }
    }

    @Override
    public void onKeyDown(int k) {
        if (k == IInputManager.Keys.P) {
            this.game.getApi().getSound().playMusic("music", 1f, false, true);
            this.game.getApi().getInput().setMouseCursorVisible(false);
        }

        if (k == IInputManager.Keys.X) {
            this.iterations--;
            
            if (this.iterations < 1) {
                this.iterations = 1;
            }
            
            GabeLogger.info("" + this.iterations, this, "Iterations");
        }

        if (k == IInputManager.Keys.V) {
            this.iterations++;
            GabeLogger.info("" + this.iterations, this, "Iterations");
        }

        if (k == IInputManager.Keys.L) {
            this.lastPlayingSound = this.game.getApi().getSound().playSound("snd", 1f, 1f, false);
        }

        if (k == IInputManager.Keys.LEFT) {
            this.lastPlayingSound.seek(-1);
        }

        if (k == IInputManager.Keys.RIGHT) {
            this.lastPlayingSound.seek(1);
        }

        if (k == IInputManager.Keys.S) {
            this.game.getWindow().shutdown(0);
        }

        if (k == IInputManager.Keys.F) {
            this.game.getWindow().resize(640, 480);
        }
    }
}