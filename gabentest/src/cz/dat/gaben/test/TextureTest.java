package cz.dat.gaben.test;

import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowFactory;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.impl.ContentManager;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.api.interfaces.ITextureManager;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.GabeLogger;

import java.io.FileNotFoundException;
import java.util.Random;

public class TextureTest extends ScreenBase {

    int numTextures;
    Random rnd = new Random();

    public TextureTest(Game game, int numTextures) {
        super(game, "GabeNgine LWJGL Texture Test");
        this.numTextures = numTextures;
    }

    public static void main(String[] args) throws FileNotFoundException {
        GameWindowFactory.enableSplashscreen(true, null);
        GabeLogger.init(TextureTest.class);
        GameWindowFactory.createGame(GameWindowFactory.Api.LWJGL, 1280, 720,
                new ContentManager("./res_test", false), new Game() {
                    @Override
                    public void init() {
                        super.init();

                        final int[] i = {0};
                        this.getContent().getFilesInDirectory("texture_test/").forEach(s -> this.getApi().getTexture().loadTexture("t" + i[0]++, s));
                        this.getContent().getDirectoriesInDirectory("texture_test").forEach(System.out::println);
                        this.getApi().getTexture().loadTexture("glogo", "GabeNgine_small");
                        this.getApi().getTexture().finishLoading();

                        this.addScreen(0, new TextureTest(this, i[0]));
                        this.openScreen(0);
                    }
                }).run();
    }

    @Override
    public void tick() {

    }

    @Override
    public void renderTick(float ptt) {
        IRenderer g = this.game.getApi().getRenderer();
        ITextureManager t = this.game.getApi().getTexture();

        if (this.game.getApi().getInput().isMouseButtonDown(0)) {
            g.color(Color.BLUE);
            g.clear();
            g.color(Color.WHITE);
        }

        int x = 0;
        int y = 0;
        int largestY = 0;

        for (int i = 0; i < this.numTextures; i++) {
            ITexture tex = t.getTexture("t"+i);

            if (this.game.getApi().getInput().isMouseButtonDown(0)) {
                g.drawTexture(tex, (float) Math.sin(ptt) * x, y);
            } else {
                g.drawTexture(tex, x, y);
            }

            x += tex.getSize().x();
            if (tex.getSize().y() > largestY)
                largestY = (int) tex.getSize().y();
            if (x > this.getGame().getWidth()) {
                x = 0;
                y += largestY;
            }
        }

        g.drawTexture(t.getTexture("glogo"), 10, 10);
    }
}
