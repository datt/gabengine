package cz.dat.gaben.test;

import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.game.GameWindowFactory;
import cz.dat.gaben.api.impl.ContentManager;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.GabeLogger;

import java.io.FileNotFoundException;

public class ColoursTest extends Game {
    int color = 0xFF000000;
    float f = 0;

    public static void main(String[] args) throws FileNotFoundException {
        GameWindowFactory.enableSplashscreen(true, null);
        GabeLogger.init(SnowTest.class);
        GameWindowBase window = GameWindowFactory.createGame(GameWindowFactory.Api.LWJGL, 800,
                800, new ContentManager("./res_test", false), new ColoursTest());
        window.start();
    }

    @Override
    public void onFpsCount() {
        //super.onFpsCount();
    }

    @Override
    public void init() {
        super.init();
        this.getWindow().setWindowTitle("Bablbam");
    }

    @Override
    public void onTick() {
        super.onTick();
        f += 0.01f;
        this.color = Color.color(f, f, f, 1f);
    }

    @Override
    public void onRenderTick(float ptt) {
        super.onRenderTick(ptt);
        this.getApi().getRenderer().clearColor(this.color);
        // WHITE
        this.getApi().getRenderer().color(0xFFFFFFFF);
        this.getApi().getRenderer().drawRect(0, 0, 100, 100);
        // RED
        this.getApi().getRenderer().color(0xFF0000FF);
        this.getApi().getRenderer().drawRect(100, 0, 200, 100);
        // GREEN
        this.getApi().getRenderer().color(0xFF00FF00);
        this.getApi().getRenderer().drawRect(200, 0, 300, 100);
        // BLUE
        this.getApi().getRenderer().color(0xFFFF0000);
        this.getApi().getRenderer().drawRect(300, 0, 400, 100);
        // YELLOW
        this.getApi().getRenderer().color(0xFF00FFFF);
        this.getApi().getRenderer().drawRect(400, 0, 500, 100);
        // MAGENTA
        this.getApi().getRenderer().color(0xFFFF00FF);
        this.getApi().getRenderer().drawRect(500, 0, 600, 100);
        // CYAN
        this.getApi().getRenderer().color(0xFFFFFF00);
        this.getApi().getRenderer().drawRect(600, 0, 700, 100);
        // BLACK
        this.getApi().getRenderer().color(0xFF000000);
        this.getApi().getRenderer().drawRect(700, 0, 800, 100);

        this.getApi().getRenderer().primitiveMode(IRenderer.PrimitiveMode.TRIANGLES);
        this.getApi().getRenderer().color(Color.RED);
        this.getApi().getRenderer().vertex(0, 200);
        this.getApi().getRenderer().color(Color.GREEN);
        this.getApi().getRenderer().vertex(600, 200);
        this.getApi().getRenderer().color(Color.BLUE);
        this.getApi().getRenderer().vertex(0, 800);
    }
}
