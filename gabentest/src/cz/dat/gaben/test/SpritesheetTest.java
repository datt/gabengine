package cz.dat.gaben.test;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.game.GameWindowFactory;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.impl.ContentManager;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.GabeLogger;
import cz.dat.gaben.util.Vector2;

import java.io.FileNotFoundException;

public class SpritesheetTest extends ScreenBase {
    
    int c = 0;

    public SpritesheetTest(Game game) {
        super(game, "Spritesheet Test");
    }

    public static void main(String args[]) throws FileNotFoundException {
        GameWindowFactory.enableSplashscreen(true, null);
        GabeLogger.init(SpritesheetTest.class);
        GameWindowBase window = GameWindowFactory.createGame(GameWindowFactory.Api.LWJGL, 1280,
                720, new ContentManager("./res_test", false), new Game() {
                    public void init() {
                        super.init();

                        long start = System.nanoTime();
                        this.getApi().getTexture().loadSpritesheet("destroyStages", "texture_test/destroy_stage_%n", 5);
                        this.getApi().getTexture().loadSpritesheet("terrain", "texture_test/terrain", new Vector2(32, 32));
                        this.getApi().getFont().loadFont("font", "font", 48);
                        this.getApi().getTexture().finishLoading();
                        this.getApi().getShader().loadShader("blue", "cv", "cf");
                        this.getApi().getRenderer().shader(this.getApi().getShader().getShader("blue"));
                        this.getApi().getRenderer().setupShader(this.getApi().getShader().getShader("blue"), false);
                        
                        Settings ssss = new Settings();
                        ssss.setMinFilter(Settings.Filters.LINEAR);
                        ssss.setMagFilter(Settings.Filters.LINEAR);
                        
                        this.getApi().getFbo().createFbo("test", 1280, 720, 1, ssss);
                        System.out.println("Texture load time in ms: " + (System.nanoTime() - start) / 1000000F);

                        SpritesheetTest s = new SpritesheetTest(this);
                        this.addScreen(0, s);
                        this.openScreen(0);
                    }
                });

        if (window != null) {
            window.start();
        }
    }

    @Override
    public void tick() {
        //this.animation.tick();
        c++;
    }

    @Override
    public void renderTick(float ptt) {
    	IRenderer g = this.game.getApi().getRenderer();
    	
        g.setMatrix(g.identityMatrix().rotateFrom(640, 360, (c+ptt)*0.25f*2.5f));
    	
        g.fbo(this.game.getApi().getFbo().getFbo("test"));
        g.defaultShader();
        
        boolean mb = this.game.getApi().getInput().isMouseButtonDown(IInputManager.Keys.MOUSE_LEFT);
        
        g.clearColor(mb ? Color.BLACK : Color.WHITE);
        g.clear();

        g.color(Color.WHITE);
        int x = 0;
        int y = 0;
 
        //System.out.println(this.game.getApi().getTexture().getSpritesheet("terrain").keySet().toString());
        
        for (int i = 0; i < 256; i++) {
            g.drawTexture(this.game.getApi().getTexture().getTexture("terrain", "TEX_" + (i % 16) + "_" + (i / 16)), x, y);
            x += 32;

            if (x >= this.getGame().getWidth()) {
                x = 0;
                y += 32;
            }
        }

        Vector2 mousePos = this.game.getApi().getInput().getMousePosition();
        Vector2 objPos = new Vector2(800, 400);

        double atan = Math.toDegrees(Math.atan2((mousePos.y() - objPos.y()), (mousePos.x() - objPos.x()))) - 90;

        g.drawTextureRotated(this.game.getApi().getTexture().getTexture("terrain", "TEX_" + 15 + "_" + 0), 800, 400, (float) (atan), 1);
        
        g.color(mb ? Color.WHITE : Color.BLACK);

        this.game.getApi().getFontRenderer().setSize((float) Math.abs((Math.sin(System.nanoTime() / 3000000000D) * 72)));
        this.game.getApi().getFontRenderer().setAnchor(Anchor.BOTTOM_RIGHT);
        this.game.getApi().getFontRenderer().setFont(this.game.getApi().getFont().getFont("font"));
        this.game.getApi().getFontRenderer().drawString("#HoloYOLO, lel", 1280-10, 720);
       
        this.game.getApi().getFontRenderer().setAnchor(Anchor.TOP_LEFT);
        
        float yo = 0;
        
        for (int i = 4; i < 56; i += 4) {
        	this.game.getApi().getFontRenderer().setSize(i);
        	
        	String b = "This is a test";
        	
        	if (i == this.game.getApi().getFont().getFont("font").getNominalSize()) {
        		b += " (font's nominal size)";
        	}
        	
        	this.game.getApi().getFontRenderer().drawString(b, 100, 250+yo);
        	yo += this.game.getApi().getFontRenderer().getStringHeight(b);
        } 
        
        g.fbo(null);
    	
        //g.shader(this.game.getApi().getShader().getShader("blue"));
        //g.setMatrix(g.identityMatrix());
        //this.game.getApi().getShader().getShader("blue").setUniform1f("time", (c+ptt)*0.01f);
        g.bindFboTexture(this.game.getApi().getFbo().getFbo("test"), 0, 0);
        g.passthroughShader();
        g.drawFullscreenQuad();
    }
}
