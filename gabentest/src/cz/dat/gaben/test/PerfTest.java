package cz.dat.gaben.test;

import java.io.FileNotFoundException;

import cz.dat.gaben.api.impl.ContentManager;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.game.GameWindowFactory;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.IRenderer.PrimitiveMode;
import cz.dat.gaben.api.interfaces.IRenderer.ShapeMode;
import cz.dat.gaben.api.game.GameWindowFactory.Api;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.GabeLogger;

public class PerfTest extends ScreenBase {

    public PerfTest(Game game, String title) {
        super(game, title);
    }

    public static void main(String[] args) throws FileNotFoundException {
        GameWindowFactory.enableSplashscreen(true, null);
        GabeLogger.init(SnowTest.class);
        GameWindowBase window = GameWindowFactory.createGame(Api.LWJGL, 1280,
                720, new ContentManager("./res_test", false), new Game() {

            public void init() {
                super.init();

                this.getApi().getTexture().finishLoading();

                PerfTest s = new PerfTest(this, "Gabengine performance test");
                this.addScreen(0, s);
                this.openScreen(0);
            }
        });
        
        window.start();
    }

    @Override
    public void tick() {
        // Nothing
    }

    @Override
    public void renderTick(float ptt) {
        IRenderer r = this.game.getApi().getRenderer();
        
        r.enableTexture(false);
        r.shapeMode(ShapeMode.FILLED);
        r.primitiveMode(PrimitiveMode.TRIANGLES);
        
        float div = 5;
        
        for (int x = 0; x < 1280/div; x++) {
            for (int y = 0; y < 720/div; y++) {
                float xm = x * div;
                float ym = y * div;
                
                r.color(Color.RED);
                r.vertex(xm, ym);
                r.color(Color.GREEN);
                r.vertex(xm + div, ym);
                r.color(Color.BLUE);
                r.vertex(xm, ym + div);
                
            }
        }
        
        r.flush();
    }
    
}
