package cz.dat.gaben.test;

import cz.dat.gaben.api.data.IDataProvider;
import cz.dat.gaben.api.data.TextDataProvider;

public class DataTest {
    public static void main(String[] args) throws Exception {
        long time = System.nanoTime();
        long constructionTime = 0, loadTime = 0, readTime = 0, saveTime = 0;

        IDataProvider provider = new TextDataProvider("E:\\test.txt");
        System.out.println("Construction 1: " + (System.nanoTime() - time));

        Employee test = new Employee();
        test.name = "Test Employee";
        test.address = "Test Address";
        test.number = 123456789;

        time = System.nanoTime();
        provider.setData("int_val", 5, Integer.class);
        provider.setData("bool_val", true, Boolean.class);
        provider.setData("str_val", "It should work", String.class);
        provider.setData("float_val", 6.92f, Float.class);
        provider.setData("oth_val", test, Employee.class);
        System.out.println("Setting data: " + (System.nanoTime() - time));

        time = System.nanoTime();
        int i = provider.getData("int_val", Integer.class);
        boolean b = provider.getData("bool_val", Boolean.class);
        String s = provider.getData("str_val", String.class);
        float f = provider.getData("float_val", Float.class);
        Employee e = provider.getData("oth_val", Employee.class);
        System.out.println("Getting data 1: " + (System.nanoTime() - time));

        System.out.println("Integer: " + i);
        System.out.println("Float: " + f);
        System.out.println("Boolean: " + b);
        System.out.println("String: " + s);
        System.out.println("Employee: " + e.toString());

        System.out.println();
        System.out.println("Starting real measurement");
        System.out.println("-------------------------");

        int steps = 100000;
        for (int c = 0; c < steps; c++) {
            time = System.nanoTime();
            provider.sync(IDataProvider.MergeType.PREFER_MEMORY);
            saveTime += (System.nanoTime() - time);

            time = System.nanoTime();
            provider = new TextDataProvider("E:\\test.txt");
            constructionTime += (System.nanoTime() - time);

            time = System.nanoTime();
            provider.sync(IDataProvider.MergeType.PREFER_LOCAL);
            loadTime += (System.nanoTime() - time);

            time = System.nanoTime();
            i = provider.getData("int_val", Integer.class);
            b = provider.getData("bool_val", Boolean.class);
            s = provider.getData("str_val", String.class);
            f = provider.getData("float_val", Float.class);
            e = provider.getData("oth_val", Employee.class);
            readTime += (System.nanoTime() - time);
        }

        constructionTime /= steps;
        loadTime /= steps;
        readTime /= steps;
        saveTime /= steps;

        System.out.println("Saving: " + (saveTime / 1000) + " μs");
        System.out.println("Construction: " + (constructionTime / 1000) + " μs");
        System.out.println("Loading: " + (loadTime / 1000) + " μs");
        System.out.println("Reading: " + (readTime / 1000) + " μs");
    }
}

class Employee implements java.io.Serializable {
    public String name;
    public String address;
    public int number;

    @Override
    public String toString() {
        return "Name: " + this.name + "\nAddress: " + this.address + "\nNumber: " + this.number;
    }
}