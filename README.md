### Building ###
GN FW is built using [Maven](http://maven.apache.org/).
You can run

```
#!bash

mvn package
```

to start building.

### Licensing ###

Copyright 2016 Trigon Team ([Ondřej Ondryáš](http://www.ondryaso.eu); Jaroslav Boudný)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0).

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
[Paulscode SoundSystem](http://www.paulscode.com/forum/index.php?topic=4.0) by Paul Lamb included with modified parts