#version 140

in vec4 color;
in vec2 tex;

out vec4 fragColor;

uniform sampler2D fbotex;

uniform vec2 screenSize;
uniform float iteration;

vec4 blur13(sampler2D image, vec2 uv, vec2 pixelSize, float iteration) {
	vec2 uvSample = vec2(0.0, 0.0);
	vec2 dUV = pixelSize.xy * iteration;
	
	vec4 color = vec4(0.0);

	uvSample.x = uv.x - dUV.x;
	uvSample.y = uv.y + dUV.y;
	color = texture(image, uvSample);
	
	uvSample.x = uv.x + dUV.x;
	uvSample.y = uv.y + dUV.y;
	color += texture(image, uvSample);

	uvSample.x = uv.x + dUV.x;
	uvSample.y = uv.y - dUV.y;
	color += texture(image, uvSample);

	uvSample.x = uv.x - dUV.x;
	uvSample.y = uv.y - dUV.y;
	color += texture(image, uvSample);

	color *= 0.25f;

	return color;
}

void main(void)
{
	fragColor = blur13(fbotex, tex, vec2(1.0, 1.0) / screenSize, iteration);
}