#version 140

in vec4 color;
in vec2 tex;

out vec4 fragColor;

uniform sampler2D fbotex;
uniform float time;

void main(void)
{
    fragColor = texture(fbotex, vec2(tex.s + sin(tex.t*10.0+time)*0.1, tex.t));
}